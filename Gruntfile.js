module.exports = function(grunt) {
    
    // Initialize configs
    grunt.initConfig({
	
	// Tasks
	sass: {
		dist: {
			options: {
				style: 'expanded'
			},
			files: {
				'public/css/main.css': 'public/sass/main.sass'
			}
		}
	},
	watch: {
		css: {
			files: ['public/sass/**/*.sass'],
			tasks: ['sass']
		}
	}

    });

    // Load plugins
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Task definitions
    
    grunt.registerTask('default', ['sass', 'watch']);
}
