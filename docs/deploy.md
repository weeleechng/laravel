## Deployment Process

### Server Requirement

* PHP 5.4 and above
* php-mcrypt

### Processs
 
1. Use `git-ftp` and sync with the domain folder __i.e. /home/[domain] Note: this is so that we can link the public_html later__
2. Manually FTP to the `/home/[domain]` and remove/rename `/home/[domain]/public_html` folder
3. Create a link called `public_html` pointing to `/home/[domain]/public`
4. Copy local `/vendor/` folder to server root
5. Copy local `/app/config/app.php` to server
6. Make changes to `/app/config/database.php` to your database settings 
7. Copy local `/public/bower_components` folder to server
8. Copy local `/public/css/main.css` folder to server
9. Create folders `/app/storage/` and `/public/download/` with permissions set to 777