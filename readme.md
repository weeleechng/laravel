## Leads Manager System v.2014

### Contents
* [How to get setup](#how-to-get-setup) 
* [Deploying to live](src/master/docs/deploy.md)

### How to get setup 

- Clone the project
- Setup a Postgresql database named __lm2014__
	- the database username and password should be the default `postgres`
- Create the file `app\config\app.php` and set up OR copy from `app\config\app.php.sample`
- In `app\config\app.php`, look for the `aliases` array definition and add `'Carbon' => 'Carbon\Carbon',` into it
- Libraries to setup 
	- [composer](https://getcomposer.org/doc/00-intro.md) 
	- Nodejs & npm 
		- linux (*Use you package manager to install*)
		- [On Windows](http://nodejs.org/)
	- grunt 
		- run on your terminal `npm install -g grunt-cli` if you get permission errors do `sudo npm install -g grunt-cli`
	- bower
		- `npm install -g bower`  if you get permission errors do `sudo npm install -g grunt-cli`
- Install dependencies
	- `cd [project folder] && composer install` 
- Ensure the correct folder permission settings:
	- full read and write permissions for `app/storage` recursively
	- full read and write permissions for `public/download` recursively
- Run migration scripts & seed the DB
	- `php artisan migrate`
	- `php artisan db:seed`
- Generate the CSS file from SASS
	- `grunt sass`