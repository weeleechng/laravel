@extends ('layouts.front')

@section ('content')

{{ Form:: open (array('id' => 'group_create', 'class' => 'group_create', 'route' => array ('groups.update', $group->id))) }}

<div class="row">
  <h2 class="b-page-heading">Edit: {{ $group->name }}</h2>
  <hr>

    <div>
      {{ Form::label ('id', 'Group Identifier: ') }}
      @if (isset ($group_leads) && $group_leads > 0) 
      <code title="There are leads in this group; the Group ID cannot be changed.">{{ $group->id }}</code>
      {{ Form::hidden ('id', $group->id ) }} 
      @else 
      {{ Form::input ('text', 'id', $group->id, array ('class' => 'mandatory', 'size' => '45')) }} 
      @endif
      {{ $errors->first ('id', '<span class="form-error">:message</span>') }}
    </div>

    <div>
      {{ Form::label ('nm', 'Group name: ') }}
      {{ Form::input ('text', 'nm', $group->name, array ('class' => 'mandatory', 'size' => '45')) }}
      {{ $errors->first ('nm', '<span class="form-error">:message</span>') }}
    </div>

    <div>
      {{ Form::label ('dc', 'Description: ') }}
      {{ Form::textarea ('dc', $group->description) }}
    </div>

    <div>
      {{ Form::label ('cl', 'Client: ') }}
      {{ Form::input ('text', 'cl', $group->client, array ('size' => '45')) }}
    </div>

    <div>
      {{ Form::label ('xt', 'External POST URL: ') }}
      {{ Form::input ('text', 'xt', $group->external, array ('size' => '45')) }}
    </div>

    <div>
      {{ Form::checkbox ('inst', '1', ($group->instantcron == true) ? true : false, array ('id' => 'inst')) }}
      {{ Form::label ('inst', 'No queue', array ('title' => 'Leads will be fully processed on arrival (not recommended)')) }}
      <sub>Submitted leads will be processed immediately and not queued. May result in slower server response.</sub>
    </div>

    <div>
      {{ Form::label ('st', 'Status: ') }}
      {{ Form::select ('st', $group->get_status_list(), $group->status) }}
      {{ $errors->first ('st', '<span class="form-error">:message</span>') }}
    </div>
</div>
  <div class="group--tablecontainer"> 
    <table class="group--fields b-table">

      <tr>
        <th>Display name</th>
        <th>Field name</th>
        <th>Map to</th>
        <th>Default value</th>
        <th>Test value</th>
        <th>Post</th>
        <th>Field rules</th>
        <th>&nbsp;</th>
      </tr>

    @if (Session::has ('return_fields'))
      
      <?php
        
        /*
         * Failed update submissions:
         * Use the submitted values to populate the fields
         * instead of reading fresh from the database 
         */

        $fields = Session::get ('return_fields');
      ?>

    @endif

      <?php

        $mappable = $group->get_mappable();
        foreach (array_keys ($mappable, '') as $key) 
        {
          unset ($mappable[$key]);
        }

        $mapped_fields = []; // for reference to record unmapped group fields

      ?>
      <!-- Render mandatory fields -->
    @foreach ($fields as $field => $properties)
      @foreach ($mappable as $mapname)
        @if (isset ($properties->map) && in_array ($properties->map, $mappable))
          <?php $mapped_fields[] = $properties->map; ?>
          <tr class="group-field mandatory">
            {{ Form::hidden ('deleted[]', 'false') }}
            <td>{{ Form::input ('text', 'dn[]', $properties->displayname) }}</td>
            <td>{{ Form::input ('text', 'fl[]', $field) }}</td>
            <td>{{ Form::hidden ('mp[]', $properties->map) }}{{ $properties->map }}</td>
            <td>{{ Form::input ('text', 'df[]', (isset($properties->defaultvalue) ? $properties->defaultvalue : null)) }}</td>
            <td>{{ Form::input ('text', 'ts[]', (isset($properties->testvalue) ? $properties->testvalue : null)) }}</td>
            <td title="Include this field in external post">
              {{ Form::checkbox ('ps[]', '1', (isset($properties->extpost) ? true : false), array ('id' => $field . '-ps')) }}
            </td>
            <td>
              <div>
                <?php $setrules = (isset ($properties->rules) ? $properties->rules : null); ?>
                @foreach ($group->get_validation_rules() as $rule => $name)
                <?php

                  $fieldrule = null;
                  if (is_array ($setrules))
                  {
                    foreach ($setrules as $setrule)
                    {
                      if ($setrule->baserule == $rule)
                      {
                        $fieldrule = $setrule;
                        break;
                      }
                    }
                  }

                  if (isset ($fieldrule) && $fieldrule->baserule == $rule)
                  {
                    $checked = 'checked';
                  }
                  else
                  {
                    $checked = null;
                  }

                ?>
                <div class="checkbox-fieldset">
                  {{ Form::checkbox ('rb[' . $rule . '][]', '1', null, array ('id' => $field . '-' . $rule, $checked => '')) }} 
                  {{ Form::label ($field . '-' . $rule, $name) }}
                  @if ($rule != 'honeypot')
                  <div class="textfield-floating">
                    {{ Form::input ('text', 'rm[' . $rule . '][]', (isset ($fieldrule->message) ? $fieldrule->message : null), array ('placeholder' => 'Error message')) }}
                  @if ($rule == 'regex')
                    {{ Form::input ('text', 'rl[regex][]', (isset ($fieldrule->rule) ? $fieldrule->rule : null), array ('placeholder' => 'Regex pattern')) }}
                  @endif
                  </div>
                  @endif
                </div>

                @endforeach
              </div>
            </td>
            <td>&nbsp;</td>
          </tr>

        <?php
          /* Break out of foreach loop after the mandatory field match is found, move on to next */ 
          break; 
        ?>

        @endif     
      @endforeach
    @endforeach

    <?php 
      /* Create fields for mandatory fields that have not yet been mapped, just in case */
      $map_diff = array_diff ($mappable, $mapped_fields); 
    ?>

    @foreach ($map_diff as $mapname)
      <tr class="group-field mandatory">
        {{ Form::hidden ('deleted[]', 'false') }}
        <td>{{ Form::checkbox ('ps[]', '1', false) }} {{ Form::input ('text', 'dn[]', null) }}</td>
        <td>{{ Form::input ('text', 'fl[]', $mapname) }}</td>
        <td>{{ Form::hidden ('mp[]', $mapname) }}{{ $mapname }}</td>
        <td>{{ Form::input ('text', 'df[]', null) }}</td>
        <td>{{ Form::input ('text', 'ts[]', null) }}</td>
        <td title="Include this field in external post">
          {{ Form::checkbox ('ps[]', '1', (isset($properties->extpost) ? true : false), array ('id' => $field . '-ps')) }}
        </td>
        <td>
          <div>
          @foreach ($group->get_validation_rules() as $rule => $name)
            <?php
              if ($rule == 'required' ||
                  $rule == 'email' && $mapname == 'email')
                $checked = 'checked';
              else
                $checked = null;
            ?>
            <div class="checkbox-fieldset">
              {{ Form::checkbox ('rb[' . $rule . '][]', '1', null, array ('id' => $rule, $checked)) }} 
              {{ Form::label ($rule, $name) }}
              @if ($rule != 'honeypot')
              <div class="textfield-floating">
                {{ Form::input ('text', 'rm[' . $rule . '][]', null, array ('placeholder' => 'Error message')) }}
              @if ($rule == 'regex')
                {{ Form::input ('text', 'rl[regex][]', null, array ('placeholder' => 'Regex pattern')) }}
              @endif
              </div>
              @endif
            </div>
          @endforeach
          </div>
        </td>
        <td>&nbsp;</td>
      </tr>
    @endforeach

    <!-- Non-mandatory fields -->
    @foreach ($fields as $field => $properties)
      @if (!isset ($properties->map))
        <tr class="group-field">
          {{ Form::hidden ('deleted[]', 'false') }}
          <td>{{ Form::input ('text', 'dn[]', $properties->displayname) }}</td>      
          <td>{{ Form::input ('text', 'fl[]', $field) }}</td>
          <td>&nbsp;</td>
          <td>{{ Form::input ('text', 'df[]', (isset($properties->defaultvalue) ? $properties->defaultvalue : null)) }}</td>
          <td>{{ Form::input ('text', 'ts[]', (isset($properties->testvalue) ? $properties->testvalue : null)) }}</td>
          <td title="Include this field in external post">
            {{ Form::checkbox ('ps[]', '1', (isset($properties->extpost) ? true : false), array ('id' => $field . '-ps')) }}
          </td>
          <td>
            <div>
              <?php $setrules = (isset ($properties->rules) ? $properties->rules : null); ?>
              @foreach ($group->get_validation_rules() as $rule => $name)
              <?php
                $fieldrule = null;
                if (is_array ($setrules))
                {
                  foreach ($setrules as $setrule)
                  {
                    if ($setrule->baserule == $rule)
                    {
                      $fieldrule = $setrule;
                      break;
                    }
                  }
                }

                if (isset ($fieldrule) && $fieldrule->baserule == $rule)
                {
                  $checked = 'checked';
                }
                else
                {
                  $checked = null;
                }

              ?>
              <div class="checkbox-fieldset">
                {{ Form::checkbox ('rb[' . $rule . '][]', '1', null, array ('id' => $field . '-' . $rule, $checked => '')) }} 
                {{ Form::label ($field . '-' . $rule, $name) }}
                @if ($rule != 'honeypot')
                <div class="textfield-floating">
                  {{ Form::input ('text', 'rm[' . $rule . '][]', (isset ($fieldrule->message) ? $fieldrule->message : null), array ('placeholder' => 'Error message')) }}
                @if ($rule == 'regex')
                  {{ Form::input ('text', 'rl[regex][]', (isset ($fieldrule->rule) ? $fieldrule->rule : null), array ('placeholder' => 'Regex pattern')) }}
                @endif
                </div>
                @endif
              </div>
              @endforeach
            </div>
          </td>
          <td>
            {{ Form::button ('-', array ('title' => 'Remove field', 'class' => 'remove-field tiny alert')) }}
          </td>
        </tr>
      @endif
    @endforeach

    <!-- Blank field row for adding new fields -->
    <tr class="group-field add" rowcount="1">
      {{ Form::hidden ('deleted[]', 'false') }}   
      <td>{{ Form::input ('text', 'dn[]', null) }}</td>
      <td>{{ Form::input ('text', 'fl[]', null) }}</td>
      <td>&nbsp;</td>
      <td>{{ Form::input ('text', 'df[]', null) }}</td>
      <td>{{ Form::input ('text', 'ts[]', null) }}</td>
      <td title="Include this field in external post">
        {{ Form::checkbox ('ps[]', '1', true, array ('id' => $field . '-ps')) }}
      </td>
      <td>
        <div>
          @foreach ($group->get_validation_rules() as $rule => $name)
            <div class="checkbox-fieldset">
              {{ Form::checkbox ('rb[' . $rule . '][]', '1', null, array ('id' => $rule)) }} 
              {{ Form::label ($rule, $name) }}
              @if ($rule != 'honeypot')
              <div class="textfield-floating">
                {{ Form::input ('text', 'rm[' . $rule . '][]', null, array ('placeholder' => 'Error message')) }}
              @if ($rule == 'regex')
                {{ Form::input ('text', 'rl[regex][]', null, array ('placeholder' => 'Regex pattern')) }}
              @endif
              </div>
              @endif
            </div>
          @endforeach
        </div>
      </td>
      <td>
        {{ Form::button ('-', array ('title' => 'Remove field', 'class' => 'remove-field tiny alert')) }}
      </td>
    </tr>

    </table>

    <div>
      {{ Form::button ('Add Field', array ('title' => 'Add field', 'id' => 'add-field', 'class' => 'small success')) }}
      &nbsp;
      {{ Form::button ('Undo', array ('title' => 'Undo Remove field', 'id' => 'undo-remove-field', 'class' => 'small')) }}
    </div>

   <hr>
  </div>

  <div class="row">

    <div>
      {{ Form::submit ("Save", array('class' => 'button success')) }}
      {{ link_to_route ("groups.index", "Cancel", null, array ("class" => "button")) }}
    </div>

    {{ Form::hidden ("referer", Request::header('referer')) }}

  </div>
  
{{ Form:: close () }}

@stop

@section('scripts')

{{ HTML::script('js/group_create.js'); }}

@stop
