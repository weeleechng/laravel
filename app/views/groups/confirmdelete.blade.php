@extends ('layouts.front')


@section ('content')
<div class="row">
  <div class="columns medium-6 medium-centered">
  <h4 class="text-center b-page-heading">Delete Lead Group <strong>{{ $group->name }}</strong>?</h4><hr>

  {{ Form:: open (array('class' => 'group_delete', 'route' => array ('groups.delete', $group->id), 'method' => 'delete' )) }}

    <div>
      {{ Form::submit ("Delete", array('class' => 'button alert')) }}
      {{ link_to_route ("groups.index", " Cancel ", null, ["class" => "button"]) }}
    </div>

  {{ Form:: close () }}
  </div>
</div>
@stop
