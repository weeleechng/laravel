@extends('layouts.front')

@section('content')
<div class="row">
  <div class="columns">
    
  <h3 class="b-page-heading"><strong>Lead Group: </strong> {{ $group->name }}</h3>
  <hr>
  <ul class="button-group">
    <li>{{ link_to_route ("groups.index", "&laquo; All Groups", null, array("class" => "button")) }}</li> 
    <li>{{ link_to_route ("groups.edit", "Edit " . $group->name, $group->id, array("class" => "button alert")) }}</li> 
    <li>{{ link_to_route ("groups.sampleform", "Sample Form", $group->id, array("class" => "button success", "target" => "_blank")) }}</li>
    <li>{{ link_to_route ("lead.index", "Leads", array ('where', 'group', $group->id), array("class" => "button")) }}</li> 
  </ul>

	<div class="panel group-detail">
    <div class="row"><strong class="columns medium-2">Name:</strong> <code class="columns medium-10">{{ $group->name }}</code></div><hr>
    <div class="row"><strong class="columns medium-2">Description:</strong> <span class="columns medium-10">{{ $group->description }}</span></div><hr>
    <div class="row"><strong class="columns medium-2">Client:</strong> <span class="columns medium-10">{{ $group->client }}</span></div><hr>
    <div class="row"><strong class="columns medium-2">External URL:</strong> @if($group->external) <code class="columns medium-10">{{ $group->external }}</code> @endif </div><hr>
    <div class="row"><strong class="columns medium-2">Status:</strong> <span class="columns medium-10"><span class="label radius {{ $group->status }}-label">{{ $group->status }}</span> </span></div>
  </div>

  @if (count($fields) > 0)

  <!--span class="label">Fields</span-->
  <table class="group--fields b-table">

    <tr>
      <th>Display name</th>
      <th>Fieldname</th>
      <th>Map to</th>
      <th>Default value</th>
      <th>Test value</th>
      <th title="Field included in external post">Post</th>
      <th>Rules</th>
    </tr>

    @foreach ($fields as $field => $properties)

    <tr>

      <td>{{ $properties->displayname }}</td>
      <td><code>{{ $field }}</code></td>
      <td> @if (isset ($properties->map))<code>{{ $properties->map }}</code>@endif</td>
      <td>{{ (isset($properties->defaultvalue) ? $properties->defaultvalue : '') }}</td>
      <td>{{ (isset($properties->testvalue) ? $properties->testvalue : '') }}</td>
      <td>{{ (isset($properties->extpost)) ? 'Yes' : 'No' }}</td>
      <td>
      @if (isset ($properties->rules))
        @foreach ($properties->rules as $rule)
        <div class="faux-list">{{ $rule->type }}</div>
        @endforeach
      @endif
      </td>
    </tr>

    @endforeach

  </table>

  @endif

  <ul class="button-group">
    <li>{{ link_to_route ("groups.index", "&laquo; All Groups", null, array("class" => "button")) }}</li> 
    <li>{{ link_to_route ("groups.edit", "Edit " . $group->name, $group->id, array("class" => "button alert")) }}</li> 
    <li>{{ link_to_route ("groups.sampleform", "View Sample Form", $group->id, array("class" => "button success", "target" => "_blank")) }}</li> 
    <li>{{ link_to_route ("lead.index", "Leads", array ('where', 'group', $group->id), array("class" => "button")) }}</li> 
  </ul>

  </div>
</div>
@stop