@extends('layouts.front')

@section('content')
<div class="row">
  <div class="columns">
    <h2 class="b-page-heading">Lead Groups</h2><hr>

    {{ link_to_route ("groups.create", "New Lead Group", null, array ("class" => "button")) }}

    @if (count ($groups) > 0)

      <table class="b-table">
       <thead> 
        <tr>
          <th>Group Name</th>
          <!--th>Client</th-->
          <th>External URL</th>
          <th>Status</th>
          <th colspan="5">Actions</th>
        </tr>
      <thead>
      <tbody>
      @foreach ($groups as $group)

        <tr>
          <td>{{ link_to_route ("groups.get", $group->name, $group->id) }}<br /><sub>{{ $group->description }}</sub></td>
          <!--td>{{ $group->client }}</td-->
          <td>@if ($group->external) <code>{{ $group->external }}</code> @endif </td>
          <td><span class="label {{ $group->status }}-label radius">{{ $group->status }}</span></td>
          <td>
              {{ link_to_route ("lead.index", "leads", ['where', 'group', $group->id]) }}
          </td>
          <td>
            {{ link_to_route ("groups.sampleform", "form", $group->id, array ('target' => '_blank')) }}
          </td>
          <td>
            {{ link_to_route ("groups.clone", "clone", $group->id) }}
          </td>
          <td>
            {{ link_to_route ("groups.edit", "edit", $group->id) }}
          </td>
          <td>
            {{ link_to_route ("groups.delete.confirm", "delete", $group->id) }}
          </td>
        </tr>

      @endforeach
      </tbody>
      </table>

    @else

      <p>No lead groups found</p>

    @endif
  </div>
</div>  
	
@stop
