<?php 
  $page_title = $group->name . ': Sample Form'; 
  $fields = json_decode ($group->rulejson);
  // dd ($rules);
?>

@extends('layouts.default')

@section('content')

<div class="row">
<div class="columns medium-8 medium-centered">

@if (count($fields) > 0)

<h3>Sample Form for <em>{{ $group->name }}</em> </h3>

@if ($group->honeypot != null)
<p>
  <strong>ALERT: </strong>
  <span>There is/are hidden honeypot field(s) which <u>must</u> be included in the form.</span>
</p>
@endif

<form id="sample_form" action="{{ URL::route('lead.store'); }}" method="post">
<style>
  input.system { display: none; }
</style>
@foreach ($fields as $field => $properties)
<?php
  $honeypot = false;
  $required = false;
  if (isset ($properties->rules))
  {
    foreach ($properties->rules as $rule)
    {
      if ($rule->baserule == 'required')
      {
        $required = true;
      }

      if ($rule->baserule == 'honeypot')
      {
        $honeypot = true;
      }
    }
  }
?>
@if ($honeypot == false) <div> 
  <label style="display:inline-block" for="{{ $field }}"> {{ $properties->displayname }} @if ($required) <span class="required">*</span> @endif</label>
  <span style="display:inline-block" class="response_label" id="label_{{ $field }}"></span>
  @endif <input class="field <?php if ($honeypot == true) echo 'system'; ?>" type="text" name="{{ $field }}" id="{{ $field }}" @if (isset ($properties->defaultvalue)) {{ 'value="' . $properties->defaultvalue . '"' }} @endif></input> @if ($honeypot == false)
</div> @endif

@endforeach

<!-- REQUIRED lead_group FIELD -->
<div>
  <label for="lead_group">Lead Group (required)</label>
  <input type="text" name="lead_group" value="{{ $group->id }}" readonly />
</div>
<!-- REQUIRED lead_group FIELD -->

<!-- OPTIONAL: lead_test FIELD -->
<!-- Submit lead_test=1 to mark this lead as a test -->
<div>
  <input type="checkbox" name="lead_test" id="lead_test" value="1" /><label for="lead_test"><code>This is a test lead.</code></label>
</div>
<!-- OPTIONAL: lead_test FIELD -->

<input type="submit" id="submit-button" name="Submit" value="Submit" /> 

</form>

<div>&nbsp;</div>

<textarea id="sample_form_code" style="font-family:monospace;">
</textarea>

<div>&nbsp;</div>

@endif

</div>
</div>

@stop

@section('scripts')

  {{ HTML::script('js/lead_sample_form.js'); }}
  {{ HTML::script('js/lead_sample_form_jquery_ajax.js', array ('id' => 'sample-form-ajax-script')); }}

@stop