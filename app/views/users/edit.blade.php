@extends ('layouts.front')

@section ('content')
  <div class="row b-form">
    <div class="columns panel medium-6 medium-centered">
      <h3 class="b-form-heading text-center">Edit user</h3>
      <hr>
      {{ Form:: open (array('route' => array ('user.update', $user->id))) }}

        @if($errors->has('name')) <div class="error">
        @else <div> @endif
          {{ Form::label ('name', 'Full name') }}
          {{ Form::input ('text', 'name', $user->name, array ('class' => 'mandatory', 'size' => '45')) }}
          {{ $errors->first ('name', '<small class="error">:message</small>') }}
        </div>

        @if($errors->has('id')) <div class="error">
        @else <div> @endif
          {{ Form::label ('id', 'Username') }}
          {{ Form::text('id', $user->id, array('disabled' => true)) }}
        </div>

        @if ($edit_password == true)
          @if($errors->has('changepassword')) <div class="error">
          @else <div> @endif
            {{ Form::label ('changepassword', 'Change password (**optional)') }}
            {{ Form::input ('password', 'changepassword', '', array ('size' => '45')) }}
            {{ $errors->first ('changepassword', '<small class="error">:message</small>') }} 
          </div>

          <div>
            {{ Form::label ('changepassword_confirmation', 'Repeat password') }}
            {{ Form::input ('password', 'changepassword_confirmation', '', array ('size' => '45')) }}
            {{ $errors->first ('changepassword_confirmation', '<small class="error">:message</small>') }}
          </div>
        @endif

        @if($errors->has('role')) <div class="error">
        @else <div> @endif
          {{ Form::label ('role', 'Role') }}
          @if (is_array ($roles)) 
            {{ Form::select ('role', $roles, $user->role) }}
          @else
            {{ Form::hidden ('role', $user->role) }}
            {{ Form::text('', $user->userrole->name, array('disabled', true) ) }}
          @endif
          {{ $errors->first ('role', '<small class="error">:message</small>') }}
        </div>

        @if($errors->has('email')) <div class="error">
        @else <div> @endif
          {{ Form::label ('email', 'E-mail') }}
          {{ Form::input ('email', 'email', $user->email, array ('class' => 'mandatory', 'size' => '45')) }}
          {{ $errors->first ('email', '<small class="error">:message</small>') }}
        </div>

        @if($errors->has('status')) <div class="error">
        @else <div> @endif
          {{ Form::label ('status', 'User status') }}
          @if (is_array ($roles)) 
            {{ Form::radio('status', 'active', (strtolower ($user->status) == 'active') ? true : false) }} Active &nbsp; 
            {{ Form::radio('status', 'blocked', (strtolower ($user->status) != 'active') ? true : false) }} Blocked
          @else
            {{ Form::hidden ('status', strtolower ($user->status)) }}
            {{ Form::text('', $user->status, array('disabled' => true)) }}
          @endif
          {{ $errors->first ('status', '<small class="error">:message</small>') }}
        </div>

        <div>
          {{ Form::submit ("Update", array('class' => 'button success expand')) }}
          {{ link_to_route ("users.index", "Cancel", null, array ("class" => "button expand")) }}
          @if ($user->userrole->canbedeleted == true && $roles != false) 
          {{ link_to_route ("user.delete.confirm", "Delete", $user->id, array ("class" => "button alert expand")) }} 
          @endif
        </div>

      {{ Form:: close () }}
    
    </div>
  </div>
@stop
