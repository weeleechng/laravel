@extends ('layouts.front')

@section ('content')
  <div class="row">
      <div class="columns medium-6 medium-centered">
  @if ($user->userrole->canbedeleted == true && $can_delete == true)

    <h4 class="text-center b-page-heading">Delete user: <strong>{{ $user->name }}</strong>?</h4><hr>

    {{ Form:: open (array('class' => 'user_delete', 'route' => array ('user.delete', $user->id), 'method' => 'delete' )) }}

      <div>
        {{ Form::submit ("Delete", array('class' => 'button alert')) }}
        {{ link_to_route ("users.index", " Cancel ", null, ["class" => "button"]) }}
      </div>

    {{ Form:: close () }}

  @else

    <h4 class="text-center b-page-heading">Request Denied</h4><hr>

    <div>
      <p>User <strong>{{ $user->name }}</strong> cannot be deleted.</p>
      <p>{{ link_to_route ("users.index", " Go back ", null, ["class" => "button"]) }}</p>
    </div>

  @endif

  </div>
    </div>
@stop
