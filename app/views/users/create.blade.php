@extends ('layouts.front')

@section ('content')
  <div class="row b-form">
    <div class="panel columns medium-6 medium-centered">
      <h3 class="b-form-heading text-center">Add New User</h3>
      <hr>
      {{ Form:: open (array('class' => 'user_create', 'route' => 'user.store')) }}

        @if($errors->has('name')) 
        <div class="error"> @else <div> @endif
            {{ Form::label ('name', 'Full name') }}
            {{ Form::input ('text', 'name', '', array ('class' => 'mandatory')) }}
          {{ $errors->first ('name', '<small class="error">:message</small>') }}
        </div>

        @if($errors->has('newid')) 
        <div class="error"> @else <div> @endif
            {{ Form::label ('newid', 'Username') }}
            {{ Form::input ('text', 'newid', '', array ('class' => 'mandatory')) }}
          
          {{ $errors->first ('newid', '<small class="error">:message</small>') }}
        </div>

        @if($errors->has('newpassword')) 
        <div class="error"> @else <div> @endif
            {{ Form::label ('newpassword', 'Password') }}
            {{ Form::input ('password', 'newpassword', '', array ('class' => 'mandatory')) }}
          
          {{ $errors->first ('newpassword', '<small class="error">:message</small>') }}
        </div>

        <div>
            {{ Form::label ('newpassword_confirmation', 'Repeat Password') }}
            {{ Form::input ('password', 'newpassword_confirmation', '', array ('class' => 'mandatory')) }}
        </div>
      

        @if($errors->has('role'))<div class="error"> @else <div> @endif
            {{ Form::label ('role', 'Role') }}
            {{ Form::select ('role', $roles) }}
            {{ $errors->first ('role', '<small class="error">:message</small>') }}
        </div>

        @if($errors->has('email')) 
        <div class="error"> @else <div> @endif
          {{ Form::label ('email', 'E-mail') }}
          {{ Form::input ('email', 'email', '', array ('class' => 'mandatory')) }}
          {{ $errors->first ('email', '<small class="error">:message</small>') }}
        </div>

        @if($errors->has('status')) 
        <div class="error"> @else <div> @endif
          {{ Form::label('', 'User status') }}
          {{ Form::radio('status', 'active', true, array('id' => 'active')) }}
          {{ Form::label ('active', 'Active') }} 
          {{ Form::radio('status', 'blocked', '', array('id' => 'blocked')) }}
          {{ Form::label ('blocked', 'Blocked') }}
          {{ $errors->first ('status', '<small class="error">:message</small>') }}
        </div>

        <div>
          {{ Form::submit ("Submit", array('class' => 'button success expand')) }}
          {{ link_to_route ("users.index", " Cancel ", null, array("class" => "button expand")) }}
        </div>

      {{ Form:: close () }}
    </div>
  </div>
  
@stop
