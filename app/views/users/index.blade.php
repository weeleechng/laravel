@extends('layouts.front')

@section('content')
	<div class="row">
		<div class="columns medium-12">
			<h2 class="b-page-heading">Users @if (isset($status)) <small>&dash; {{ $status }}</small> @endif</h2>
			<hr>
			
			@if (isset($status) && $status != '')
		    {{ link_to_route ("users.index", "&laquo; All Users", null, array ("class" => "button")) }}
			@endif

			{{ link_to_route ("user.create", "Add User", null, array ("class" => "button")) }}

			@if (count ($users) > 0)

				<table class="b-table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Username</th>
							<th>Email Address</th>
							@if (!@$status)<th>Status</th> @endif
							<th colspan="2">Actions</th>
						</tr>
					</thead>
					<tbody>
					  @foreach ($users as $user)

					    <tr>
					    	<td>{{ link_to_route ("user.get", $user->name, $user->id) }} </td>
					    	<td>@if (isset ($user->userrole->name)) {{ $user->userrole->name }} @endif</td>
					    	<td><code>&lt;{{ $user->email }}&gt;</code></td>

				    	  @if (!isset($status)) 
				    	    <td>
				    	    	{{ link_to_route("users.all.bystatus", $user->status, $user->status, array('class' => "label radius $user->status-label")) }}
				    	    </td>
				    	  @endif 

			    		<span class="form-action"> 
			    	  
						<td>{{ link_to_route("user.edit", "edit", $user->id) }}</td> 
						<td>
				    	  @if ($current_user != $user->id && $user->userrole->canbedeleted == true)
				    	    {{ link_to_route("user.delete.confirm", "delete", $user->id, array("data-method" => "delete")) }}</td>
				    	  @endif
				    	</td>
				    	</span>
					    </tr>

					  @endforeach
					</tbody>
				</table>
			@else

			  <p>No @if (isset($status)) {{ $status }} @endif users found</p>

			@endif

		</div>
	</div>
@stop
