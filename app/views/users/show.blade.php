@extends('layouts.front')

@section('content')
 <?php
  $permission = json_decode ($user->userrole->permissionjson);
?> 
  <div class="row">
    <div class="columns medium-8 medium-centered">
        <h3 class="b-page-heading">{{ $user->name }}</h3>
        <hr>  
        <div class="panel">
        <div class="row">
          <div class="columns medium-6"><strong>Username</strong></div>
          <div class="columns medium-6"> <code>{{ $user->id }}</code></div>
        </div>
        
        <div class="row">
          <div class="columns medium-6"><strong>Email</strong></div>
          <div class="columns medium-6"> <code>{{ $user->email }}</code></div>
        </div>
        
        @if(isset($user->userrole->name))
        <div class="row">
          <div class="columns medium-6">
            <strong>Role</strong>
          </div>
          <div class="columns medium-6">
            {{ $user->userrole->description }}
          </div>
        </div>

        <div class="row">
          <div class="columns">
            <strong>Permissions</strong>
              <ul>
              @foreach ($permission as $key => $value) 
                @if ($value == "true") 
                  <li><code>{{ $permissions->find($key)->name }}</code></li>  
                @endif
              @endforeach
              </ul>
          </div>
        </div>
        @endif
        
        <div class="row">
          <div class="columns medium-6"><strong>Status</strong></div>
          <div class="columns medium-6"><div class="label @if($user->status == 'blocked') alert @endif">{{ $user->status }}</div></div>
        </div>
        </div>
        <div class="row">
          <div class="columns">
            {{ link_to_route ("users.index", "&laquo; All Users", null, array("class" => "button")) }}
            {{ link_to_route ("user.edit", "Edit user", $user->id, array("class" => "button")) }}
            @if ($user->userrole->canbedeleted == true && json_decode ($user->userrole->permissionjson)->manage_users == "true") 
            {{ link_to_route ("user.delete.confirm", " Delete ", $user->id, array ("class" => "button alert")) }} 
            @endif
          </div>
        </div>
        
    </div>
  </div>
@stop