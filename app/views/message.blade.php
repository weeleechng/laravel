@extends('layouts.standard')

@section('head_title')
Lead Manager v.2014 @if (isset($head_title)) - {{ $head_title }} @endif
@stop

@section('content')
	<div class="welcome">
		<h1>{{ $head_title }}</h1>
    <div class="content">
      @if (isset($message)) {{ $message }} @endif
    </div>
	</div>
@stop
