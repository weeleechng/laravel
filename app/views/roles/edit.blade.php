@extends ('layouts.front')

@section ('content')

<?php
  $user_permissions = json_decode ($role->permissionjson, true);
?>
<div class="row b-form">
  <div class="columns panel medium-6 medium-centered">
    <h3>Edit: {{ $role->name }}</h3>
    <hr>
    {{ Form:: open (array('class' => 'role_create', 'route' => array ('roles.update', $role->id))) }}

      <div>
        {{ Form::label ('nm', 'Role name: ') }}
        {{ Form::input ('text', 'nm', $role->name, array ('class' => 'mandatory', 'size' => '45')) }}
        {{ $errors->first ('nm', '<span class="form-error">:message</span>') }}
      </div>

      <div>
        {{ Form::label ('dc', 'Description: ') }}
        {{ Form::textarea ('dc', $role->description) }}
        {{ $errors->first ('dc', '<span class="form-error">:message</span>') }}
      </div>

      <div>
        {{ Form::label ('pm', 'Permissions: ') }}
        
        <div class="form-field-inline">

          @foreach ($permissions as $permission)

            {{ Form::checkbox ('pm[]', $permission->id, ($user_permissions[$permission->id] == "true") ? true : false) }}
            {{ Form::label($permission->name) }}
            <br />

          @endforeach

        </div>

        {{ $errors->first ('pm', '<span class="form-error">:message</span>') }}
      </div>

      <div>
        {{ link_to_route ("roles.index", "Cancel", null, array ("class" => "button expand")) }}
        {{ Form::submit ("Update", array('class' => 'button success expand')) }}
        {{ link_to_route ("role.delete.confirm", "Delete", $role->id, array ("class" => "button expand alert")) }}
      </div>

    {{ Form:: close () }}
  </div>
</div>

@stop
