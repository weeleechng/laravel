@extends ('layouts.front')

@section ('content')
<div class="row b-form">
  <div class="panel columns medium-6 medium-centered">
    <h3 class="b-form-heading text-center">Create User Role</h3>
    <hr>
    {{ Form:: open (array('class' => 'role_create', 'route' => array ('roles.store'))) }}

       @if($errors->has('nm')) 
        <div class="error"> @else <div> @endif
        {{ Form::label ('nm', 'Role name: ') }}
        {{ Form::input ('text', 'nm', '', array ('class' => 'mandatory')) }}
        {{ $errors->first ('nm', '<small class="error">:message</small>') }}
      </div>

       @if($errors->has('dc')) 
        <div class="error"> @else <div> @endif
        {{ Form::label ('dc', 'Description: ') }}
        {{ Form::textarea ('dc', '') }}
        {{ $errors->first ('dc', '<small class="error">:message</small>') }}
      </div>

      @if($errors->has('pm')) 
        <div class="error"> @else <div> @endif
        {{ Form::label ('pm', 'Permissions: ') }}
        
        <div class="form-field-inline">

          @foreach ($permissions as $permission)

            {{ Form::checkbox ('pm[]', $permission->id) }} {{ $permission->name }}

            <br />

          @endforeach

        </div>

       {{ $errors->first ('pm', '<small class="error">:message</small>') }}
      </div>

      <div>
        {{ Form::submit ("Submit", array('class' => 'button success expand')) }}
        {{ link_to_route ("roles.index", "Cancel", null, array ("class" => "button expand alert")) }}
      </div>

    {{ Form:: close () }}
  </div>
</div>

@stop
