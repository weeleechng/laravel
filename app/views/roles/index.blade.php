@extends('layouts.front')

@section('content')

<div class="row">
  <div class="columns medium-12">
    <h2 class="b-page-heading">User Roles</h2>
    <hr>
    {{ link_to_route ("roles.create", "Add Role", null, array ("class" => "button")) }}

    @if (count ($roles) > 0)

      <table class="b-table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Permissions</th>
            <th colspan="2">Actions</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($roles as $role)
            <tr>
              <td>{{ link_to_route ("roles.get", $role->name, $role->id) }}</td>
              <td>{{ $role->description }}</td>
              <td>{{ link_to_route ("roles.edit", "edit", $role->id) }}</td>
              <td>
                    @if ($role->canbedeleted == true)
                    {{ link_to_route("role.delete.confirm", "delete", $role->id, array("data-method" => "delete")) }}
                    @endif
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>

    @else

      <p>No user roles found</p>

    @endif

  </div>
</div>
@stop
