@extends ('layouts.front')

@section ('content')
  <div class="row">
    <div class="columns medium-6 medium-centered">
      @if ($role->canbedeleted == true)

        <h3 class="text-center b-page-heading">Delete role: {{ $role->name }}</h3><hr>

        {{ Form:: open (array('class' => 'role_delete', 'route' => array ('role.delete', $role->id), 'method' => 'delete' )) }}

          <div>
            {{ Form::submit ("Delete", array('class' => 'button alert')) }}
            {{ link_to_route ("roles.index", "Cancel", null, ["class" => "button"]) }}
          </div>

        {{ Form:: close () }}

      @else

        <h4 class="text-center b-page-heading">Request Denied</h4><hr>

        <div>
          <p>The role <strong>{{ $role->name }}</strong> cannot be deleted.</p>
          <p>{{ link_to_route ("roles.index", " Go back ", null, ["class" => "button"]) }}</p>
        </div>

      @endif
    </div>
  </div>
@stop
