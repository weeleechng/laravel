@extends('layouts.front')

@section('content')

<?php
  $permission = json_decode ($role->permissionjson);
?>

<div class="row">
  <div class="columns medium-8 medium-centered">
    <h3 class="b-page-heading">{{ $role->name }}</h3>
    <hr>
    <div class="panel">
      <div class="row">
        <div class="columns medium-6"><strong>Name</strong></div>
        <div class="columns medium-6">{{ $role->name }}</div>
      </div>
      <div class="row">
        <div class="columns medium-6"><strong>Description</strong></div>
        <div class="columns medium-6">{{ $role->description }}</div>
      </div>
      <div class="row">
        <div class="columns">
          <strong>Permissions</strong>
            <ul>
            @foreach ($permission as $key => $value) 
              @if ($value == "true") 
                <li><code>{{ $permissions->find($key)->name }}</code></li>  
              @endif
            @endforeach
            </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="columns">
        {{ link_to_route ("roles.index", "&laquo; All Roles", null, array("class" => "button")) }} 
        {{ link_to_route ("roles.edit", "Edit role", $role->id, array("class" => "button")) }}
        @if ($role->canbedeleted == true) {{ link_to_route ("role.delete.confirm", "Delete", $role->id, array("class" => "button alert")) }} @endif
      </div>
    </div>
  </div>
</div>

@stop