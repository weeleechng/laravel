@extends('layouts.standard')

@section('head_title')
Database Access Demo
@stop

@section('content')
	<h1>Database content</h1>
	<span class="system">
		@if (count ($users) > 0)

		  @foreach ($users as $user)
		    <p>
		    	<?php print ($user->name); ?>
		    </p>
		  @endforeach

		@else

		  <p>No records in selected database table</p>

		@endif
  </span>
@stop