@extends('layouts.front')

@section('content')
<div class="row">
		<div class="b-error panel columns small-centered ">
			<h1 class="text-center"><strong>Error 404: </strong><span class="text-alert">Page not found!</span></h1><hr>
			{{ link_to_route('home', 'Click here to go back', null, array('class' => 'button expand')) }}
		</div>
</div>
@stop
