@extends ('layouts.front')

<?php 

function is_honeypot ($properties)
{
  if (isset ($properties['rules']))
  {
    foreach ($properties['rules'] as $rule)
    {
      if (isset ($rule['baserule']) && $rule['baserule'] == 'honeypot')
      {
        return true;
      }
    }
  }

  return false;
}

$text_size = 45;

$core_fields = $report->get_core_field ();
$groups_fields = json_decode ($group->rulejson, true);
$report_email = json_decode ($report->email, true);
$reportjson = json_decode ($report->reportjson);
$reportfields = json_decode (json_encode ($reportjson->fields), true);

$frequency_select = $report->get_frequency_default ();
$field_condition = $report->get_field_condition ();

// var_dump ($groups_fields); exit ();

?>

@section ('content')
<div class="row">
  <h2 class="b-page-heading">{{ $page_title }}</h2>
  <hr>

  {{ Form:: open (array('id' => 'report_create', 'class' => 'report_create', 'route' => array ('report.update', $report->id))) }}

    <div>
      {{ Form::label ('nm', 'Report name: ') }}
      {{ Form::input ('text', 'nm', $report->name, array ('size' => $text_size, 'class' => 'mandatory')) }}
      {{ $errors->first ('nm', '<span class="form-error">:message</span>') }}
    </div>

    <div>
      {{ Form::label ('dc', 'Description: ') }}
      {{ Form::textarea ('dc', $report->description) }}
    </div>

    <div>
      {{ Form::label ('st', 'Status: ') }}
      {{ Form::select ('st', $report->get_status_list(), $report->status) }}
    </div>

    <div>
      {{ Form::label ('fr', 'Frequency: ') }}
      {{ Form::select ('fr', $frequency_select, $reportjson->frequency) }}
    </div>

    <div>
      {{ Form::label ('lg', 'Last Generated: ') }}
      {{ Form::input ('text', 'lg', $report->last_generated, array ('placeholder' => 'YYYY-MM-DD HH:MM:SS')) }}
      {{ $errors->first ('lg', '<span class="form-error">:message</span>') }}
    </div>

    <div>
      {{ Form::label ('fl', 'CSV filename template: ') }}
      {{ Form::input ('text', 'fl', $reportjson->filename, array ('id' => 'report_csv_filename')) }}
    </div>

    {{ $errors->first ('rf-sel', '<span class="form-error">:message</span>') }}

    <div>
      <table class="b-table">
        <tr>
          <th>Field</th>
          <th>Report fieldname</th>
          <th>Condition</th>
          <th>Condition Value(s)</th>
        </tr>

        @foreach ($groups_fields as $field => $properties)
        @if (!is_honeypot ($properties))
        <tr class="report-field {{ $group->id }}" style="display:none;">
          <td>{{ Form::checkbox ('rf-sel[' . $field . ']', '1', (isset($reportfields[$field])) ? true : false, array ('id' => 'rf-sel_' . $field)) }}
          {{ Form::label ('rf-sel_' . $field, ($properties['displayname'] != '') ? $properties['displayname'] : $field) }}
          {{ Form::hidden ('rf-dn[' . $field . ']', $properties['displayname']) }}</td>
          <td>{{ Form::input ('text', 'rf-alias[' . $field . ']', (isset ($reportfields[$field]['alias'])) ? $reportfields[$field]['alias'] : $field, array ('id' => 'rf-alias_' . $field)) }}</td>
          <td>{{ Form::select ('rf-cond[' . $field . ']', $field_condition, (isset ($reportfields[$field]['option'])) ? $reportfields[$field]['option'] : null, array ('id' => 'rf-cond_' . $field, 'class' => 'sel-cond')) }}</td>
          <td>
            {{ Form::input ('text', 'rf-val-1[' . $field .']', (isset ($reportfields[$field]['val1'])) ? $reportfields[$field]['val1'] : null, array ('id' => 'rf-val-1_' . $field, 'class' => 'rf-val-1', 'style' => 'display:none;')) }}
            <span class="rf-val-2-prefix" id="rf-val-2-prefix_{{ $field }}" style="display:none;">and</span>
            {{ Form::input ('text', 'rf-val-2[' . $field .']', (isset ($reportfields[$field]['val2'])) ? $reportfields[$field]['val2'] : null, array ('id' => 'rf-val-2_' . $field, 'class' => 'rf-val-2', 'style' => 'display:none;')) }}
          </td>
        </tr>
        @endif
        @endforeach

        @foreach ($core_fields as $field => $name)
        <tr class="report-field core">
          <td>{{ Form::checkbox ('rf-sel[' . $field . ']', '1', (isset($reportfields[$field])) ? true : false, array ('id' => 'rf-sel_' . $field)) }}
          {{ Form::label ('rf-sel_' . $field, $name) }}
          {{ Form::hidden ('rf-dn[' . $field . ']', $field) }}
          </td>
          <td>{{ Form::input ('text', 'rf-alias[' . $field . ']', (isset ($reportfields[$field]['alias'])) ? $reportfields[$field]['alias'] : null, array ('id' => 'rf-alias_' . $field)) }}</td>
          <td>{{ Form::select ('rf-cond[' . $field . ']', $field_condition, (isset ($reportfields[$field]['option'])) ? $reportfields[$field]['option'] : null, array ('id' => 'rf-cond_' . $field, 'class' => 'sel-cond')) }}</td>
          <td>
            {{ Form::input ('text', 'rf-val-1[' . $field .']', (isset ($reportfields[$field]['val1'])) ? $reportfields[$field]['val1'] : null, array ('id' => 'rf-val-1_' . $field, 'class' => 'rf-val-1', 'style' => 'display:none;')) }}
            <span class="rf-val-2-prefix" id="rf-val-2-prefix_{{ $field }}" style="display:none;">and</span>
            {{ Form::input ('text', 'rf-val-2[' . $field .']', (isset ($reportfields[$field]['val2'])) ? $reportfields[$field]['val2'] : null, array ('id' => 'rf-val-2_' . $field, 'class' => 'rf-val-2', 'style' => 'display:none;')) }}
          </td>
        </tr>
        @endforeach

      </table>
    </div>

    <fieldset id="report_email">
      <legend>Email settings</legend>
      <div>
        {{ Form::label ('em-fr', 'From: ') }}
        {{ Form::input ('text', 'em-fr', (isset ($report_email['from'])) ? $report_email['from'] : null, array ('size' => $text_size)) }}
        {{ $errors->first ('em-fr', '<span class="form-error">:message</span>') }}
      </div>

      <div>
        {{ Form::label ('em-to', 'To: ') }}
        {{ Form::input ('text', 'em-to', (isset ($report_email['to'])) ? (is_array ($report_email['to'])) ? implode (', ', $report_email['to']) : $report_email['to'] : null, array ('size' => $text_size)) }}
        {{ $errors->first ('em-to', '<span class="form-error">:message</span>') }}
      </div>

      <div>
        {{ Form::label ('em-sb', 'Subject: ') }}
        {{ Form::input ('text', 'em-sb', (isset ($report_email['subject'])) ? $report_email['subject'] : null, array ('size' => $text_size)) }}
        {{ $errors->first ('em-sb', '<span class="form-error">:message</span>') }}
      </div>

      <div>
        {{ Form::label ('em-bd', 'Message: ') }}
        {{ Form::textarea ('em-bd', (isset ($report_email['message'])) ? $report_email['message'] : null) }}
        {{ $errors->first ('em-bd', '<span class="form-error">:message</span>') }}
      </div>
    </fieldset>

    <div>
      {{ Form::submit ("Save", array('class' => 'button success')) }}
      {{ link_to_route ("report.index", "Cancel", null, array ("class" => "button")) }}
    </div>

    {{ Form::hidden ("referer", Request::header('referer')) }}

  {{ Form:: close () }}
</div>

@stop

@section('scripts')

{{ HTML::script('js/report_create.js'); }}

@stop