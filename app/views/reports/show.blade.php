@extends('layouts.front')

<?php 
  $email = ($report->email != null) ? json_decode ($report->email) : null;
  $reportjson = json_decode ($report->reportjson);
  $fields = $reportjson->fields;
?>

@section('content')
<div class="row">
  <div class="columns">
    
  <h3 class="b-page-heading"><strong>Lead Report: </strong> {{ $report->name }}</h3>
  <hr>
  <ul class="button-group">
    <li>{{ link_to_route ("report.index", "&laquo; Reports", null, array("class" => "button")) }}</li> 
    <li>{{ link_to_route ("report.edit", "Edit " . $report->name, $report->id, array("class" => "button alert")) }}</li> 
  </ul>

	<div class="panel group-detail">
    <div class="row"><strong class="columns medium-2">Name:</strong> <code class="columns medium-10">{{ $report->name }}</code></div><hr>
    <div class="row"><strong class="columns medium-2">Description:</strong> <span class="columns medium-10">{{ $report->description }}</span></div><hr>
    <div class="row"><strong class="columns medium-2">Status:</strong> <span class="columns medium-10"><span class="radius {{ $report->status }}-label">{{ $report->status }}</span> </span></div><hr>
    <div class="row"><strong class="columns medium-2">CSV filename:</strong> <code class="columns medium-10">{{ json_decode ($report->reportjson)->filename }} </code></div><hr>
    <div class="row"><strong class="columns medium-2">Last generated:</strong> <span class="columns medium-10"><?php $carbonite = new Carbon ($report->last_generated); ?>{{ $carbonite->format('l, Y F d, H:i:s'); }}</span></div><hr>
    <div class="row"><strong class="columns medium-2">Current time:</strong> <span class="columns medium-10"><?php $carbonow = Carbon::now (); ?>{{ $carbonow->format('l, Y F d, H:i:s'); }}</span></div>
  </div>

  @if (count($fields) > 0)

  <!--span class="label">Fields</span-->
  <table class="group--fields b-table">

    <tr>
      <th>Field</th>
      <th>Alias</th>
      <th>Condition</th>
      <th>Condition values</th>
    </tr>

    @foreach ($fields as $field => $properties)

    <tr>

      <td>{{ (isset ($properties->displayname) && $properties->displayname != '') ? $properties->displayname : $properties->alias }}</td>
      <td><code>{{ $properties->alias }}</code></td>
      <td>{{ (isset($properties->option) ? $properties->option : '') }}</td>
      <td>{{ (isset($properties->val1) ? '<code>' . $properties->val1 . '</code>' : '') }} {{ (isset($properties->val2) ? ' and <code>' . $properties->val2 .'</code>' : '') }}</td>
    </tr>

    @endforeach

  </table>

  @endif

  @if ($email != null)
  <div class="panel group-detail">
    <div class="row"><strong class="columns medium-2">From:</strong> <code class="columns medium-10">{{ $email->from }}</code></div><hr>
    <div class="row"><strong class="columns medium-2">To:</strong> <code class="columns medium-10">{{ is_array ($email->to) ? implode (', ', $email->to) : $email->to }}</code></div><hr>
    <div class="row"><strong class="columns medium-2">Subject:</strong> <span class="columns medium-10">{{ $email->subject }}</span></div><hr>
    <div class="row"><strong class="columns medium-2">Message:</strong> <span class="columns medium-10">{{ $email->message }}</span></div>
  </div>
  @endif

  <ul class="button-group">
    <li>{{ link_to_route ("report.index", "&laquo; Reports", null, array("class" => "button")) }}</li> 
    <li>{{ link_to_route ("report.edit", "Edit " . $report->name, $report->id, array("class" => "button alert")) }}</li> 
  </ul>

  </div>
</div>
@stop