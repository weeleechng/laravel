@extends ('layouts.front')

@section ('content')
  <div class="row">
    <div class="columns medium-6 medium-centered">
      <h4 class="text-center b-page-heading">Delete report: <strong>{{ $report->name }}</strong>?</h4><hr>

      {{ Form:: open (array('class' => 'report_delete', 'route' => array ('report.delete', $report->id), 'method' => 'delete' )) }}

        <div>
          {{ Form::submit ("Delete", array('class' => 'button alert')) }}
          {{ link_to_route ("report.index", "Cancel", null, ["class" => "button"]) }}
        </div>

      {{ Form:: close () }}

    </div>
  </div>
@stop
