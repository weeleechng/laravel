@extends('layouts.front')

@section('content')
<div class="row">
  <div class="columns">
    <h2 class="b-page-heading">Reports</h2><hr>

    <p>Scheduled compilation and posting of valid (pass) leads.</p>
    {{ link_to_route ("report.create", "Add Report", null, array ("class" => "button")) }}
    @if (count ($reports) > 0)

      <table class="b-table">
       <thead> 
        <tr>
          <th>Report Name</th>
          <th>Status</th>
          <th>Frequency</th>
          <th>Last Generated (GMT)</th>
          <th colspan="2">Actions</th>
        </tr>
      <thead>
      <tbody>
      @foreach ($reports as $report)
<?php
$frequency_array = $report->get_frequency_default ();
$generated = ($report->last_generated == null) ? null : new Carbon ($report->last_generated);
?>
        <tr>
          <td>{{ link_to_route ('report.show', $report->name, $report->id) }}<br/><sub>{{ $report->description }}</sub></td>
          <td><span class="label radius {{ $report->status }}-label">{{ $report->status }}</span></td>
          <td>{{ $frequency_array[json_decode ($report->reportjson)->frequency] }}</td>
          <td>{{ ($generated == null) ? '&mdash;' : $generated->format ('D Y-M-d H:i:s') }}</td>
          <td>
            {{ link_to_route ('report.edit', 'edit', $report->id) }}
          </td>
          <td>
            {{ link_to_route ('report.delete.confirm', 'delete', $report->id) }}
          </td>
        </tr>

      @endforeach
      </tbody>
      </table>

    @else

      <p>No lead reports available.</p>

    @endif

    <?php $carbonow = Carbon::now (); ?><sub>Server time: <code>{{ $carbonow->format('l, Y F d, H:i:s'); }}</code></sub>
  </div>
</div>  

@stop
