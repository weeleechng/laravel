@extends('layouts.standard')

@section('head_title')
Welcome
@stop

@section('content')
	<div class="welcome">
		<h1>Welcome, <span class="var">{{ $name }}</span>!</h1>
	</div>
@stop
