<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body style="font-family: Arial, Helvetica, sans-serif;">

    <div>
      {{ $report_name }} : there are no leads to report at this time.
    </div>

    <div>
      <strong>Lead Manager</strong><br/>
      <em>- estorm International</em>
    </div>

  </body>
</html>