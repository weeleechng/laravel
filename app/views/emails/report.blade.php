<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body style="font-family: Arial, Helvetica, sans-serif;">
    <pre style="font-family: Arial, Helvetica, sans-serif;">
{{ $email_body }}

You may also {{ link_to ($email_csv, 'download') }} the report file.

    </pre>
    <div>
      <strong>Lead Manager</strong><br/>
      <em>- estorm International</em>
    </div>

  </body>
</html>