@extends('layouts.standard')

@section('head_title')
Lead Manager v2014 - estorm International
@stop

@section('content')
	<div class="welcome">
		<h1>Lead Manager</h1>
    <p>v.2014 &copy; 2014 estorm International</p>
    <p>{{ link_to_route ("session.create", "Login") }} required</p>
	</div>
@stop
