@extends('layouts.default')

@section('content') 
<div class="row b-form">
    <div class="panel radius columns medium-6 medium-centered">
      <h3 class="b-form-heading text-center"><strong>Lead Manager <sup><small>v.2014</small></sup></strong></h3>
      <hr>
      {{ Session::get('auth_msg') }}
      @if($errors->has('auth_msg'))
      <div data-alert class="alert-box alert">
        {{ $errors->first('auth_msg') }}
        <a href="" class="close">×</a>
      </div>
      @endif
      {{ Form::open(array('route' => 'users.login')) }}
        <div class="row">
          <div class="columns medium-10 medium-centered @if($errors->has('id')) error @endif">
            {{ Form::label('id', 'Username') }}
            {{ Form::text('id') }}
            {{ $errors->first('id', '<small class="error">:message</small>') }}
          </div>
        </div>
        <div class="row">
          <div class="columns medium-10 medium-centered @if($errors->has('password')) error @endif">
            {{ Form::label('password', 'Password') }}
            {{ Form::password('password') }}
            {{ $errors->first('password', '<small class="error">:message</small>') }}
          </div>
        </div>
        <div class="row">
          <div class="columns medium-10 medium-centered">
            {{ Form::submit('Log In', array('class' => 'button expand')) }}
          </div>
        </div>
      {{ Form::close() }}
    </div>
</div>
@stop