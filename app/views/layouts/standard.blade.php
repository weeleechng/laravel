<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('head_title')</title>
	{{ HTML::style('css/standard.css'); }}
  {{ HTML::script('http://code.jquery.com/jquery-1.11.1.min.js'); }}
  @yield('head_tags')
<body>
  <div class="top-bar">
    <div class="top-bar--content top-bar--user">
      {{ Auth::user() ? link_to_route ("user.get", Auth::user()->name, Auth::user()->id) . ' &nbsp; | &nbsp; ' . link_to_route ("session.destroy", "Logout") : link_to_route ("session.create", "Log In") }}
    </div>
    <div class="top-bar--content top-bar--menu">

      <div class="menu--item">
        {{ link_to_route ("home", "Home") }}
      </div>

      @if (Auth::user())

      <div class="menu--item">
        {{ link_to ("temp", "Leads") }}
      </div>
      <div class="menu--item">
        {{ link_to_route ("groups.index", "Groups") }}
      </div>
      <div class="menu--item">
        {{ link_to ("temp", "Views") }}
      </div>
      <div class="menu--item">
        {{ link_to ("temp", "Reports") }}
      </div>
      <div class="menu--item">
        {{ link_to_route ("users.index", "Users") }}
      </div>
      <div class="menu--item">
        {{ link_to_route ("roles.index", "Roles") }}
      </div>

      @endif

      <div class="menu--item">
        {{ link_to_route ("about", "About") }}
      </div>

    </div>
  </div>

  <div class="flashmessage">
    @yield('top_bar_message')
  </div>

  <div class="content">

	  @yield('content')

  </div>
  
  @yield('scripts')
  
</body>
</html>
