<!DOCTYPE html>
<!-- App Environment: {{ App::environment() }} -->
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="{{ asset ('favicon.png') }}" type="image/png" />
  <title>Lead Manager @if( isset($page_title) ) {{ '- '. $page_title }} @endif</title>

    <!-- Foundation -->
	{{ HTML::style('bower_components/foundation/css/normalize.css'); }}
	{{ HTML::style('bower_components/foundation/css/foundation.css'); }}
	{{ HTML::style('css/main.css'); }}
	
	{{ HTML::script('bower_components/modernizr/modernizr.js'); }}
</head>
<body>
	@yield('navigation')
	@yield('content')

{{ HTML::script('bower_components/jquery/dist/jquery.min.js'); }}
{{ HTML::script('bower_components/foundation/js/foundation.min.js'); }}
	
	@yield('scripts')

<script>
  $(document).foundation();
</script>
</body>
</html>