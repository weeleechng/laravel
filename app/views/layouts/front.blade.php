@extends('layouts.default')

@section('navigation')
@if (Auth::user())
<!-- Navigation -->
<nav class="top-bar" data-topbar>
	<ul class="title-area">
		<li class="name">
			<h1>{{ HTML::linkRoute('home', 'LM v.2014') }}</h1>
		</li>

		<li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
	</ul>
	<section class="top-bar-section">
		<ul class="left">
			<li class="@if( isset($menu) && in_array('home', $menu) ) active @endif">{{ HTML::linkRoute('home', 'Summary') }}</li>
			<li class="has-dropdown @if( isset($menu) && in_array('group', $menu) ) active @endif">{{ HTML::linkRoute('groups.index', 'Groups') }}
				<ul class="dropdown">
					<li class="@if( isset($menu) && in_array('new_group', $menu) ) active @endif">{{ HTML::linkRoute('groups.create', 'Add New') }}</li>
					<li class="@if( isset($menu) && in_array('manage_group', $menu) ) active @endif">{{ HTML::linkRoute('groups.index', 'Manage Groups') }}</li>
				</ul>
			</li>
			<li class="has-dropdown @if( isset($menu) && in_array('all_leads', $menu) ) active @endif">{{ HTML::linkRoute('lead.index', 'Leads') }}
				<ul class="dropdown">
					<li>{{ HTML::linkRoute('lead.index', 'Valid Leads') }}</li>
					<li>{{ HTML::linkRoute('lead.index', 'Pending Leads', ['where', 'status', 'queued']) }}</li>
					<li>{{ HTML::linkRoute('lead.index', 'Invalid Leads', ['where', 'status', 'fail,duplicate']) }}</li>
					<li>{{ HTML::linkRoute('lead.index', 'Test Leads', ['where', 'istestlead', 'true']) }}</li>
					<li>{{ HTML::linkRoute('lead.cron', 'Lead cron', null, array ('target' => '_blank', 'class' => 'link-cron-execute')) }}</li>
				</ul>
			</li>
			<!--li class="has-dropdown"><a href="#">Views</a>
				<ul class="dropdown">
					<li><a href="#">Add New</a></li>
					<li><a href="#">Manage Views</a></li>
				</ul>
			</li-->
			<li class="has-dropdown @if( isset($menu) && in_array('reports', $menu) ) active @endif">{{ HTML::linkRoute('report.index', 'Reports') }}
				<ul class="dropdown">
					<li>{{ HTML::linkRoute('report.create', 'Add New') }}</li>
					<li>{{ HTML::linkRoute('report.index', 'Manage Reports') }}</li>
					<li>{{ HTML::linkRoute('report.cron', 'Report cron', null, array ('target' => '_blank', 'class' => 'link-cron-execute')) }}</li>
				</ul>
			</li>
		</ul>

		<ul class="right">
			<li class="@if( isset($menu) && in_array('settings', $menu)) active @endif has-dropdown">
				<a href="#">Settings</a>
				<ul class="dropdown">
					<li class="@if( isset($menu) && in_array('manage_users', $menu)) active @endif">{{ HTML::linkRoute('users.index', 'Manage Users') }}</li>
					<li class="@if( isset($menu) && in_array('manage_roles', $menu)) active @endif" >{{ HTML::linkRoute('roles.index', 'Manage Roles') }}</li>
				</ul>
			</li>
			<li class="has-form"><a>Hi, @if(Auth::check()) {{ Auth::user()->id; }} @endif</a></li>
			<li class="has-form">{{ HTML::linkRoute("session.destroy", "Logout", '', array('class' => 'button')) }}</li>
		</ul>
	</section>
</nav>

@endif

@if(Session::get('flashmessage'))
<div data-alert class="alert-box {{ Session::get('flashmessage.class') }}">
	{{ Session::get('flashmessage.message') }}
	<a href="" class="close">×</a>
</div> 
@endif

@stop

@section ('scripts')
{{ HTML::script('js/confirm_cron.js'); }}
@stop