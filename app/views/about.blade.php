@extends('layouts.standard')

@section('head_title')
About Lead Manager v.2014
@stop

@section('content')
	<div>
		<h1>About Lead Manager v.2014</h1>
    <p>Powered by {{ link_to ("http://www.laravel.com", "Laravel", array ("target" => "_blank", )) }} and {{ link_to ("http://www.postgresql.org", "PostgreSQL", array ("target" => "_blank", )) }}, estorm International's Lead Manager system is back &mdash; with a vengance!</p>
	</div>
@stop
