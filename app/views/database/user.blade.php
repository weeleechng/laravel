@extends('layouts.standard')
@section('head_title')
LMv2014 - {{ $user->name }}
@stop

@section('content')
	<h1>{{ $user->name }}</h1>
	<div class="system">
		<span class="label">UserId:</span> {{ $user->id }}<br />
    <span class="label">Email:</span> {{ $user->email }}<br />
    <span class="label">Status:</span> {{ $user->status }}<br />
  </div>
@stop