@extends('layouts.standard')

@section('head_title')
LMv2014 Users
@stop

@section('content')

	<h1>Active Users</h1>

	@if (count ($users) > 0)

	  @foreach ($users as $user)
	    <div>
	    	{{ link_to ("/users/{$user->id}", $user->id) }} : {{ $user->name }} &lt;{{ $user->email }}&gt;
	    </div>
	  @endforeach

	@else

	  <p>No active users found</p>

	@endif

	<h2>Inactive Users</h2>

	@if (count ($blocked) > 0)

	  @foreach ($blocked as $block)
	    <div>
	    	{{ link_to ("users/{$block->id}", $block->id) }} : {{ $block->name }} &lt;{{ $block->email }}&gt;
	    </div>
	  @endforeach

	@else

	  <p>No blocked users found</p>

	@endif

@stop
