@extends('layouts.front')

@section('content')
<div class="row">
  <div class="columns">
    
  <h3 class="b-page-heading"><strong>Lead:</strong> 
    {{ $lead->lastname . ' ' . $lead->firstname }} 
    @if ($lead->istestlead == true)
      <code>[TEST]</code>
    @endif 
    @if ($lead->status != 'pass')
      <code>[{{ $lead->status }}]</code>
    @endif
  </h3>
  <hr>

  <ul class="button-group">
    <li>{{ link_to_route ("lead.index", "&laquo; All Leads", null, array("class" => "button small")) }}</li> 
    @if (in_array ($lead->status, ['fail', 'duplicate']))
    <li>{{ link_to_route ("lead.index", "&laquo; Invalid Leads", array ('where', 'status', 'fail,duplicate'), array("class" => "button small alert")) }}</li> 
    @endif
    @if ($lead->status == 'queued')
    <li>{{ link_to_route ("lead.index", "&laquo; Pending Leads", array ('where', 'status', 'queued'), array("class" => "button small alert")) }}</li> 
    @endif
    @if ($lead->istestlead == true)
    <li>{{ link_to_route ("lead.index", "&laquo; Test Leads", array ('where', 'istestlead', 'true'), array("class" => "button small alert")) }}</li> 
    @endif
  </ul>

  <table class="lead--fields b-table">
    <tr>
      <th>Display name</th>
      <th>Field name</th>
      <th>Value</th>
    </tr>

    @foreach ($fields as $fieldname => $value)
    <tr>
      <td>
        @if (isset ($displaynames[$fieldname])) 
        {{ $displaynames[$fieldname] }} 
        @else 
        {{ $fieldname }} 
        @endif</td>
      <td><code>{{ $fieldname }}</code></td>
      <td>{{ $value }}</td>
    </tr>
    @endforeach
  </table>

  <table class="b-table">
    <tr>
      <td>
        <strong>Test lead:</strong> {{ ($lead->istestlead == true) ? '<code>YES</code>' : 'no' }}<br />
        <strong>Lead Group:</strong> <?php $leadgroup = Group::find ($lead->group); ?>{{ $leadgroup->get_name() }} : <code>{{ $leadgroup->id }}</code><br />
        <strong>UUID:</strong> {{ $lead->id }}<br />
        <strong>Created:</strong> {{ $lead->created_at }}<br />
        <strong>IP address:</strong> {{ $lead->ipaddress }}<br />
        @if (isset ($lead->urlsource) && $lead->urlsource != '') <strong>Source URL:</strong> {{ $lead->urlsource }}<br /> @endif
        @if (isset ($lead->useragent)) <strong>User Agent String:</strong> {{ $lead->useragent }}<br /> @endif
        @if (isset ($leadgroup->external) && $leadgroup->external != '') <strong>External POST URL:</strong> {{ $leadgroup->external }}<br /> @endif
        @if ($lead->extlog != '') 
          <?php $log = json_decode($lead->extlog); ?> 
          <strong>External log:</strong> {{ $log->type }} - {{ $log->logs }} 
        @endif
        @if ($lead->remarks != '') 
          <?php $remarks = json_decode($lead->remarks, true); ?>
          <strong>Remarks:</strong><br /> 
          @foreach ($remarks as $key => $value)
            <code>{{ $key }} : @if (is_array ($value)) {{ implode ($value, ', ') }} @else {{ $value }} @endif</code> <br/>
          @endforeach
        @endif
      </td>
    </tr>
  </table>

  <hr>

  <ul class="button-group">
    <li>{{ link_to_route ("lead.index", "&laquo; All Leads", null, array("class" => "button small")) }}</li> 
    @if (in_array ($lead->status, ['fail', 'duplicate']))
    <li>{{ link_to_route ("lead.index", "&laquo; Invalid Leads", array ('where', 'status', 'fail,duplicate'), array("class" => "button small alert")) }}</li> 
    @endif
    @if ($lead->status == 'queued')
    <li>{{ link_to_route ("lead.index", "&laquo; Pending Leads", array ('where', 'status', 'queued'), array("class" => "button small alert")) }}</li> 
    @endif
    @if ($lead->istestlead == true)
    <li>{{ link_to_route ("lead.index", "&laquo; Test Leads", array ('where', 'istestlead', 'true'), array("class" => "button small alert")) }}</li> 
    @endif
  </ul>

  </div>
</div>
@stop