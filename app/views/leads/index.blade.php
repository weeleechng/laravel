@extends('layouts.front')
<?php

  function build_link_orderby_param_array ($columname, $param_array)
  {
    $out_array = $param_array;
    
    if (in_array ('orderby', $out_array))
    {
      $key = array_search ('orderby', $out_array);
      $out_array[$key + 1] = $columname;
      if (isset ($out_array[$key + 2]) && $out_array[$key + 2] == 'desc')
      {
        $out_array[$key + 2] = 'asc';
      }
      else
      {
        $out_array[$key + 2] = 'desc';
      }
    }
    else
    {
      $out_array[] = 'orderby';
      $out_array[] = $columname;
      $out_array[] = 'desc';
    }

    return $out_array;

  }
  
  $url_segments = Request::segments ();

  $param_array = [];
  for ($i = 1; $i < count ($url_segments); $i++)
  {
    $param_array[] = $url_segments[$i];
  }

  $emailname_param = build_link_orderby_param_array ('email', $param_array);
  $contactno_param = build_link_orderby_param_array ('contactno', $param_array);
  $group_param = build_link_orderby_param_array ('group', $param_array);
  $datetime_param = build_link_orderby_param_array ('created_at', $param_array);
  $status_param = build_link_orderby_param_array ('status', $param_array);
  $ipaddress_param = build_link_orderby_param_array ('ipaddress', $param_array);
  
?>
@section('content')
<div class="row">
  <div class="columns">
    <h2 class="b-page-heading">{{ $page_title }}</h2>
      @if (isset ($description)) <p>{{ $description }}</p> @endif
      <hr>
    @if (count ($leads) > 0)
      {{ $leads->links () }}
      <table class="b-table">
       <thead> 
        <tr>
          <th>{{ link_to_route ('lead.index', 'Email &amp; Name', $emailname_param) }}</th>
          @if ($status != 'invalid') <th>{{ link_to_route ('lead.index', 'Contact no.', $contactno_param) }}</th> @endif
          <th>{{ link_to_route ('lead.index', 'Group', $group_param) }}</th>
          <th>{{ link_to_route ('lead.index', 'Date Time', $datetime_param) }}</th>
          <th>{{ link_to_route ('lead.index', 'IP Address', $ipaddress_param) }}</th>
          @if ($status == 'invalid') 
          <th>{{ link_to_route ('lead.index', 'Status', $status_param) }}</th> 
          @endif
          <th>Actions</th>
        </tr>
      <thead>
      <tbody>
      @foreach ($leads as $lead) <?php $leadgroup = Group::find ($lead->group); ?>
        <tr>
          <td>{{ $lead->lastname . ' ' . $lead->firstname }}<br />
            <code>{{ $lead->email }}</code></td>
          @if ($status != 'invalid') <td>{{ $lead->contactno }}</span></td> @endif
          <td>{{ link_to_route ('lead.index', $leadgroup->get_name(), array ('where', 'group', $leadgroup->id)) }}</td>
          <td>{{ str_replace (' ', '<br/>', $lead->created_at) }}</td>
          <td>{{ str_replace (' ', '<br/>', $lead->ipaddress) }}</td>
          @if ($status == 'invalid') 
          <td><code>{{ $lead->status }}</code></td> 
          @endif
          <td>
            {{ link_to_route ("lead.show", "details", $lead->id) }} &nbsp; 
            @if ($status == null && $leadgroup->external != '') {{ link_to_route ("lead.external", "repost", $lead->id, array ('title' => 'Perform external post', 'target' => '_blank')) }} @endif
            @if ($status == 'pending') {{ link_to_route ("lead.cron.selected", "cron", $lead->id, array ('title' => 'Process this lead', 'target' => '_blank') ) }} @endif
          </td>
        </tr>
      @endforeach
      </tbody>
      </table>
    {{ $leads->links () }}
    @else
      <p>No @if ($status != null) {{ $status }} @endif leads found</p>
    @endif
  </div>
</div>  

@stop
