<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* is User ID an administrator? */
function is_admin ($id)
{
  $obj_user = new User ();

  return $obj_user->is_admin($id);
}

/* Route filter: manage_users permission */
Route::filter ('canManageUsers', function ($route)
{
  $loggedinuser = Auth::user();

  /* user 'admin' can bypass this filter */
  if (is_admin ($loggedinuser->id))
    return;

  $param = $route->getParameter('id');
  $permissions = json_decode ($loggedinuser->userrole->permissionjson);
  
  /* non-user managers can only edit their own user account */
  if ($permissions->manage_users == "false" && $param != $loggedinuser->id)
  {
    return View::make ('message')->with([
      'head_title' =>'You shall not pass', 
      'message' => 'You do not have sufficient privileges to access or manage user accounts.',
    ]);
  }

});

/* Route filter: access lead group */
Route::filter ('canAccessLeadGroup', function ($route)
{
  $loggedinuser = Auth::user();

  /* user 'admin' can bypass this filter */
  if (is_admin ($loggedinuser->id))
    return;

  $permissions = json_decode ($loggedinuser->userrole->permissionjson);

  if ($permissions->access_lead_group == "false")
  {
    return View::make ('message')->with([
      'head_title' =>'You shall not pass', 
      'message' => 'You do not have sufficient privileges to view Lead Groups.',
    ]);
  }

});

/* Route filter: manage lead group */
Route::filter ('canManageLeadGroup', function ($route)
{
  $loggedinuser = Auth::user();

  /* user 'admin' can bypass this filter */
  if (is_admin ($loggedinuser->id))
    return;

  $permissions = json_decode ($loggedinuser->userrole->permissionjson);

  if ($permissions->access_lead_group == "false")
  {
    return View::make ('message')->with([
      'head_title' =>'You shall not pass', 
      'message' => 'You do not have sufficient privileges to manage Lead Groups.',
    ]);
  }

});

/* Route filter: access leads */
Route::filter ('canAccessLead', function ($route)
{
  $loggedinuser = Auth::user();

  /* user 'admin' can bypass this filter */
  if (is_admin ($loggedinuser->id))
    return;

  $permissions = json_decode ($loggedinuser->userrole->permissionjson);

  if ($permissions->access_lead == "false")
  {
    return View::make ('message')->with([
      'head_title' =>'You shall not pass', 
      'message' => 'You do not have sufficient privileges to view Leads.',
    ]);
  }

});

/* Route filter: access lead reports */
Route::filter ('canAccessLeadReport', function ($route)
{
  $loggedinuser = Auth::user();

  /* user 'admin' can bypass this filter */
  if (is_admin ($loggedinuser->id))
    return;

  $permissions = json_decode ($loggedinuser->userrole->permissionjson);

  if ($permissions->access_lead_report == "false")
  {
    return View::make ('message')->with([
      'head_title' =>'You shall not pass', 
      'message' => 'You do not have sufficient privileges to view Lead Reports.',
    ]);
  }

});

/* Route filter: manage lead report */
Route::filter ('canManageLeadReport', function ($route)
{
  $loggedinuser = Auth::user();

  /* user 'admin' can bypass this filter */
  if (is_admin ($loggedinuser->id))
    return;

  $permissions = json_decode ($loggedinuser->userrole->permissionjson);

  if ($permissions->manage_lead_report == "false")
  {
    return View::make ('message')->with([
      'head_title' =>'You shall not pass', 
      'message' => 'You do not have sufficient privileges to manage Lead Reports.',
    ]);
  }

});


Route::get ('home', array ('as' => 'home', 'uses' => 'PagesController@house')); // this will become the administration home page
// Route::get ('home', array ('uses' => 'PagesController@house')); // this is an alias of the home page
Route::get ('about', array ('as' => 'about', 'uses' => 'PagesController@about'));
Route::get ('temp', array ('as' => 'temp', 'uses' => 'PagesController@temp'));

/* Authentication and Session Management */
Route::get ('/', array ('as' => 'session.create', 'uses' => 'SessionsController@create')); // Set this as first page
Route::get ('/login', array ('as' => 'session.create', 'uses' => 'SessionsController@create')); // Set this as first page
Route::post ('users/login', array ('as' => 'users.login', 'uses' => 'UsersController@login'));
Route::get ('logout', array ('as' => 'session.destroy', 'uses' => 'SessionsController@destroy'));

/* User Management */
Route::get ('users', array ('as' => 'users.index', 'uses' => 'UsersController@index'))->before('auth|canManageUsers');
Route::get ('users/status/{status}', array ('as' => 'users.all.bystatus', 'uses' => 'UsersController@bystatus'))->before('auth|canManageUsers');
Route::get ('users/{id}', array ('as' => 'user.get', 'uses' => 'UsersController@show'))->before('auth|canManageUsers');
Route::post ('user/store', array ('as'=> 'user.store', 'uses' => 'UsersController@store'))->before('auth|canManageUsers');
Route::get ('user/create', array ('as' => 'user.create', 'uses' => 'UsersController@create'))->before('auth|canManageUsers');
Route::get ('user/{id}/edit', array ('as' => 'user.edit', 'uses' => 'UsersController@edit'))->before('auth|canManageUsers');
Route::post ('user/{id}/update', array ('as'=> 'user.update', 'uses' => 'UsersController@update'))->before('auth|canManageUsers');
Route::get ('user/{id}/delete/confirm', array ('as'=> 'user.delete.confirm', 'uses' => 'UsersController@confirmdestroy'))->before('auth|canManageUsers');
Route::delete ('user/{id}/delete', array ('as'=> 'user.delete', 'uses' => 'UsersController@destroy'))->before('auth|canManageUsers');

/* User Role Management */
Route::get ('roles', array ('as' => 'roles.index', 'uses' => 'RoleController@index'))->before('auth|canManageUsers');
Route::get ('roles/{id}', array ('as' => 'roles.get', 'uses' => 'RoleController@show'))->before('auth|canManageUsers');
Route::get ('role/create', array ('as' => 'roles.create', 'uses' => 'RoleController@create'))->before('auth|canManageUsers');
Route::post ('role/store', array ('as'=> 'roles.store', 'uses' => 'RoleController@store'))->before('auth|canManageUsers');
Route::get ('role/{id}/edit', array ('as' => 'roles.edit', 'uses' => 'RoleController@edit'))->before('auth|canManageUsers');
Route::post ('role/{id}/update', array ('as'=> 'roles.update', 'uses' => 'RoleController@update'))->before('auth|canManageUsers');
Route::get ('role/{id}/delete/confirm', array ('as'=> 'role.delete.confirm', 'uses' => 'RoleController@confirmdestroy'))->before('auth|canManageUsers');
Route::delete ('role/{id}/delete', array ('as'=> 'role.delete', 'uses' => 'RoleController@destroy'))->before('auth|canManageUsers');

/* Lead Groups aka campaigns */
Route::get ('groups', array ('as' => 'groups.index', 'uses' => 'GroupController@index'))->before('auth|canAccessLeadGroup');
Route::get ('groups/{id}', array ('as' => 'groups.get', 'uses' => 'GroupController@show'))->before('auth|canAccessLeadGroup');
Route::get ('groups/{id}/sampleform', array ('as' => 'groups.sampleform', 'uses' => 'GroupController@generate_form'));
Route::get ('group/{id}/edit', array ('as' => 'groups.edit', 'uses' => 'GroupController@edit'))->before('auth|canManageLeadGroup');
Route::get ('group/create', array ('as' => 'groups.create', 'uses' => 'GroupController@create'))->before('auth|canManageLeadGroup');
Route::get ('group/clone/{id}', array ('as' => 'groups.clone', 'uses' => 'GroupController@create_clone'))->before('auth|canManageLeadGroup');
Route::post ('group/store', array ('as' => 'groups.store', 'uses' => 'GroupController@store'))->before('auth|canManageLeadGroup');
Route::post ('group/{id}/update', array ('as' => 'groups.update', 'uses' => 'GroupController@update'))->before('auth|canManageLeadGroup');
Route::get ('group/{id}/delete/confirm', array ('as'=> 'groups.delete.confirm', 'uses' => 'GroupController@confirmdestroy'))->before('auth|canManageLeadGroup');
Route::delete ('group/{id}/delete', array ('as'=> 'groups.delete', 'uses' => 'GroupController@destroy'))->before('auth|canManageLeadGroup');

/* Lead */
Route::post ('lead/entry', array ('as' => 'lead.store', 'uses' => 'LeadController@store'));
Route::get ('lead/cron', array ('as' => 'lead.cron', 'uses' => 'LeadController@lead_cron'));
Route::get ('lead/cron/{id}', array ('as' => 'lead.cron.selected', 'uses' => 'LeadController@lead_cron_selected'))->before('auth|canAccessLead');
Route::get ('lead/external/{id}', array ('as' => 'lead.external', 'uses' => 'LeadController@post_to_external'));
Route::get ('lead/show/{id}', array ('as' => 'lead.show', 'uses' => 'LeadController@show'))->before('auth|canAccessLead');
Route::get ('leads/{op1?}/{op1_column?}/{op1_value?}/{op2?}/{op2_column?}/{op2_value?}', array ('as' => 'lead.index', 'uses' => 'LeadController@index'))->before('auth|canAccessLead');

/* Reports */
Route::get ('report/cron', array ('as' => 'report.cron', 'uses' => 'ReportController@report_cron'));
Route::get ('reports', array ('as' => 'report.index', 'uses' => 'ReportController@index'))->before('auth|canAccessLeadReport');
Route::get ('report/{id}/show', array ('as' => 'report.show', 'uses' => 'ReportController@show'))->before('auth|canAccessLeadReport');
Route::get ('report/create', array ('as' => 'report.create', 'uses' => 'ReportController@create'))->before('auth|canManageLeadReport');
Route::post ('report/store', array ('as' => 'report.store', 'uses' => 'ReportController@store'))->before('auth|canManageLeadReport');
Route::get ('report/{id}/edit', array ('as' => 'report.edit', 'uses' => 'ReportController@edit'))->before('auth|canManageLeadReport');
Route::post ('report/{id}/update', array ('as' => 'report.update', 'uses' => 'ReportController@update'))->before('auth|canManageLeadReport');
Route::get ('report/{id}/delete/confirm', array ('as' => 'report.delete.confirm', 'uses' => 'ReportController@confirmdestroy'))->before('auth|canManageLeadReport');
Route::delete ('report/{id}/delete', array ('as' => 'report.delete', 'uses' => 'ReportController@destroy'))->before('auth|canManageLeadReport');
Route::get ('report/download/file/{filename}', ['as' => 'report.download', 'uses' => 'ReportController@report_download_file']);

/* Cron (serial) */
Route::get ('system/cron', array ('as' => 'system.cron', 'uses' => 'CronController@cron'));

/* utilities */
Route::get ('hash/make/{pw}', function ($pw) { if (!isset($pw)) return ""; return Hash::make ($pw); });
Route::get ('guid/make', function () { return Guid::make(); });
