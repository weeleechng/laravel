<?php

class LeadValidator extends Illuminate\Validation\Validator
{
  private $json_column_regex = "/^\w+->>\'(\w+)\'$/";

  private function column_or_json ($field)
  {
    preg_match ($this->json_column_regex, $field, $match);
    
    // JSON column notations cannot be enclosed in double quotes
    if (isset ($match[1]))
    {
      return $field;
    }
    else
    {
      return '"' . $field . '"';
    }
  }

  private function restore_commas ($input)
  {
    return str_replace ('{..}', ',', $input);
  }

  /* 
    Custom unique validator for core and JSON field in Leads.
    Binding will not work for PostgreSQL JSON column selections, 
    so the query has to be hard-coded.
    
    $params:
    [0]: table
    [1]: column to check for $value
    [2]: column to check for NOT value (<>)
    [3]: NOT value
    [4]: additional column to check
    [5]: additional value
    [6]: additional column to check
    [7]: additional value
    [8]: ...
    [9]: ...

   */
  public function validateUniqueInGroup ($field, $value, $params)
  {
    $this->column_or_json ("email");

    $query = 'select count(*) as aggregate from ' . $this->column_or_json ($params[0]) . ' where ' . $this->column_or_json ($params[1]) . ' = :fieldvalue';
    if (strtolower($params[2]) != 'null')
    {
      $query .= ' and ' . $this->column_or_json ($params[2]) . ' <> :param2value';
    }

    $additional_index = 4;
    $keys_values = [];
    while (isset ($params[$additional_index]) && isset ($params[$additional_index + 1]))
    {
      $query .= ' and ' . $this->column_or_json ($params[$additional_index]) . ' = :param' . $additional_index . 'value ';
      $keys_values['param' . $additional_index . 'value'] = $params[$additional_index + 1];
      $additional_index += 2; 
    }

    $keys_values['fieldvalue'] = $value; 
    $keys_values['param2value'] = $params[3];

    $result = DB::select ($query, $keys_values);

    if ($result[0]->aggregate > 1)
    {
      return false;
    }
    else
    {
      return true;
    }

  }

  /* 
    Custom unique-with validator. Check to ensure the value-pair is unique.
    Binding will not work for PostgreSQL JSON column selections, 
    so the query has to be hard-coded.
    
    $params:
    [0]: table
    [1]: column to check for $value
    [2]: column to check for NOT value (<>)
    [3]: NOT value
    [4]: additional column to check
    [5]: additional value
    [6]: additional column to check
    [7]: additional value
    [8]: ...
    [9]: ...

   */
  public function validateUniqueWith ($field, $value, $params)
  {
    if (count ($params) == 0)
    {
      // rule string passed as $params only for first field with unique_with validation
      // don't need to repeat this for every field
      return true;
    }

    $query = 'select count(*) as aggregate from ' . $params[0] . ' where ' . $this->column_or_json ($params[1]) . ' <> :fieldvalue';
    $additional_index = 3;
    $keys_values = [];
    while (isset ($params[$additional_index]) && isset ($params[$additional_index + 1]))
    {
      $query .= ' and ' . $this->column_or_json ($params[$additional_index]) . ' = :param' . $additional_index . 'value ';
      $keys_values['param' . $additional_index . 'value'] = $this->restore_commas ($params[$additional_index + 1]);
      $additional_index += 2; 
    }

    $keys_values['fieldvalue'] = $params[2];
    $result = DB::select ($query, $keys_values);

    if ($result[0]->aggregate > 1)
    {
      return false;
    }
    else
    {
      return true;
    }
  }

  public function validateArrayEmail ($field, $value, $params)
  {
    $returnval = true;

    if (is_array ($value))
    {
      foreach ($value as $index => $email)
      {
        if (!filter_var ($email, FILTER_VALIDATE_EMAIL))
        {
          $returnval = false;
        }
      }
    }
    else
    {
      if (!filter_var($value, FILTER_VALIDATE_EMAIL))
      {
        $returnval = false;
      }
    }

    return $returnval;
    
  }

}
