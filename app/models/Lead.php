<?php

class Lead extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'lead';

	public $error_messages;

	public $field_rules;
	public $field_rules_messages;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public static $lead_system_rules = array 
	(
		'lead_group' => 'required',
	);

	public static $lead_system_rules_messages = array 
	(
		'lead_group.required' => 'Lead Group identifer is required',
	);

	private function build_unique_rule_array ($group_jsonfields, $inputdata)
	{
		$this->field_rules = [];
		$this->field_rules_messages = [];

		foreach ($group_jsonfields as $group_field => $properties)
		{
			if (isset ($properties['rules']))
			{
				foreach ($properties['rules'] as $rule)
				{
					// Use customised unique validator for leads to validate JSON field values:
					if ($rule['baserule'] == 'unique')
					{
						$rule['baserule'] = 'unique_in_group'; // app/validators/LeadValidator.php
						// the $rule['rule'] string is the same as the Laravel unique rule string
					}

					if ($rule['baserule'] == 'unique_with' && isset ($rule['rule']))
					{
						// replace {fieldvalue} in validation rules with values from form
						$ruleparts = explode (',', $rule['rule']);
						foreach ($ruleparts as $key => $item)
						{
							if ($item == '{fieldvalue}')
							{
								// discard datajson->> notation
								preg_match ("/^\w+->>\'(\w+)\'$/", $ruleparts[($key - 1)], $matches);
								if (isset ($matches[1]))
								{
									$fieldname = $matches[1];
								}
								else
								{
									$fieldname = $ruleparts[($key - 1)];
								}

								// obtain form value from $form_field, escape commas into {..}
								$ruleparts[$key] = str_replace (',', '{..}', $inputdata[$fieldname]);
							}
						}

						// rebuild rule string
						$rule['rule'] = implode ($ruleparts, ',');
					}

					// actual leads will not be checked against test leads, and vice versa
					if (in_array ($rule['baserule'], ['unique_in_group', 'unique_with']) && isset ($rule['rule']))
					{
						if (isset ($inputdata['lead_test']) && $inputdata['lead_test'] == '1')
						{
							$rule['rule'] .= ',istestlead,true';
						}
						else
						{
							$rule['rule'] .= ',istestlead,false';	
						}
						// Log::info($rule['rule']);

						if (isset ($this->field_rules[$group_field]))
						{
							$this->field_rules[$group_field] .= '|';
						}
						else
						{
							$this->field_rules[$group_field] = '';	
						}

						$this->field_rules[$group_field] .= $rule['baserule'];

						if (isset ($rule['rule']))
						{
							$this->field_rules[$group_field] .= ':' . $rule['rule'];
						}

						if (isset ($rule['message']))
						{
							$this->field_rules_messages[$group_field . '.' . $rule['baserule']] = $rule['message'];
						}

					}
					// the other validation rules have been executed during lead creation

				}
			}
		}
	}

	private function build_rules_array ($group_jsonfields, $inputdata)
	{

		$this->field_rules = [];
		$this->field_rules_messages = [];

		foreach ($group_jsonfields as $group_field => $properties)
		{
			if (isset ($properties['rules']))
			{
				foreach ($properties['rules'] as $rule)
				{
					// check unique validation only in cron job
					// honeypot rule has been processed earlier
					if (in_array ($rule['baserule'], ['unique', 'unique_in_group', 'unique_with', 'honeypot']))
					{
						continue;
					}

					if (isset ($this->field_rules[$group_field]))
					{
						$this->field_rules[$group_field] .= '|';
					}
					else
					{
						$this->field_rules[$group_field] = '';	
					}

					$this->field_rules[$group_field] .= $rule['baserule'];

					if (isset ($rule['rule']))
					{
						$this->field_rules[$group_field] .= ':' . $rule['rule'];
					}

					if (isset ($rule['message']))
					{
						$this->field_rules_messages[$group_field . '.' . $rule['baserule']] = $rule['message'];
					}

				}
			}
		}
	}

	public function isValid ($inputdata)
	{
		$validation = Validator::make ($inputdata, static::$lead_system_rules, static::$lead_system_rules_messages);

		if ($validation->passes())
		{
			return true;
		}
		else
		{
			$this->error_messages = $validation->messages ();
			return false;
		}

	}

	public function isValidFields ($inputdata, $group_jsonfields)
	{
		$this->build_rules_array ($group_jsonfields, $inputdata);
		$validation = Validator::make ($inputdata, $this->field_rules, $this->field_rules_messages);

		if ($validation->passes ())
		{
			return true;
		}
		else
		{
			// dd ($validation->messages ());
			$this->error_messages = $validation->messages ();
			return false;
		}
	}

	public function isValidUnique ()
	{
		$inputdata = json_decode ($this->datajson, true);
		if ($this->istestlead == true)
		{
			$inputdata['lead_test'] = '1';
		}

		/* get rule from cache, or dB */
		$group_rulejson = (Cache::has ($this->group . '_rulejson')) ? Cache::get ($this->group . '_rulejson') : Group::find ($this->group)->rulejson;

		$group_jsonfields = json_decode ($group_rulejson, true);
		$this->build_unique_rule_array ($group_jsonfields, $inputdata); 
		$validation = Validator::make ($inputdata, $this->field_rules, $this->field_rules_messages);

		if ($validation->passes ())
		{
			return true;
		}
		else
		{
			// dd ($validation->messages ());
			$this->error_messages = $validation->messages ();
			return false;
		}
	}

	private function get_honeypot_fields_array ($group_jsonfields)
	{
		$honeyfields = [];

		foreach ($group_jsonfields as $field => $properties)
		{
			if (isset ($properties['rules']))
			{
				foreach ($properties['rules'] as $rule)
				{
					if ($rule['baserule'] == 'honeypot')
					{
						$honeyfields[] = $field;
					}
				}
			}
		}

		if (count ($honeyfields) > 0)
		{
			return $honeyfields;
		}
		else
		{
			return false;
		}
	}

	public function isHoneypotFilled ($input, $group_jsonfields)
	{
		$honeyfields = $this->get_honeypot_fields_array ($group_jsonfields);
		
		/* no honeypot fields */
		if ($honeyfields == false)
		{
			return false;
		}

		foreach ($honeyfields as $field)
		{
			// missing honeypot field
      if (!isset ($input[$field]))
      {
        return true;
      }

      // non-empty honeypot field
      if ($input[$field] != '')
      {
        return true;
      }
		}

		// honeypot field(s) is/are present and empty
		return false;

	}

}
