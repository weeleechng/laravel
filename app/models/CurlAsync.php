<?php

class CurlAsync 
{
	private function __curl_get_async_internal ($path_url)
  {
  	$base_url = url ('/');

  	/* localhost and path organiser */	
  	$localhost_pattern = '/(^https?:\/\/localhost)\/([\w\-\.\_\~\:\/\?\#\[\]\@\!\$\&\'\(\)\*\+\,\;\=]+)/';
  	$ending_slash_pattern = '/(\/)$/';
    $starting_slash_pattern = '/^(\/)/';
  	
  	preg_match ($localhost_pattern, $base_url, $matches);

  	if (isset ($matches[1]) && isset ($matches[2]))
  	{
  		$base_url = $matches[1];
  		if (preg_match ($ending_slash_pattern, $matches[2]) == 1)
  		{
  			$path_url = '/' . $matches[2] . $path_url;
  		}
  		else
  		{
  			$path_url = '/' . $matches[2] . '/' . $path_url;
  		}
  	}
    else
    {
      /* this part is not yet tested in real scenario: */
      if (preg_match ($starting_slash_pattern, $path_url) == 0)
      {
        $path_url = '/' . $path_url;
      }
    }

    /* fork a separate HTTP request for external post */
    $base_url = str_replace ("http://", "", $base_url);

    $fp = fsockopen ($base_url, 80, $errno, $errstr, 30);

    if (!$fp) 
    {
      return "$errstr ($errno)<br />\n"; // technically this should not happen
      // exit ();
    } 
    else 
    {
      $out = "GET " . $path_url . " / HTTP/1.1\r\n";
      $out .= "Host: " . $base_url . "\r\n";
      $out .= "Connection: Close\r\n\r\n";
      fwrite ($fp, $out);
      // $ret = "";
      fclose ($fp);

      // Log::info('CurlAsync: ' . $base_url . $path_url);
    }
  }

	public static function make_request_internal ($path_url)
	{
		$curl_async = new CurlAsync;
		$curl_async->__curl_get_async_internal ($path_url);
	}

}
