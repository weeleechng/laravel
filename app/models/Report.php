<?php

class Report extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'report';

	protected $report_status =
	[
		"active" => "Active",
		"inactive" => "Inactive",
	];

	protected $report_frequency =
	[
		"hour" => "Hourly",
		"day" => "Daily",
		"week" => "Weekly",
		"fortnight" => "Fortnightly",
		"month" => "Monthly",
	];

	protected $report_field_condition =
	[
		"none" => "-",
		"not_empty" => "Is not empty",
		"equals" => "Equals",
		"not" => "Is not",
		"contains" => "Contains",
		"less" => "Is lesser than",
		"less_equal" => "Is not more than",
		"between" => "Is between",
		"more_equal" => "Is at least",
		"more" => "Is greater than",
		"empty" => "Is empty",
	];

	protected $report_core_field =
	[
		'ipaddress' => 'IP address',
		'useragent' => 'User agent string',
		'urlsource' => 'Form URL',
	];

	public static $_rules = array 
	(
		'nm' => 'required',
		'em-fr' => 'email',
		'em-to' => 'array_email',
		'em-sb' => 'required_with:em-fr',
		'rf-sel'=> 'required',
		'lg' => 'date_format:Y-m-d H:i:s',
	);

	public static $_rules_messages = array
	(
    'nm.required' => 'Report Name is REQUIRED',
    'em-fr.email' => 'Sender e-mail address must be VALID',
    'em-to.array_email' => 'Recipient e-mail must be VALID',
    'em-sb.required_with' => 'E-mail subject must be PROVIDED',
    'rf-sel.required' => 'At least one field must be selected to create a report',
    'lg.date_format' => 'Valid format is YYYY-MM-DD HH:MM:SS',
  );

  /* Validate create input */
	public function isValid ($inputdata)
	{
		$validation = Validator::make ($inputdata, static::$_rules, static::$_rules_messages);
		/* toDo: multiple email address validation */

		if ($validation->passes ())
		{
			return true;
		}
		else
		{
			$this->error_messages = $validation->messages ();
			return false;
		}

	}

	public function get_status_list ()
	{
		return $this->report_status;
	}

	public function get_frequency_default ()
	{
		return $this->report_frequency;
	}

	public function get_field_condition ()
	{
		return $this->report_field_condition;
	}

	public function get_core_field ()
	{
		return $this->report_core_field;
	}

}
