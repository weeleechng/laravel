<?php

class Guid 
{

	/* 
	 *  Globally Unique Identifier (GUID) generator utility
	 */

	private static function _guid_generate ()
	{
		return sprintf (
			'%04X%04X-%04X-%04X-%04X-%04X%04X%04X', 
			mt_rand(0, 65535), 
			mt_rand(0, 65535), 
			mt_rand(0, 65535), 
			mt_rand(16384, 20479), 
			mt_rand(32768, 49151), 
			mt_rand(0, 65535), 
			mt_rand(0, 65535), 
			mt_rand(0, 65535)
		);
	}

	/* Public interfaces with aliases */

	public static function generate ()
	{
		$obj_guid = new Guid;
		return $obj_guid->_guid_generate ();
	}

	public static function make ()
	{
		$obj_guid = new Guid;
		return $obj_guid->_guid_generate ();
	}

	public static function get ()
	{
		$obj_guid = new Guid;
		return $obj_guid->_guid_generate ();
	}

}
