<?php

class Group extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'group';

  /* These core fields can be mapped to custom fields in the lead data */
	protected $mappable_fields = 
	[
		"" => "",
	  "lastname" => "lastname",
	  "firstname" => "firstname",
	  "email" => "email",
	  "contactno" => "contactno",
	];

	protected $group_status =
	[
		"running" => "Running",
		"paused" => "Paused",
	];

	protected $validation_rules =
	[
	  "required" => "Required",
	  "email" => "Email",
	  "date_format" => "Datetime",
	  "unique" => "Unique",
	  "unique_with" => "Unique with",
	  "regex" => "Regex",
	  "honeypot" => "Honeypot",
	];

	public static $group_rules = array 
	(
		'id' => 'sometimes|required|alpha_dash|unique:group', 
		'nm' => 'required', 
	);

	public static $group_rules_messages = array
	(
    'id.required' => "Group Idenfitier is REQUIRED",
    'id.alpha_dash' => 'Group Identifier can only consist of ALPHA NUMERIC values',
    'id.unique' => 'This Group Identifier is already IN USE',
    'nm.required' => 'Group Name is REQUIRED',
  );

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public function get_mappable ()
	{
		return $this->mappable_fields;
	}

	public function get_status_list ()
	{
		return $this->group_status;
	}

	public function get_validation_rules ()
	{
		return $this->validation_rules;
	}

	public function has_external ()
	{
		if (isset($this->external) && $this->external != '')
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function get_name ()
	{
		return $this->name;
	}

	public $error_messages;

  /* Validate create input */
	public function isValid ($inputdata)
	{
		$validation = Validator::make ($inputdata, static::$group_rules, static::$group_rules_messages);

		if ($validation->passes())
		{
			return true;
		}
		else
		{
			$this->error_messages = $validation->messages ();
			return false;
		}

	}

}
