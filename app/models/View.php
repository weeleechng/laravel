<?php

class View extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'view';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

}
