<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

  protected $admin_id = 'admin'; // protected user ID
	/*
	protected $fillable = array (
		'id',
		'name',
		'password',
		'role',
		'email',
		'status',
	);
	*/
	public $error_messages;

	// Validation rules - can be expanded and applied to more scenarios
	public $rules = [
		'id' => 'sometimes|required',
		'newid' => 'sometimes|required|unique:users,id',
		'password' => 'sometimes|required',
		'newpassword' => 'sometimes|required|confirmed',
		'changepassword' => 'sometimes|confirmed',
		'name' => 'sometimes|required', // Adding 'sometimes' helps reusing validation rules 
		'role' => 'sometimes|required', 
		'email' => 'sometimes|required|email', 
		'status' => 'sometimes|required',
	];


	// Validation rules messages
	public $rules_messages = [
		'id.required' => "Username is required",
		'newid.required' => "Username is required",
		'newid.unique' => 'The User ID is already taken',
		'name.required' => "Name is required",
		'password.required' => "Password is required",
		'newpassword.required' => "Password is required",
		'newpassword.confirmed' => 'Passwords must MATCH',
		'changepassword.confirmed' => 'Passwords must match',
		'pa.same' => "Password entries must MATCH",
		'role.required' => "User Role is required",
		'email.required' => "E-mail is required",
		'email.email' => "E-mail address must be valid",
		'status.required' => "User account Status is required"
	];

	// public static $create_rules = array 
	// (
	// 	'id' => ['required', 'unique:users,id'], 
	// 	'name' => 'required', 
	// 	'pw' => ['required', 'confirmed'], 
	// 	/*'pa' => 'required|same:pw',*/
	// 	'rl' => 'required', 
	// 	'em' => ['required', 'email'], 
	// 	'st' => 'required',
	// );

	// public static $create_rules_messages = array
	// (
 //    'id.required' => "Username is REQUIRED",
 //    'name.required' => "Name is REQUIRED",
 //    'pw.required' => "Password is REQUIRED",
 //    'pw.confirmed' => 'Passwords must MATCH',
 //    'pa.same' => "Password entries must MATCH",
 //    'rl.required' => "User Role is REQUIRED",
 //    'em.required' => "E-mail is REQUIRED",
 //    'em.email' => "E-mail address must be valid",
 //    'st.required' => "User account Status is REQUIRED",
 //  );

	// public static $update_rules = array 
	// (
	// 	'name' => 'required',
 //    'pw' => 'confirmed',
	// 	'pw' => 'same:pa', 
	// 	/*'pa' => 'same:pw',*/
	// 	'rl' => 'required', 
	// 	'em' => ['required', 'email'], 
	// 	'st' => 'required',
	// );

	// public static $update_rules_messages = array
	// (
 //    'name.required' => "Name is REQUIRED",
 //    /*'pw.same' => "Password entries must MATCH",
 //    'pa.same' => "Password entries must MATCH",*/
 //    'pw.confirmed' => 'Password entries must MATCH',
 //    'rl.required' => "User Role is REQUIRED",
 //    'em.required' => "E-mail is REQUIRED",
 //    'em.email' => "E-mail address must be valid",
 //    'st.required' => "User account Status is REQUIRED",
 //  );

  /* Define relationship with user role */
  public function userrole()
  {
    return $this->hasOne ('Role', 'id', 'role');
  }

  public function is_admin ($id)
  {
    if ($id == $this->admin_id)
      return true;
    else
      return false;
  }

	/**
	 * Checks data against validation rules 
	 *
	 * @param array $inputData
	 * @return bool Response
	 */	
	public function isValid ($inputdata)
	{
		$validation = Validator::make ($inputdata, $this->rules, $this->rules_messages);

		if ($validation->passes())
			return true;

		// If validation fails
		$this->error_messages = $validation->messages();
			return false;

	}

  /* Validate update input */
	public function isValidUpdate ($inputdata)
	{
		$validation = Validator::make ($inputdata, static::$update_rules, static::$update_rules_messages);

		if ($validation->passes())
		{
			return true;
		}
		else
		{
			$this->error_messages = $validation->messages();
			return false;
		}

	}

}
