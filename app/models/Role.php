<?php

class Role extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'userrole';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	// protected $hidden = array('password', 'remember_token');


	public static $create_rules = array 
	(
		'nm' => 'required', 
		'pm' => 'required', 
	);

	public static $create_rules_messages = array
	(
    'nm.required' => "Name is REQUIRED",
    'pm.required' => "At least one permission item is REQUIRED",
  );

	public $error_messages;

  /* Validate create input */
	public function isValid ($inputdata)
	{
		$validation = Validator::make ($inputdata, static::$create_rules, static::$create_rules_messages);

		if ($validation->passes())
		{
			return true;
		}
		else
		{
			$this->error_messages = $validation->messages ();
			return false;
		}

	}


}
