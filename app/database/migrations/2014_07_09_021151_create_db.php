<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDb extends Migration {

	/**
	 * Run the migrations.
	 */

	/*******

For PostgreSQL, execute the following SQL statements after running the migration script :

ALTER TABLE "users" ALTER COLUMN role SET DATA TYPE uuid USING id::UUID;
ALTER TABLE "userrole" ALTER COLUMN id SET DATA TYPE uuid USING id::UUID;
ALTER TABLE "userrole" ALTER COLUMN permissionjson SET DATA TYPE json USING permissionjson::JSON;
ALTER TABLE "lead" ALTER COLUMN id SET DATA TYPE uuid USING id::UUID;
ALTER TABLE "lead" ALTER COLUMN datajson SET DATA TYPE json USING datajson::JSON;
ALTER TABLE "group" ALTER COLUMN rulejson SET DATA TYPE json USING rulejson::JSON;
ALTER TABLE "view" ALTER COLUMN id SET DATA TYPE uuid USING id::UUID;
ALTER TABLE "view" ALTER COLUMN viewjson SET DATA TYPE json USING viewjson::JSON;
ALTER TABLE "report" ALTER COLUMN id SET DATA TYPE uuid USING id::UUID;
ALTER TABLE "report" ALTER COLUMN email SET DATA TYPE json USING email::JSON;
ALTER TABLE "report" ALTER COLUMN reportjson SET DATA TYPE json USING reportjson::JSON;
	 
	*******/

	/* 
	 * @return void
	 */

	private function _pgsql_specific ()
	{
		if (DB::connection()->getDriverName() == 'pgsql')
		{
			DB::statement('ALTER TABLE "users" ALTER COLUMN role SET DATA TYPE uuid USING id::UUID');
			DB::statement('ALTER TABLE "userrole" ALTER COLUMN id SET DATA TYPE uuid USING id::UUID');
			DB::statement('ALTER TABLE "userrole" ALTER COLUMN permissionjson SET DATA TYPE json USING permissionjson::JSON');
			DB::statement('ALTER TABLE "lead" ALTER COLUMN id SET DATA TYPE uuid USING id::UUID');
			DB::statement('ALTER TABLE "lead" ALTER COLUMN datajson SET DATA TYPE json USING datajson::JSON');
			DB::statement('ALTER TABLE "group" ALTER COLUMN rulejson SET DATA TYPE json USING rulejson::JSON');
			DB::statement('ALTER TABLE "view" ALTER COLUMN id SET DATA TYPE uuid USING id::UUID');
			DB::statement('ALTER TABLE "view" ALTER COLUMN viewjson SET DATA TYPE json USING viewjson::JSON');
			DB::statement('ALTER TABLE "report" ALTER COLUMN id SET DATA TYPE uuid USING id::UUID');
			DB::statement('ALTER TABLE "report" ALTER COLUMN email SET DATA TYPE json USING email::JSON');
	    DB::statement('ALTER TABLE "report" ALTER COLUMN reportjson SET DATA TYPE json USING reportjson::JSON');
		}

	}

	public function up()
	{
		Schema::create ('users', function (Blueprint $table)
		{
			$table->string ('id', 32)->primary (); // string
			$table->string ('name', 255);
			$table->string ('password', 255);
			$table->string ('email', 255);
			$table->string ('status', 16)->nullable();
			$table->rememberToken();
			$table->string ('role', 64)->nullable();
			$table->timestamps (); // created_at, updated_at
		});		

		Schema::create ('userrole', function (Blueprint $table)
		{
			$table->string ('id', 64)->primary (); // = Guid::make();
			$table->string ('name', 255);
			$table->longText('description')->nullable();
			$table->boolean ('canbedeleted')->default (true);
			$table->longText ('permissionjson')->nullable();
			$table->timestamps (); // created_at, updated_at
		});

		Schema::create ('permission', function (Blueprint $table)
		{
			$table->string ('id', 128)->primary (); // = Guid::make();
			$table->string ('name', 255);
			$table->longText('description')->nullable();
			$table->timestamps (); // created_at, updated_at
		});
		
		Schema::create ('lead', function (Blueprint $table)
		{
			$table->string ('id', 64)->primary (); // = Guid::make();
			$table->string ('group', 64);
			$table->string ('lastname', 255);
			$table->string ('firstname', 255);
			$table->string ('email', 255)->nullable();
			$table->string ('contactno', 32)->nullable();
			$table->longText ('datajson');
			$table->string ('ipaddress', 24);
			$table->string ('urlsource', 2048)->nullable();
			$table->string ('status', 16)->nullable();
			$table->longText ('remarks')->nullable ();
			$table->longText ('extlog')->nullable ();
			$table->boolean ('istestlead')->default (false);
			$table->timestamps (); // created_at, updated_at
		});

		Schema::create ('group', function (Blueprint $table)
		{
			$table->string ('id', 64)->primary (); // string
			$table->string ('name', 255);
			$table->longText ('description')->nullable();
			$table->string ('client', 255)->nullable();
			$table->string ('external', 2048)->nullable();
			$table->string ('status', 16)->default ('Running');
			$table->longText ('rulejson');
			$table->timestamps (); // created_at, updated_at
		});
		
		Schema::create ('view', function (Blueprint $table)
		{
			$table->string ('id', 64)->primary (); // = Guid::make();
			$table->string ('name', 255);
			$table->longText ('description')->nullable();
			$table->string ('authkey', 255);
			$table->longText ('viewjson');
			$table->timestamps (); // created_at, updated_at
		});
		
		Schema::create ('report', function (Blueprint $table)
		{
			$table->string ('id', 64)->primary (); // = Guid::make();
			$table->string ('name', 255);
			$table->longText ('description')->nullable();
			$table->longText ('email')->nullable();
			$table->longText ('reportjson');
			$table->string ('status', 16)->nullable();
			$table->timestamps (); // created_at, updated_at
		});

    // run pgsql-only queries
		$this->_pgsql_specific();

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop ('users');
		Schema::drop ('userrole');
		Schema::drop ('permission');
		Schema::drop ('lead');
		Schema::drop ('group');
		Schema::drop ('view');
		Schema::drop ('report');
	}

}
