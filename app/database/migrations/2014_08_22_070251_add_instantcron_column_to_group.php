<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInstantcronColumnToGroup extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table ('group', function ($table)
		{
	    $table->boolean ('instantcron')->default (false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table ('group', function ($table)
		{
			$table->dropColumn ('instantcron');
		});
	}

}
