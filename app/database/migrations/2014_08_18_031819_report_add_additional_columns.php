<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReportAddAdditionalColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table ('report', function ($table)
		{
	    $table->dateTime ('last_generated')->nullable ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table ('report', function ($table)
		{
			$table->dropColumn ('last_generated');
		});
	}

}
