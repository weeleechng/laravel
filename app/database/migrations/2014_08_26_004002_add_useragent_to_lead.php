<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUseragentToLead extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table ('lead', function ($table)
		{
	    $table->longText ('useragent')->nullable ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table ('lead', function ($table)
		{
	    $table->dropColumn ('useragent');
		});
	}

}
