<?php

class DatabaseSeeder extends Seeder 
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
    $this->command->info('Table users seeded!');
		
    $this->call('RoleTableSeeder');
    $this->command->info('Table userrole seeded!');

    $this->call('PermissionTableSeeder');
    $this->command->info('Table permission seeded!');

    $this->call('LeadTableSeeder');
    $this->command->info('Table lead seeded!');

    $this->call('GroupTableSeeder');
    $this->command->info('Table group seeded!');

    /*
		$this->call('ReportTableSeeder');
    $this->command->info('Table report seeded!');
		$this->call('ViewTableSeeder');
    $this->command->info('Table view seeded!');
		*/
	}

}

class UserTableSeeder extends Seeder 
{

    public function run()
    {
        DB::table('users')->delete();
        
        User::create
        ([
        	'id' => 'admin',
        	'name' => 'System Administrator',
        	'password' => Hash::make ('admin'),
        	'email' => 'leads@estorm.com',
        	'status' => 'active',
        	'role' => '00000000-0000-0000-0000-000000000000',
        ]);
        User::create
        ([
        	'id' => 'ezekiel',
        	'name' => 'Ezekiel Mangalu',
        	'password' => Hash::make ('ezekiel'),
        	'email' => 'ezekiel@estorm.com',
        	'status' => 'active',
        	'role' => '00000000-0000-0000-0000-000000000000',
        ]);
        User::create
        ([
        	'id' => 'jonathan',
        	'name' => 'Jonathan Reyes',
        	'password' => Hash::make ('jonathan'),
        	'email' => 'jonathan@estorm.com',
        	'status' => 'active',
        	'role' => '203ff89f-0ad1-4971-a710-ac473a5a6488',
        ]);
        User::create
        ([
        	'id' => 'tracy',
        	'name' => 'Tracy Green',
        	'password' => Hash::make ('tracy'),
        	'email' => 'tracy@estorm.com',
        	'status' => 'active',
        	'role' => '26b12b6f-a3c6-4bcf-8094-d6224ebb58a5',
        ]);
        User::create
        ([
        	'id' => 'tiffany',
        	'name' => 'Tiffany Hinson',
        	'password' => Hash::make ('tiffany'),
        	'email' => 'tiffany@estorm.com',
        	'status' => 'blocked',
        	'role' => '203ff89f-0ad1-4971-a710-ac473a5a6488',
        ]);

    }

}

class RoleTableSeeder extends Seeder 
{

    public function run()
    {
        DB::table('userrole')->delete();
        
        Role::create
        ([
        	'id' => '00000000-0000-0000-0000-000000000000',
        	'name' => 'Administrator',
        	'description' => 'Lead Manager system administrator. Full system access and control.',
        	'permissionjson' => json_encode (
        	[
        		"manage_users" => "true",
        		"manage_lead_group" => "true",
        		"manage_lead_view" => "true",
        		"manage_lead_report" => "true",
        		"access_lead" => "true",
        		"access_lead_group" => "true",
        		"access_lead_report" => "true"
        	]),
            'canbedeleted' => false,
        ]);
        Role::create
        ([
        	'id' => '203ff89f-0ad1-4971-a710-ac473a5a6488',
        	'name' => 'Account Manager',
        	'description' => 'Manage lead groups, lead views and lead reports.',
        	'permissionjson' => json_encode (
        	[
        		"manage_users" => "false",
        		"manage_lead_group" => "true",
        		"manage_lead_view" => "true",
        		"manage_lead_report" => "true",
        		"access_lead" => "true",
        		"access_lead_group" => "true",
        		"access_lead_report" => "true"
        	]),
            'canbedeleted' => true,
        ]);
        Role::create
        ([
        	'id' => '26b12b6f-a3c6-4bcf-8094-d6224ebb58a5',
        	'name' => 'Auditor',
        	'description' => 'Read-only access to leads, views and reports.',
        	'permissionjson' => json_encode (
        	[
        		"manage_users" => "false",
        		"manage_lead_group" => "false",
        		"manage_lead_view" => "false",
        		"manage_lead_report" => "false",
        		"access_lead" => "true",
        		"access_lead_group" => "true",
        		"access_lead_report" => "true"
        	]),
            'canbedeleted' => true,
        ]);

    }

}

class PermissionTableSeeder extends Seeder
{
	public function run()
  {
    DB::table('permission')->delete();
    
    Permission::create
    ([
    	'id' => "access_lead",
    	'name' => "Access Lead",
    	'description' => "Read-only access to all lead",
    ]);
    Permission::create
    ([
    	'id' => "access_lead_group",
    	'name' => "Access Lead Group",
    	'description' => "Read-only access to lead groups and settings",
    ]);
    Permission::create
    ([
    	'id' => "access_lead_report",
    	'name' => "Access Lead Report",
    	'description' => "Read-only access to lead reports and settings",
    ]);
    Permission::create
    ([
    	'id' => "manage_lead_group",
    	'name' => "Manage Lead Group",
    	'description' => "Manage lead group, fields, rules",
    ]);
    Permission::create
    ([
    	'id' => "manage_lead_report",
    	'name' => "Manage Lead Report",
    	'description' => "Control lead reporting",
    ]);
    Permission::create
    ([
    	'id' => "manage_lead_view",
    	'name' => "Manage Lead View",
    	'description' => "Create, update or remove lead view",
    ]);
    Permission::create
    ([
    	'id' => "manage_users",
    	'name' => "Manage Users",
    	'description' => "Manage system users and roles",
    ]);

  }

}

class LeadTableSeeder extends Seeder
{
	public function run()
  {
    DB::table('lead')->delete();
    
    Lead::create
    (
        [
        	'id' => '00000000-0000-4000-0000-000000000002',
        	'lastname' => 'Vang',
        	'firstname' => 'Hlee',
        	'email' => 'private.information@yahoo.com',
        	'contactno' => '558-374-1126',
        	'ipaddress' => '99.45.25.89',
        	'urlsource' => 'degrees.nu.edu/nu_universitywide?track=ppcmsn001&kwid=national+university+fresno_broad&ksID=kw27792',
        	'group' => 'nu_talisma',
        	'status' => 'pass',
            'created_at' => '2014-04-16 02:03:04',
        	'datajson' => json_encode(
        	[
        	  'Contact_LastNamePropID' => 'Vang',
              'Contact_FirstNamePropID' => 'Hlee',
              'Contact_MiddleNamePropID' => '',
              'Contact_HomePhonePropID' => '558-374-1126',
              'Lead_AlternativePhonePropID' => '',
              'Contact_EmailPropID' => 'private.information@yahoo.com',
              'Contact_Address1PropID' => '',
              'Contact_Address2PropID' => '',
              'Contact_Address3PropID' => '',
              'Contact_CityPropID' => '',
              'Contact_StatePropID' => '',
              'Contact_ZipCodePropID' => '',
              'Lead_MilitaryAssociationPropID' => '',
              'Lead_eLeadUSCitizenPropID' => '',
              'Lead_InternationalAddress1PropID' => '',
              'Lead_InternationalAddress2PropID' => '',
              'Lead_InternationalCityPropID' => '',
              'Lead_CountryPropID' => 'United States',
              'Lead_CollegeCreditsoutsideUSAPropID' => '',
              'Contact_GenderPropID' => '',
              'Lead_WorkExperiencePropID' => '',
              'Lead_eLeadGPAPropID' => '',
              'Lead_eLeadDesiredStartDatePropID' => 'June',
              'Lead_eLeadDesiredStartYearPropID' => '2014',
              'Lead_HowDidYouHearPropID' => '',
              'Lead_CampaignActivityNamePropID' => 'ppcmsn001',
              'Lead_ClassDeliveryPropID' => '',
              'Lead_eLeadCampusPreferencePropID' => 'Online',
              'Lead_LevelofEducationPropID' => '',
              'Lead_eLeadDegreeofInterestPropID' => 'Bachelor of Arts in General Studies',
              'Lead_eLeadAreaofInterestPropID' => '',
              'Lead_eLeadOtherAreaofInterestPropID' => '',
              'Lead_FinancingEducationPropID' => '',
              'Lead_eLeadCommentsPropID' => '',
              'Lead_FormIDPropID' => 'nu_universitywidePPC',
              'Lead_AffiliatePropID' => 'National University',
              'Lead_LeadTypePropID' => 'Internet',
              'Lead_ExternalWebContactDatePropID' => '2014-04-15 13:29:21',
              'Lead_eLeadFlagPropID' => 'eMarketing',
              'Lead_LeadGroupPropID' => '',
              'Lead_StatusPropID' => 'New',
              'Lead_Address1PropID' => '',
              'Lead_Address2PropID' => '',
              'Lead_Address3PropID' => '',
              'Lead_CityPropID' => '',
              'Lead_StatePropID' => '',
              'Lead_ZipCodePropID' => '93726',
              'vendor' => 'NPI',
              'creative' => 'nu_universitywidePPC',
        	]),
        ]);
    Lead::create
    (
        [
            'id' => '00000000-0000-4000-0000-000000000001',
            'lastname' => 'Kelly',
            'firstname' => 'Kathy',
            'email' => 'private.information@gmail.com',
            'contactno' => '9152188106',
            'ipaddress' => '74.205.26.243',
            'urlsource' => '',
            'group' => 'nu_talisma',
            'status' => 'pass',
            'created_at' => '2014-04-15 18:38:20',
            'datajson' => json_encode(
            [
              'Contact_LastNamePropID' => 'Kelly',
              'Contact_FirstNamePropID' => 'Kathy',
              'Contact_HomePhonePropID' => '9152188106',
              'Lead_AlternativePhonePropID' => '5305372396',
              'Contact_EmailPropID' => 'private.information@gmail.com',
              'Lead_eLeadUSCitizenPropID' => 'Yes',
              'Lead_CountryPropID' => 'United States',
              'Contact_GenderPropID' => 'Female',
              'Lead_eLeadDesiredStartDatePropID' => 'Oct',
              'Lead_eLeadDesiredStartYearPropID' => '2014',
              'Lead_CampaignActivityNamePropID' => 'EDUDYN041',
              'Lead_eLeadCampusPreferencePropID' => 'Pleasant Hill, CA',
              'Lead_LevelofEducationPropID' => 'High School Diploma\/GED',
              'Lead_eLeadDegreeofInterestPropID' => 'EDUDYN041',
              'Lead_FormIDPropID' => 'nu_universitywidePPC',
              'Lead_AffiliatePropID' => 'National University',
              'Lead_LeadTypePropID' => 'Internet',
              'Lead_ExternalWebContactDatePropID' => '2014-04-15 18:23:14',
              'Lead_eLeadFlagPropID' => 'eMarketing',
              'Lead_StatusPropID' => 'New',
              'Lead_Address1PropID' => '2110 Shoreview Ca.',
              'Lead_CityPropID' => 'Pittsburg',
              'Lead_StatePropID' => 'CA',
              'Lead_ZipCodePropID' => '94565',
              'vendor' => 'EduDyn',
            ]),
        ]
    );

  }

}

class GroupTableSeeder extends Seeder
{
  public function run()
  {
    DB::table('group')->delete();

    Group::create
    (
      [
        'id' => 'storm_test',
        'name' => 'estorm Test',
        'description' => 'Testing only',
        'client' => 'estorm International',
        'external' => 'http://requestb.in/19voz5x1',
        'status' => 'paused',
        'rulejson' => json_encode
        (
          [
            'storm_lastname' => 
            [
              'displayname' => 'Last Name', 
              'map' => 'lastname',
              'rules' => 
              [
                [
                  'type' => 'Required', 
                  'baserule' => 'required',
                  'rule' => 'required', 
                  'message' => 'Last name is required',
                ],
              ],
            ],
            'storm_firstname' =>
            [
              'displayname' => 'First Name',
              'map' => 'firstname',
              'rules' =>
              [
                [
                  'type' => 'Required',
                  'baserule' => 'required',
                  'rule' => 'required',
                  'message' => 'First name is required',
                ],
              ],
            ],
            'storm_phone'=>
            [
              'displayname' => 'Contact No',
              'map' => 'contactno',
              'rules'=>
              [
                [
                  'type' => 'Required',
                  'baserule' => 'required',
                  'rule' => 'required',
                  'message' => 'Phone number is required',
                ],
              ],
            ],
            'storm_email' =>
            [
              'displayname' => 'E-mail address',
              'map' => 'email',
              'rules'=>
              [
                [
                  'type' => 'Required',
                  'baserule' => 'required',
                  'message' => 'E-mail address is required',
                ],
                [
                  'type'=> 'E-mail',
                  'baserule' => 'email',
                  'message'=> 'Please provide a valid e-mail address',
                ],
                [
                  'type'=> 'Unique',
                  'baserule' => 'unique',
                  'rule'=> 'lead,email,status,fail,group,storm_test',
                  'message'=> 'This e-mail address exists in our records',
                ],
              ],
            ],
            'storm_country' =>
            [
              'displayname' => 'Country',
              'rules'=>
              [
                [
                  'type' => 'Required',
                  'baserule' => 'required',
                  'rule' => 'required',
                  'message' => 'Country is required',
                ],
              ]
            ],
            'storm_postcode'=>
            [
              'displayname' => 'Postcode',
              'rules' =>
              [
                [
                  'type' => 'Regex',
                  'baserule' => 'regex',
                  'rule' => '/^[a-zA-Z\\d]{3,10}$/',
                  'message' => 'Please provide a valid Postcode',
                ],
              ],
            ],
            'storm_campaign_id' =>
            [
              'displayname' => 'Campaign ID',
              'defaultvalue' => 'storm-track-001',
              'rules' =>
              [
                [
                  'type' => 'Required',
                  'baserule' => 'required',
                  'rule' => 'required',
                  'message' => 'We need to track you',
                ],
              ]
            ],
          ]
        )
      ]
    );
    
    Group::create
    (
      [
      	'id' => 'nu_talisma',
      	'name' => 'NU Talisma',
      	'description' => 'With external post to NU Talisma',
      	'client' => 'National University',
        'external' => 'http://requestb.in/19voz5x1',
        'status' => 'running',
      	'rulejson' => json_encode
        (
      	  [
      	    'Contact_LastNamePropID' => 
            [
              'displayname' => 'Last Name', 
              'rules' => 
              [
                [
                  'type' => 'Required', 
                  'baserule' => 'required',
                  'rule' => 'required', 
                  'message' => 'Last name is required',
                ],
              ],
              'map' => 'lastname',
            ],
            'Contact_FirstNamePropID' =>
            [
              'displayname' => 'First Name',
              'rules' =>
              [
                [
                  'type' => 'Required',
                  'baserule' => 'required',
                  'rule' => 'required',
                  'message' => 'First name is required',
                ],
              ],
              'map' => 'firstname',
            ],
            'Contact_MiddleNamePropID' =>
            [
              'displayname' => 'Middle Name',
            ],
            'Contact_HomePhonePropID'=>
            [
              'displayname' => 'Home Phone',
              'rules'=>
              [
                [
                  'type' => 'Required',
                  'baserule' => 'required',
                  'rule' => 'required',
                  'message' => 'Phone number is required',
                ],
                [
                  'type' => 'Regex',
                  'baserule' => 'regex',
                  'rule' => '/^\\(?\\d{3}\\)?[-.\\s]?\\d{3}[-.\\s]?\\d{4}$/',
                  'message' => 'Please provide a valid US phone number format',
                ],
              ],
              'map' => 'contactno'
            ],
            'Lead_AlternativePhonePropID' =>
            [
              'displayname' => 'Alternative Phone',
            ],
            'Contact_EmailPropID' =>
            [
              'displayname' => 'E-mail address',
              'rules'=>
              [
                [
                  'type' => 'Required',
                  'baserule' => 'required',
                  'rule' => 'required',
                  'message' => 'E-mail address is required',
                ],
                [
                  'type'=> 'E-mail',
                  'baserule' => 'email',
                  'rule'=> 'email',
                  'message'=> 'Please provide a valid e-mail address',
                ],
                [
                  'type'=> 'Unique',
                  'baserule' => 'unique',
                  'rule'=> 'lead,email,status,fail,group,nu_talisma',
                  'message'=> 'This e-mail address is already registered',
                ],
              ],
              'map' => 'email',
            ],
            'Contact_Address1PropID' =>
            [
              'displayname' => 'Address line 1',
            ],
            'Contact_Address2PropID' =>
            [
              'displayname' => 'Address line 2',
            ],
            'Contact_Address3PropID' =>
            [
              'displayname' => 'Address line 3',
            ],
            'Contact_CityPropID' =>
            [
              'displayname' => 'City',
            ],
            'Contact_StatePropID' =>
            [
              'displayname' => 'State',
            ],
            'Contact_ZipCodePropID' =>
            [
              'displayname' => 'Zip Code',
            ],
            'Lead_MilitaryAssociationPropID' =>
            [
              'displayname' => 'Military Association',
            ],
            'Lead_eLeadUSCitizenPropID' =>
            [
              'displayname' => 'US Citizen',
            ],
            'Lead_InternationalAddress1PropID' =>
            [
              'displayname' => 'International Address 1',
            ],
            'Lead_InternationalAddress2PropID' =>
            [
              'displayname' => 'International Address 2',
            ],
            'Lead_InternationalCityPropID' =>
            [
              'displayname' => 'International City',
            ],
            'Lead_CountryPropID' =>
            [
              'displayname' => 'Country',
              'rules'=>
              [
                [
                  'type' => 'Required',
                  'baserule' => 'required',
                  'rule' => 'required',
                  'message' => 'Country is required',
                ],
              ]
            ],
            'Lead_CollegeCreditsoutsideUSAPropID' =>
            [
              'displayname' => 'College credits outside USA',
            ],
            'Contact_GenderPropID' =>
            [
              'displayname' => 'Gender',
            ],
            'Lead_WorkExperiencePropID' =>
            [
              'displayname' => 'Work Experience',
            ],
            'Lead_eLeadGPAPropID' =>
            [
              'displayname' => 'GPA',
            ],
            'Lead_eLeadDesiredStartDatePropID' =>
            [
              'displayname' => 'Desired Start Date',
            ],
            'Lead_eLeadDesiredStartYearPropID' =>
            [
              'displayname' => 'Desired Start Year',
            ],
            'Lead_HowDidYouHearPropID' =>
            [
              'displayname' => 'How did you hear about NU',
            ],
            'Lead_CampaignActivityNamePropID' =>
            [
              'displayname' => 'Track ID',
              'testvalue' => 'VendorTest_iServ',
              'rules' =>
              [
                [
                  'type' => 'Required',
                  'baserule' => 'required',
                  'rule' => 'required',
                  'message' => 'Please provide a valid Track ID',
                ],
              ]
            ],
            'Lead_ClassDeliveryPropID' =>
            [
              'displayname' => 'Class Delivery',
            ],
            'Lead_eLeadCampusPreferencePropID' =>
            [
              'displayname' => 'Campus Preference',
            ],
            'Lead_LevelofEducationPropID' =>
            [
              'displayname' => 'Level of Education',
            ],
            'Lead_eLeadDegreeofInterestPropID' =>
            [
              'displayname' => 'Degree of Interest',
            ],
            'Lead_eLeadAreaofInterestPropID' =>
            [
              'displayname' => 'Area of Interest',
            ],
            'Lead_eLeadOtherAreaofInterestPropID' =>
            [
              'displayname' => 'Other Area of Interest',
            ],
            'Lead_FinancingEducationPropID' =>
            [
              'displayname' => 'Financing Education',
            ],
            'Lead_eLeadCommentsPropID' =>
            [
              'displayname' => 'Comments',
            ],
            'Lead_FormIDPropID'=>
            [
              'displayname' => 'Form ID',
            ],
            'Lead_AffiliatePropID'=>
            [
              'displayname' => 'Affiliate',
              'defaultvalue' => 'National University',
            ],
            'Lead_LeadTypePropID'=>
            [
              'displayname' => 'Lead Type',
            ],
            'Lead_ExternalWebContactDatePropID'=>
            [
              'displayname' => 'External Contact Date',
              'rules' =>
              [
                [                              
                  'type'=> 'Datetime',
                  'baserule' => 'date_format',
                  'rule'=> 'Y-m-d H:i:s',
                  'message'=> 'Invalid date format! Please use YYYY-MM-DD HH:MM:SS',
                ],
              ],
            ],
            'Lead_eLeadFlagPropID'=>
            [
              'displayname' => 'Flag',
              'defaultvalue' => 'Emarketing',
            ],
            'Lead_LeadGroupPropID'=>
            [
              'displayname' => 'Group',
            ],
            'Lead_StatusPropID'=>
            [
              'displayname' => 'Status',
              'defaultvalue' => 'New',
            ],
            'Lead_Address1PropID'=>
            [
              'displayname' => 'Address 1',
            ],
            'Lead_Address2PropID'=>
            [
              'displayname' => 'Address 2',
            ],
            'Lead_Address3PropID'=>
            [
              'displayname' => 'Address 3',
            ],
            'Lead_CityPropID'=>
            [
              'displayname' => 'City',
            ],
            'Lead_StatePropID'=>
            [
              'displayname' => 'State',
            ],
            'Lead_ZipCodePropID'=>
            [
              'displayname' => 'Zip code',
              'rules' =>
              [
                [
                  'type' => 'Required',
                  'baserule' => 'required',
                  'rule' => 'required',
                  'message' => 'Please provide a Zip Code',
                ],
                [
                  'type' => 'Regex',
                  'baserule' => 'regex',
                  'rule' => '/^\\d{5}$/',
                  'message' => 'Please provide a valid US Zip Code',
                ],
              ],
            ],
            'vendor' =>
            [
              'displayname' => 'vendor',
            ],
            'creative'=>
            [
              'displayname' => 'creative',
            ],
          ]
        )
      ]
    );

  }

}

class ReportTableSeeder extends Seeder
{
	public function run()
  {
    DB::table('report')->delete();
    
    Report::create
    ([
    	'id' => '',
    	'name' => '',
    	'description' => '',
    	'email' => '',
    	'status' => '',
    	'reportjson' => json_encode(
    	[
    	  '' => '',
    	]),
    ]);

  }

}

class ViewTableSeeder extends Seeder
{
	public function run()
  {
    DB::table('view')->delete();
    
    Report::create
    ([
    	'id' => '',
    	'name' => '',
    	'description' => '',
    	'authkey' => '',
    	'viewjson' => json_encode(
    	[
    	  '' => '',
    	]),
    ]);

  }

}

