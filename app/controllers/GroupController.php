<?php

class GroupController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $group;
	protected $problematic_fields;

	public function __construct (Group $group)
	{
		$this->group = $group;
		$this->problematic_fields = ['deleted', 'ps', 'dn', 'fl', 'mp', 'df', 'ts', 'rb', 'rl', 'rm']; // array fields that are incompatible with blade elements
	}

	public function index()
	{
		$groups = $this->group->orderBy ('name', 'asc')->get ();
		return View::make('groups.index')->with([ 'groups' => $groups, 'page_title' => 'Manage Groups', 'menu' => array('group', 'manage_group') ]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('groups.create')->with([ 'group' => $this->group, 'page_title' => 'Add lead group', 'menu' => array('group', 'new_group')]);
	}

	public function create_clone ($id)
	{
		$group = $this->group->find ($id);
		$fields = json_decode ($group->rulejson);
		return View::make('groups.clone')->with([ 'group' => $group, 'page_title' => 'Clone ' . $group->name, 'fields' => $fields, 'menu' => array('group', 'new_group') ]);
		// dd ($group);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$group = new Group;
		$input = Input::all ();

		$fields = json_decode (json_encode ($this->format_dynamic_fields ($input, $group)));

		if (!$group->isValid ($input))
		{
			return Redirect::back()->with('return_fields', $fields)->withInput(Input::except($this->problematic_fields))->withErrors($group->error_messages);
		}

		$group->id = $input['id'];
		$group->name = $input['nm'];
		$group->description = $input['dc'];
		$group->client = $input['cl'];
		$group->external = strlen ($input['xt'] > 0) ? $input['xt'] : null;
		$group->instantcron = (isset ($input['inst']) && $input['inst'] == '1') ? true : false;
		$group->status = $input['st'];

		$group->rulejson = json_encode ($fields);

		$group->save ();

		/* cache new group info */
		$this->delete_group_cache ($group);
		$this->update_group_cache ($group);

		return Redirect::route('groups.index')->with('flashmessage', array('message' => "Lead Group <em>" . $group->name . "</em> created", 'class' => "success"));

	}

	private function cache_item ($key, $value)
	{
		$returnval = Cache::forever ($key, $value);
		Log::info ('cache_item: ' . $key . ':' . $value . ' : ' . $returnval);
		// $returnval = Cache::forever ($key, $value);
		// dd ($returnval);
	}

	private function uncache_item ($key)
	{
		Cache::forget ($key);
	}

	private function update_group_cache ($group)
	{
		/*$this->cache_item ($group->id . '_name', $group->name);*/
		$this->cache_item ($group->id . '_status', $group->status);
		$this->cache_item ($group->id . '_rulejson', $group->rulejson);
		$this->cache_item ($group->id . '_instantcron', $group->instantcron);

		if ($group->external != null)
		{
			$this->cache_item ($group->id . '_external', $group->external);
		}

	}

	private function delete_group_cache ($group)
	{
		$this->uncache_item ($group->id . '_name');
		$this->uncache_item ($group->id . '_external');
		$this->uncache_item ($group->id . '_status');
		$this->uncache_item ($group->id . '_rulejson');	
		$this->uncache_item ($group->id . '_instantcron');	
		$this->uncache_item ($group->id . '_honeypot');	
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show ($id)
	{
		$group = $this->group->find ($id);
		$fields = json_decode ($group->rulejson);

		/* cache this group data if it's not in the cache */
		if (!Cache::has ($id . '_name'))
		{
			$this->update_group_cache ($group);
		}
		
		return View::make('groups.show')->with([ 'group' => $group, 'fields' => $fields, 'page_title' => 'Lead Group: '.$group->name , 'menu' => array('group') ]);
	}

	
	/*
	 * Generate a basic sample lead form to receive lead submissions
	 */

	public function generate_form ($id)
	{
		$group = $this->group->find ($id);
		// $fields = json_decode ($group->rulejson);
		return View::make('groups.sampleform')->with ([ 'group' => $group ]);
	}

	private function group_has_lead ($group)
	{
		$group_leads = DB::table ('lead')->where ('group', $group->id)->count ();
		return ($group_leads);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$group = $this->group->find ($id);
		$group_leads = $this->group_has_lead ($group);
		$fields = json_decode ($group->rulejson);
		return View::make('groups.edit')->with([ 'group' => $group, 'fields' => $fields, 'group_leads' => $group_leads, 'menu' => array('group') ]);
	}

	/*
	 * Get all fields in group that has unique_with validation rule
	 */
	private function get_all_unique_with_fields ($input, $fieldname)
	{
		$retarr = [];
		$firstfield = null;

		foreach ($input['fl'] as $field)
		{
			if (isset ($input['rb']['unique_with'][$field]))
			{
				if ($firstfield == null)
				{
					$firstfield = $field;
				}

				// only create rule for first field that has unique_with validation
				if ($firstfield != null && $firstfield != $fieldname)
				{
					return null;
				}

				$retarr[] = 'datajson->>\'' . $field . '\'';
			}
		}

		return $retarr;
	}

	private function get_mapfieldname ($input, $fieldname)
	{
		if (isset ($input['mp'][$fieldname]))
		{
			// core field
			$map_fieldname = $input['mp'][$fieldname];
		}
		else
		{
			// custom JSON field, PostgreSQL syntax
			$map_fieldname = 'datajson->>\'' . $fieldname . '\'';
		}

		return $map_fieldname;
	}

	private function construct_field_validation_rule ($input, $group, $fieldname)
	{
		$validation_rules = $group->get_validation_rules();
		
		$retarr = [];

		foreach ($validation_rules as $validation_rule => $validation_rule_name)
		{
			if (isset ($input['rb'][$validation_rule][$fieldname]))
			{
				$rule = [];
				$rule['type'] = $validation_rule_name;
				$rule['baserule'] = $validation_rule;
				/*$rule['rule'] = $rule['baserule'];*/
				
				if (isset ($input['rl'][$validation_rule][$fieldname]) && strlen ($input['rl'][$validation_rule][$fieldname]) > 0)
				{
					$rule['rule'] = $input['rl'][$validation_rule][$fieldname];
				}
				
				/* Construct Larevel unique validation rule string */
				if ($rule['baserule'] == 'unique')
				{
					$map_fieldname = $this->get_mapfieldname ($input, $fieldname);

					// SELECT * FROM lead WHERE $map_fieldname = $value AND "status" <> 'fail' AND "group" = $input['id'];
					$rule['rule'] = 'lead,' . $map_fieldname . ',status,fail,group,' . $input['id']; // . ',istestlead,false'; // . ',status,pass';
				}

				/* Construct custom unique_with rule string */
				if ($rule['baserule'] == 'unique_with')
				{
					$with_fields = $this->get_all_unique_with_fields ($input, $fieldname);
					if ($with_fields != null)
					{
						$map_fieldname = $this->get_mapfieldname ($input, $fieldname);
						$rule['rule'] = 'lead,status,fail,group,' . $input['id']; // . ',istestlead,false';

						foreach ($with_fields as $with_field)
						{
							$rule['rule'] .= ',' . $with_field . ',{fieldvalue}';
						}
					}
				}

				/* Construct Laravel date_format validation rule string */
				if ($rule['baserule'] == 'date_format')
				{
					$rule['rule'] = 'Y-m-d H:i:s';
				}

				if (isset ($input['rm'][$validation_rule][$fieldname]) && $input['rm'][$validation_rule][$fieldname] != '')
				{
					$rule['message'] = $input['rm'][$validation_rule][$fieldname];
				}

				array_push ($retarr, $rule);
			}
		}

		if (count ($retarr) == 0)
		{
			// unset the array key if there are no validation rules for this field:
			// unset ($retarr['rules']);
			$retarr = null;
		}

		return $retarr;
	}

	/*
	 *  Convert fields array into JSON
	 * 
   */
	private function format_dynamic_fields ($input, $group)
	{
		$fields = [];

		foreach ($input['fl'] as $fieldname)
		{
			$fields[$fieldname] = [];

			if (isset ($input['dn'][$fieldname]))
			{
				$fields[$fieldname]['displayname'] = $input['dn'][$fieldname];
			}

			if (isset ($input['mp'][$fieldname]))
			{
				$fields[$fieldname]['map'] = $input['mp'][$fieldname];
			}

			if (isset ($input['df'][$fieldname]) && $input['df'][$fieldname] != '')
			{
				$fields[$fieldname]['defaultvalue'] = $input['df'][$fieldname];	
			}

			if (isset ($input['ts'][$fieldname]) && $input['ts'][$fieldname] != '')
			{
				$fields[$fieldname]['testvalue'] = $input['ts'][$fieldname];	
			}

			if (isset ($input['ps'][$fieldname]) && $input['ps'][$fieldname] != '')
			{
				$fields[$fieldname]['extpost'] = true;	
			}

			$field_rules = $this->construct_field_validation_rule ($input, $group, $fieldname);

			if (is_array ($field_rules))
			{
				$fields[$fieldname]['rules'] = $field_rules;
			}

		}

		return $fields;

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$group = $this->group->find ($id);
		$input = Input::all();
		
		$fields = json_decode (json_encode ($this->format_dynamic_fields ($input, $group)));

		/* If ID is not changed, temporarily unset the ID to prevent the 'unique' validation rule violation */
		if ($input['id'] == $id)
		{
			unset ($input['id']);
		}

		if (!$group->isValid ($input))
		{
			return Redirect::back()->with(['return_fields' => $fields])->withInput(Input::except($this->problematic_fields))->withErrors($group->error_messages);
		}

		/* Re-set the ID in the input array to be updated with the object */
		if (!isset($input['id']))
		{
			$input['id'] = $id;
		}

		// dd ($input);

		$group->id = $input['id'];
		$group->name = $input['nm'];
		$group->description = $input['dc'];
		$group->client = $input['cl'];
		$group->external = strlen ($input['xt'] > 0) ? $input['xt'] : null;
		$group->instantcron = (isset ($input['inst']) && $input['inst'] == '1') ? true : false;
		$group->status = $input['st'];

		$group->rulejson = json_encode ($fields);

		$group->save ();

		/* update group data in the cache */
		$this->delete_group_cache ($group);
		$this->update_group_cache ($group);

		return Redirect::route('groups.index')->with('flashmessage', array('message' => "Lead Group <em>" . $group->name . "</em> updated successfully", 'class' => "success"));

		// dd ($group);

	}

	public function confirmdestroy($id)
	{
		$group = $this->group->find ($id);
		return View::make('groups.confirmdelete')->with(['group' => $group]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$group = $this->group->find ($id);
		$the_name = $group->name;

		/* Delete group from cache */
		$this->delete_group_cache ($group);
		
		$group->delete ();
		return Redirect::route ("groups.index")->with ("flashmessage", array('message' => "Lead Group <em>" . $group->name . "</em> deleted", 'class' => "success"));
	}


}
