<?php

class ReportController extends \BaseController 
{

	protected $report;
	protected $problematic_fields;

	public function __construct (Report $report)
	{
		$this->report = $report;
		$this->problematic_fields = ['rf-sel', 'rf-dn', 'rf-alias', 'rf-cond', 'rf-val-1', 'rf-val-2'];
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index ()
	{
		$reports = $this->report->orderBy ('name', 'asc')->get ();
		return View::make('reports.index')->with
		([ 
			'reports' => $reports, 
			'page_title' => 'Manage Reports', 
			'menu' => array('reports', 'manage_reports') 
		]);
	}

	private function get_groups ()
	{
		return Group::orderBy ('name', 'asc')->get ();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('reports.create')->with
		([ 
			'report' => $this->report, 
			'groups' => $this->get_groups(), 
			'page_title' => 'Create Lead Report', 
			'menu' => array('reports', 'new_report')
		]);
	}

	private function build_field_array ($input)
	{
		$fields = [];

		foreach ($input['rf-dn'] as $fieldname => $value)
		{
			$fields[$fieldname] = [];
			$fields[$fieldname]['displayname'] = $input['rf-dn'][$fieldname];

			if (isset ($input['rf-alias'][$fieldname]))
			{
				$fields[$fieldname]['alias'] = ($input['rf-alias'][$fieldname] != '') ? $input['rf-alias'][$fieldname] : $fieldname;
			}

			if (isset ($input['rf-cond'][$fieldname]) && $input['rf-cond'][$fieldname] != 'none')
			{
				$fields[$fieldname]['option'] = $input['rf-cond'][$fieldname];

				if (isset ($input['rf-val-1'][$fieldname]) && $input['rf-val-1'][$fieldname] != '')
				{
					$fields[$fieldname]['val1'] = $input['rf-val-1'][$fieldname];
				}

				if (isset ($input['rf-val-2'][$fieldname]) && $input['rf-val-2'][$fieldname] != '')
				{
					$fields[$fieldname]['val2'] = $input['rf-val-2'][$fieldname];
				}

			}

		}

		return $fields;

	}

	private function field_array_filter_selected ($selected, $fields)
	{
		$selected = array_keys ($selected);
		foreach ($fields as $field => $properties)
		{
			if (!in_array ($field, $selected))
			{
				unset ($fields[$field]);
			}
		}

		return $fields;
	}

	private function build_report_email_array ($input)
	{
		if (isset ($input['em-fr']) && $input['em-fr'] != '')
		{
			$email = [];
			$email['from'] = $input['em-fr'];
			$email['to'] = $input['em-to'];
			$email['subject'] = $input['em-sb'];
			$email['message'] = $input['em-bd'];

			return $email;
		}

		return null;
	}

	private function build_report_array ($group, $fields, $frequency, $filename)
	{
		$report = [];

		$report['group'] = $group;
		$report['fields'] = $fields;
		$report['frequency'] = $frequency;
		$report['filename'] = ($filename != '') ? $filename : $group;

		return $report;
	}

	private function granularity_hour ($datetime)
	{
		$carbonite = new Carbon ($datetime);
		$carbonite->minute = 0;
		$carbonite->second = 0;

		return $carbonite->toDateTimeString ();
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store ()
	{
		$input = Input::all ();
		$separator_pattern = '/([\w\@\.\-\+\|]+)/';
		preg_match_all ($separator_pattern, $input['em-to'], $matches);

		if (isset ($matches[1]) && is_array ($matches[1]))
		{
			$input['em-to'] = $matches[1];
		}

		$report = new Report;
		$fields = $this->build_field_array ($input);

		if (!$report->isValid ($input))
		{
			return Redirect::back()->withInput(Input::except($this->problematic_fields))->withErrors($report->error_messages);
		}
		else
		{
			$fields = $this->field_array_filter_selected ($input['rf-sel'], $fields);
		}

		$email = $this->build_report_email_array ($input);

		$report->id = Guid::make ();
		$report->name = $input['nm'];
		$report->description = $input['dc'];
		$report->status = $input['st'];
		$report->email = ($email != null) ? json_encode ($email) : null;
		$report->last_generated = ($input['lg'] != '') ? $this->granularity_hour ($input['lg']) : null;
		$report->reportjson = json_encode ($this->build_report_array ($input['gr'], $fields, $input['fr'], $input['fl']));
 		$report->save ();

 		return Redirect::route('report.index')->with('flashmessage', array('message' => "Report <em>" . $report->name . "</em> created", 'class' => "success"));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show ($id)
	{
		$report = $this->report->find ($id);

		return View::make('reports.show')->with([ 'report' => $report, 'page_title' => 'Lead Report: ' . $report->name , 'menu' => array('report') ]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit ($id)
	{
		$report = $this->report->find ($id);
		$group = Group::find (json_decode ($report->reportjson)->group);

		return View::make('reports.edit')->with
		([ 
			'report' => $report,
			'group' => $group,
			'page_title' => 'Edit ' . $report->name, 
			'menu' => array('reports', 'edit_report')
		]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update ($id)
	{
		$input = Input::all ();
		$separator_pattern = '/([\w\.\@]+)[\,\;\s]?/';
		preg_match_all ($separator_pattern, $input['em-to'], $matches);

		if (isset ($matches[1]) && is_array ($matches[1]))
		{
			$input['em-to'] = $matches[1];
		}

		$report = new Report;
		$fields = $this->build_field_array ($input);

		if (!$report->isValid ($input))
		{
			return Redirect::back()->withInput()->withErrors($report->error_messages);
		}
		else
		{
			$fields = $this->field_array_filter_selected ($input['rf-sel'], $fields);
		}

		$report = $this->report->find ($id);

		$email = $this->build_report_email_array ($input);
		$group_id = json_decode ($report->reportjson)->group;

		$report->id = Guid::make ();
		$report->name = $input['nm'];
		$report->description = $input['dc'];
		$report->status = $input['st'];
		$report->email = ($email != null) ? json_encode ($email) : null;
		$report->last_generated = ($input['lg'] != '') ? $this->granularity_hour ($input['lg']) : null;
		$report->reportjson = json_encode ($this->build_report_array ($group_id, $fields, $input['fr'], $input['fl']));
 		$report->save ();

 		return Redirect::route('report.index')->with('flashmessage', array('message' => "Report <em>" . $report->name . "</em> updated", 'class' => "success"));

	}

	public function confirmdestroy ($id)
	{
		$report = $this->report->find ($id);
		return View::make('reports.confirmdelete')->with([ 'report' => $report, 'menu' => array('report') ]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy ($id)
	{
		$report = $this->report->find ($id);
		$the_name = $report->name;

		$report->delete ();
		return Redirect::route ("report.index")->with ("flashmessage", array('message' => "Lead Report <em>" . $report->name . "</em> deleted", 'class' => "success"));
	
	}

	private function init_last_generated ($report)
	{
		/* set lead group created datetime as last_generated */
		$reportjson = json_decode ($report->reportjson);
		$group = Group::find ($reportjson->group);
		$last_generated = new Carbon ($group->created_at);

		/* granularity by smallest report frequency setting (hour) only */
		$last_generated->minute = 0;
		$last_generated->second = 0;

		$report->last_generated = $last_generated->toDateTimeString ();
		$report->save ();
		Log::info ('Report "' . $report->name . '" : last_generated >> ' . $report->last_generated);
	}

	private function report_get_next_generate ($report)
	{
		$reportjson = json_decode ($report->reportjson);
		$next_generate = new Carbon ($report->last_generated);

		switch ($reportjson->frequency)
		{
			case 'hour':
				$next_generate->addHours(1);
				break;

			case 'day':
				$next_generate->addDays(1);
				break;

			case 'week':
				$next_generate->addWeeks(1);
				break;

			case 'fortnight':
				$next_generate->addWeeks(2);
				break;

			case 'month':
				$next_generate->addMonths(1);
				break;

			default:
				break;
		}

		return $next_generate;
	}

	private function report_noqueue ($report)
	{
		$reportjson = json_decode ($report->reportjson);
		$next_generate = $this->report_get_next_generate ($report)->toDateTimeString ();

		$queued_leads_count = DB::table ('lead')
			->where ('status', 'queued')
			->where ('group', $reportjson->group)
			->where ('istestlead', false)
			->whereBetween ('created_at', [$report->last_generated, $next_generate])
			->count ();

		if ($queued_leads_count == 0)
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	private function report_due ($report)
	{
		$returnval = false;

		if ($report->last_generated == null)
		{
			$this->init_last_generated ($report);
		}

		$last_generated = new Carbon ($report->last_generated);
		
		$now = Carbon::now ();
		$difference = $last_generated->diff ($now);

		$reportjson = json_decode ($report->reportjson);

		switch ($reportjson->frequency)
		{
			case 'hour':
				if ($difference->h >= 1 || $difference->days > 0)
				{
					$returnval = true;
				}
				break;

			case 'day':
				if ($difference->days >= 1)
				{
					$returnval = true;
				}
				break;

			case 'week':
				if ($difference->days >= 7)
				{
					$returnval = true;
				}
				break;

			case 'fortnight':
				if ($difference->days >= 14)
				{
					$returnval = true;
				}
				break;

			case 'month':
				if ($difference->m >= 1)
				{
					$returnval = true;
				}
				break;

			default:
				break;
		}

		return $returnval;

	}

	/*
	  Report Criteria: 
 		. by lead group, 
 		. status = 'pass', 
 		. istestlead = false, 
 		. within datetime window of report frequency relative to report last_generated timestamp
	 */
	private function build_report_query ($report)
	{
		$lead_columns = Schema::getColumnListing('lead');

		$reportjson = json_decode ($report->reportjson);

		$columns = [];
		$condition = [];

		$last_generated = new Carbon ($report->last_generated);
		$next_generate = new Carbon ($report->last_generated);

		$next_generate = $this->report_get_next_generate ($report);

		foreach ($reportjson->fields as $field => $properties)
		{
			if (in_array ($field, $lead_columns))
			{
				// core field
				$column_str = '"' . $field . '"';
			}
			else
			{
				// JSON field
				$column_str = '"datajson"->>\'' . $field . '\'';
			}

			if (isset ($properties->alias))
			{
				$columns[] = $column_str . ' AS "' . $properties->alias . '"';
			}
			else
			{
				$columns[] = $column_str;
			}

			if (isset ($properties->option))
			{
				switch ($properties->option)
				{
					case 'not_empty':
						$condition[] = $column_str . ' <> \'\'';
						break;

					case 'equals':
						if (isset ($properties->val1) && $properties->val1 != '')
						{
							$condition[] = $column_str . ' LIKE \'' . $properties->val1 . '\'';
						}
						break;

					case 'not':
						if (isset ($properties->val1) && $properties->val1 != '')
						{
							$condition[] = $column_str . ' NOT LIKE \'' . $properties->val1 . '\'';
						}
						break;

					case 'contains':
						if (isset ($properties->val1) && $properties->val1 != '')
						{
							$condition[] = $column_str . ' = \'' . $properties->val1 . '\'';
						}
						break;

					case 'less':
						if (isset ($properties->val1) && $properties->val1 != '')
						{
							$condition[] = $column_str . ' < \'' . $properties->val1 . '\'';
						}
						break;

					case 'less_equal':
						if (isset ($properties->val1) && $properties->val1 != '')
						{
							$condition[] = $column_str . ' <= \'' . $properties->val1 . '\'';
						}
						break;

					case 'between':
						if (isset ($properties->val1) && $properties->val1 != '' && isset ($properties->val2) && $properties->val2 != '')
						{
							$condition[] = $column_str . ' BETWEEN \'' . $properties->val1 . '\' AND \'' . $properties->val2 . '\'';
						}
						break;

					case 'more_equal':
						if (isset ($properties->val1) && $properties->val1 != '')
						{
							$condition[] = $column_str . ' >= \'' . $properties->val1 . '\'';
						}
						break;

					case 'more':
						if (isset ($properties->val1) && $properties->val1 != '')
						{
							$condition[] = $column_str . ' > \'' . $properties->val1 . '\'';
						}
						break;

					case 'empty':
						if (isset ($properties->val1) && $properties->val1 != '')
						{
							$condition[] = $column_str . ' = \'\'';
						}
						break;

					default:
						break;
				}

			}

		}

		/* add created_at also: */
		$columns[] = '"created_at" AS "received"';

		$condition[] = '"group" = \'' . $reportjson->group . '\'';
		$condition[] = '"status" = \'pass\'';
		$condition[] = '"istestlead" = false';
		$condition[] = '"created_at" BETWEEN \'' . $last_generated->toDateTimeString () . '\' AND \'' . $next_generate->toDateTimeString () . '\'';

		$order = ' ORDER BY "created_at" DESC';

		$columns = implode (', ', $columns);
		$condition = implode (' AND ', $condition); 

		$query_str = 'SELECT ' . $columns . ' FROM lead WHERE ' . $condition . $order;

		return $query_str;

	}

	private function get_report_csv_filename ($report, $timenow = false)
	{
		$reportjson = json_decode ($report->reportjson);
		$report_filename = $reportjson->filename;
		$last_generated = new Carbon ($report->last_generated);
		
		if ($timenow == true)
		{
			$last_generated->hour = (int) date ('H');
			$last_generated->minute = (int) date ('i');
		}

		$date_string = $last_generated->format ('Ymd-Hi');

		return $date_string . '__' . $report_filename . '.csv';
	}

	private function increment_report_generated_datetime ($report)
	{
		$reportjson = json_decode ($report->reportjson);
		$next_generate = new Carbon ($report->last_generated);

		$next_generate = $this->report_get_next_generate ($report);

		$report->last_generated = $next_generate->toDateTimeString ();
		$report->save ();

	}

	private function report_email ($report, $email_template, $data = null, $csv_file = null)
	{
		$emailjson = json_decode ($report->email);

		if ($data != null) 
		{
			$data['email_body'] = $emailjson->message;
		}
		else
		{
			$data['report_name'] = $report->name;
		}

		if ($csv_file != null)
		{
			/* reconstruct email JSON object */
			$emailjson = json_decode (json_encode ($emailjson), true);
			$emailjson['attach'] = $csv_file;
			$emailjson = json_decode (json_encode ($emailjson));
		}

		Mail::send ($email_template, $data, function ($message) use ($emailjson)
		{
	    $message->from ($emailjson->from);
	    $message->to ($emailjson->to);
	    $message->subject ($emailjson->subject);
	    
	    if (isset ($emailjson->attach)) 
	    {
	    	$message->attach ($emailjson->attach);
	    }
		});
	}

	/*
	 * Report cron job 
	 */
	public function report_cron ()
	{
		$returnstr = [];
		$active_reports = $this->report->where ('status', 'active')->orderBy ('created_at', 'asc')->get ();
		
		foreach ($active_reports as $report)
		{ 
			if ($this->report_due ($report))
			{
				if ($this->report_noqueue ($report))
				{
					$report_query = $this->build_report_query ($report);
					$report_result = DB::select ($report_query);
					
					if (count ($report_result) > 0)
					{
						$report_array = json_decode (json_encode ((array) $report_result), true);
						$header_string = array_keys ($report_array[0]);
						$filename = $this->get_report_csv_filename ($report, true);
						$csv_file = base_path() . '/public/download/' . $filename;

						$fp = fopen ($csv_file, 'w');
						
						fputcsv ($fp, $header_string);

						foreach ($report_array as $row)
						{
							fputcsv ($fp, $row);
						}

						fclose ($fp);

						if ($report->email != null)
						{
							$data['email_csv'] = route ('report.download', $filename);
							$this->report_email ($report, 'emails.report', $data, $csv_file);
						}

						$returnstr[$report->id] = 'Report ' . $report->name . ' generated : ' . $csv_file;
						Log::info ($returnstr[$report->id]);
					}
					else
					{
						if ($report->email != null)
						{
							$this->report_email ($report, 'emails.no_report');
						}

						$returnstr[$report->id] = 'Report ' . $report->name . ' : no records';
						Log::info ($returnstr[$report->id]);
					}

					$this->increment_report_generated_datetime ($report);

				}
				else
				{
					$returnstr[$report->id] = 'Report ' . $report->name . ' deferred - leads still in queue';
					Log::info ($returnstr[$report->id]);

				}

			}
		}

		
		$returnstr['status'] = date('Y-m-d H:i:s') . ' : Report cron run complete';
		Log::info ($returnstr['status']);

		return Response::json ($returnstr);
	}

	public function report_download_file ($filename)
	{
		$csv_file = base_path() . '/public/download/' . $filename;
		if (file_exists ($csv_file))
		{
			header ('Content-Type: application/csv');
	    header ('Content-disposition: attachment; filename=' . $filename);
	    readfile ($csv_file);
    } 
    else 
    {
    	return false;
		}

	}

}
