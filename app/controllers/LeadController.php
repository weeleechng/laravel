<?php

class LeadController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $lead;

	private $page_title_append;
	private $page_description; 
	private $page_status;

	public function __construct (Lead $lead)
	{
		$this->lead = $lead;
	}

	private function index_by_url_params ($op1 = null, $op1_column = null, $op1_value = null, $op2 = null, $op2_column = null, $op2_value = null)
	{
		$perpage = 30;

		$leads = $this->lead;

		$this->page_title_append = null;
		$this->page_description = null;
		$this->page_status = null;

		if ($op1 == 'where')
		{
			if (stristr ($op1_value, ','))
			{
				$leads = $leads->whereIn ($op1_column, explode (',', $op1_value));
			}
			else
			{
				$leads = $leads->where ($op1_column, $op1_value);
			}

			switch ($op1_value)
			{
				case 'queued':
					$this->page_title_append = 'Pending';
					$this->page_description = 'Newly-received leads that are pending queue processing. May contain duplicates.';
					$this->page_status = 'pending';
					break;

				case 'fail,duplicate':
				case 'duplicate,fail':
					$this->page_title_append = 'Invalid';
					$this->page_description = 'Leads that <code>fail</code> validation rules.';
					$this->page_status = 'invalid';
					break;

				default:
					$this->page_title_append = $op1_value;
			}

			if ($op1_column == 'istestlead' && $op1_value == 'true')
			{
				$this->page_title_append = 'tests';
				$this->page_description = 'Test leads that will be processed as regular leads, but will not be posted to external URLs.';
				$this->page_status = 'invalid';
			}

		}

		if (isset ($this->page_title_append))
		{
			$this->page_title_append = ' - ' . $this->page_title_append;
		}

		if ($op1 == 'orderby')
		{
			$leads = $leads->orderBy ($op1_column, $op1_value);
		}

		if ($op2 == 'orderby')
		{
			$leads = $leads->orderBy ($op2_column, $op2_value);
		}

		// display valid leads by default
		if ($op1 != 'where' || $op1_column != 'status' && $op1_column != 'istestlead')
		{
			$leads = $leads->where ('status', 'pass');
			$leads = $leads->where ('istestlead', false);
		}

		// exclude test leads by default
		if ($op1_column != 'istestlead')
		{
			$leads = $leads->where ('istestlead', false);
		}

		// order by created_at desc by default
		if ($op1 != 'orderby' && $op2 != 'orderby')
		{
			$leads = $leads->orderBy ('created_at', 'desc');
		}

		$leads = $leads->paginate ($perpage);
		// $leads = $leads->get ();

		return $leads;

	}

	public function index ($op1 = null, $op1_column = null, $op1_value = null, $op2 = null, $op2_column = null, $op2_value = null)
	{
		$leads = $this->index_by_url_params ($op1, $op1_column, $op1_value, $op2, $op2_column, $op2_value);

		// $leads = $this->lead->where('status', 'pass')->orderBy(($sort_by != null) ? $sort_by : 'created_at', ($sort_direction != null) ? $sort_direction : 'desc')->get();
		$with_array = 
		[ 
			'leads' => $leads, 
			'page_title' => 'Leads' . $this->page_title_append,
			'description' => $this->page_description,
			'status' => $this->page_status,
			'menu' => array('lead', 'all_leads'), 
		];
		
		return View::make('leads.index')->with($with_array);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	private function get_mappable_fields ($group_rules)
	{
		$returnarr = [];
		$fields = json_decode ($group_rules);

		foreach ($fields as $field => $properties)
		{
			if (isset ($properties->map))
			{
				$returnarr[$properties->map] = $field;
			}
		}

		return $returnarr;

	}

  private function has_defaultvalue ($properties)
  {
    if (isset ($properties->defaultvalue))
    {
      return $properties->defaultvalue;
    }
    else
    {
      return false;
    }
  }

	private function input_group_only ($group_rules, $input)
	{
		/* Store only fields defined in the group configuration */
		$returnarr = [];

		$fields = json_decode ($group_rules);
		foreach ($fields as $field => $properties)
		{
      $defaultvalue = $this->has_defaultvalue ($properties);

			if (isset ($input[$field]))
			{
				$returnarr[$field] = ($input[$field] == '' && $defaultvalue != false) ? $defaultvalue : $input[$field];
			}
      else
      {
        $returnarr[$field] = $defaultvalue;
      }
		}

		return $returnarr;
	}

	private function has_testvalue ($input, $group_rules)
	{
		if (isset ($input['lead_test']) && $input['lead_test'] == '1')
		{
			return true;
		}
		
		$fields = json_decode ($group_rules);
		foreach ($fields as $field => $properties)
		{
			if (isset ($properties->testvalue) && isset ($input[$field]) && $properties->testvalue == $input[$field])
			{
				return true;
			}
		}

		return false;
	}

	/* Get data stored in cache or null trying */
	private function get_cache ($key)
	{
		// Log::info ('Cache check: ' . $key);
		if (Cache::has ($key))
		{
			// Log::info ('Cache get: ' . $key);
		  return Cache::get ($key);
		}
		else
		{
			return null;
		}
	}

	/**
	 * Prepare lead object for storage.
	 * Input: $lead, $group, $input, status message, remarks (json)
	 *
	 * @return Response
	 */	
	private function build_lead_object ($lead, $group_rules, $input, $status, $remarks)
	{
		$map_fields = $this->get_mappable_fields ($group_rules);

		$lead->id = Guid::make();
		$lead->group = substr ($input['lead_group'], 0, 64);
		$lead->lastname = (isset ($input[$map_fields['lastname']])) ? substr ($input[$map_fields['lastname']], 0, 255) : '';
		$lead->firstname = (isset ($input[$map_fields['firstname']])) ? substr ($input[$map_fields['firstname']], 0, 255) : '';
		$lead->email = (isset ($input[$map_fields['email']])) ? substr ($input[$map_fields['email']], 0, 255) : '';
		$lead->contactno = (isset ($input[$map_fields['contactno']])) ? substr ($input[$map_fields['contactno']], 0, 32) : '';
		$lead->ipaddress = (isset ($input['transfer_ip_address'])) ? $input['transfer_ip_address'] : Request::getClientIp();
		$lead->urlsource = (isset ($input['transfer_source_url'])) ? $input['transfer_source_url'] : Request::server ('HTTP_REFERER');
    $lead->useragent = Request::server ('HTTP_USER_AGENT');
		$lead->status = $status;
		$lead->remarks = $remarks;
		$lead->datajson = json_encode ($this->input_group_only ($group_rules, $input));
		
		/* mark test leads as such: */
		$lead->istestlead = $this->has_testvalue ($input, $group_rules);
		/*
		if (isset ($input['lead_test']) && $input['lead_test'] == '1')
		{
			$lead->istestlead = true;
		}
		*/

		return $lead;

	}

	private function build_error_array ($errors)
	{
		$returnstr = [];
		$returnstr['status'] = 'fail';

		foreach (json_decode($errors, true) as $error => $details)
		{
			$returnstr['errors'][$error] = $details[0];
		}

		return $returnstr;
	}

  /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all ();
		$lead = $this->lead;

		$group_id = $input['lead_group'];

		/* Try to get group info from cache: */
		$group_status = $this->get_cache ($group_id . '_status');
		$group_external = $this->get_cache ($group_id . '_external');
		$group_rulejson = $this->get_cache ($group_id . '_rulejson');
		$group_instantcron = $this->get_cache ($group_id . '_instantcron');

    /* Group info not in cache. Try to get the group from dB: */
		if ($group_status == null && $group_rulejson == null)
		{
			$group = Group::find ($input['lead_group']);

			if (!isset ($group))
			{
				/* Lead Group not found. Abort. */
				$returnstr = [];
				$returnstr['status'] = 'fail';

				return Response::json ($returnstr);
			}
			else
			{
				$group_status = $group->status;
				$group_external = $group->external;
				$group_rulejson = $group->rulejson;
				$group_instantcron = $group->instantcron;
			}

		}

    /* Check for presence of honeypots and if they are filled: */
    if ($lead->isHoneypotFilled ($input, json_decode ($group_rulejson, true)))
    {
      $returnstr = [];
      $returnstr['honeypot'] = 'not empty or absent';

      $lead = $this->build_lead_object ($lead, $group_rulejson, $input, 'fail', json_encode ($returnstr));
      $lead->save ();

      $bluffstr = [];
      $bluffstr['status'] = 'success';
      
      Log::warning ('Suspected spambot entry: ' . $lead->id);    
      return Response::json ($bluffstr); /* FAKE success message */
    }

    /* If group is not running, abort */
		if ($group_status != 'running')
		{
			$returnstr = [];
			$returnstr['status'] = 'fail';
			$returnstr['errors']['lead_group'] = $group_id . ' is not running';

			return Response::json ($returnstr);
		}

		/* Perform system lead validation: */
		if (!$lead->isValid ($input))
		{
			$errors = $lead->error_messages;
			$returnstr = $this->build_error_array ($errors);

			$lead = $this->build_lead_object ($lead, $group_rulejson, $input, 'fail', json_encode ($returnstr['errors']));
			$lead->save ();

			return Response::json ($returnstr);
		}

		/* Perform validation according to group rules: */
		if (!$lead->isValidFields ($input, json_decode ($group_rulejson, true)))
		{
			$errors = $lead->error_messages;
			$returnstr = $this->build_error_array ($errors);

			$lead = $this->build_lead_object ($lead, $group_rulejson, $input, 'fail', json_encode ($returnstr['errors']));
			$lead->save ();

			return Response::json ($returnstr);
		}

		/* Input is valid: */
		$lead = $this->build_lead_object ($lead, $group_rulejson, $input, 'queued', null);
		$lead->save ();

		$returnstr['status'] = 'success';

		if (isset ($group_instantcron) && $group_instantcron == true)
		{
			Log::info ('Lead ' . $lead->id . ' : instant cron');
			$this->lead_cron_selected ($lead->id);
		}

		return Response::json ($returnstr);
	}

  private function field_hasrule ($properties, $baserule)
  {
    if (isset ($properties->rules))
    {
      foreach ($properties->rules as $rule => $value)
      {
        if ($value->baserule == $baserule)
        {
          return true;
        }
      }
    }

    return false;
  }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show ($id)
	{
		$lead = $this->lead->find ($id);
    $fields = json_decode ($lead->datajson, true);
		$group = Group::find ($lead->group);
		$displaynames = [];
		
    foreach (json_decode ($group->rulejson) as $field => $properties)
		{
      /* ignore honeypot fields, unless they are filled */
      if ($this->field_hasrule ($properties, 'honeypot') && (isset ($fields[$field]) && $fields[$field] == ''))
      {
        unset ($fields[$field]);
      }
      else
      {
        $displaynames[$field] = $properties->displayname;
      }
		}
		
		return View::make ('leads.show')->with (['lead' => $lead, 'fields' => $fields, 'displaynames' => $displaynames, 'page_title' => 'Lead: '. $lead->lastname . ' ' . $lead->firstname, 'menu' => array('lead')]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

  /* Check cache or dB for external post URL availability */
  private function lead_get_external ($lead)
  {
		$group_external = $this->get_cache ($lead->group . '_external');

		if ($group_external == null || $group_external == '')
		{
			/* check dB for external post URL: */
			$lead_group = Group::find ($lead->group);
			if ($lead_group->has_external() != false)
			{
				$group_external = $lead_group->external;
			}
			// Log::info ('dB check: ' . $lead->group);
		}

		return $group_external;
  }

	private function extractPageText ($raw_text)
	{
    /* Get the file's character encoding from a <meta> tag */
    preg_match( '@<meta\s+http-equiv="Content-Type"\s+content="([\w/]+)(;\s+charset=([^\s"]+))?@i', $raw_text, $matches );
    @$encoding = $matches[3];
     
    /* Convert to UTF-8 before doing anything else */
    $utf8_text = iconv( $encoding, "utf-8", $raw_text );
     
    /* Strip HTML tags and invisible text */
    $utf8_text = strip_tags( $utf8_text );
     
    /* Decode HTML entities */
    $utf8_text = html_entity_decode( $utf8_text, ENT_QUOTES, "UTF-8" );

    return  $utf8_text;
  }

  private function post_remove_exclude ($lead)
  {
    $group = Group::find ($lead->group);
    $rulejson = json_decode ($group->rulejson);
    $leaddata = json_decode ($lead->datajson, true);

    foreach ($rulejson as $field => $properties)
    {
      if (!isset ($properties->extpost))
      {
        unset ($leaddata[$field]);
      }
    }

    return $leaddata;
  }

	public function post_to_external ($id)
  {
  	$lead = Lead::find ($id);
  	$group_external = $this->lead_get_external ($lead);
  	
  	if ($group_external == null)
  	{
  		$error = ['external' => ['External POST URL not set for this lead group']];
  		// external post URL not set
  		return Response::json ($this->build_error_array (json_encode ($error)));
  	}
  	
    $leaddata = $this->post_remove_exclude ($lead);
    
    $postdata = "";
    foreach ($leaddata as $key => $value)
    {
      $postdata .= $key . "=" . $value . "&";
    }
    rtrim ($postdata, "&");
    
    $posturl = $group_external;
    
    $leadpost = curl_init ();//open connection    
    curl_setopt ($leadpost, CURLOPT_URL, $posturl);//set the url, number of POST vars, POST data
    curl_setopt ($leadpost, CURLOPT_POSTFIELDS, $postdata);     
    $result = curl_exec ($leadpost);//execute post    
    $curl_errno = curl_errno ($leadpost); // Added logger
    $curl_error = curl_error ($leadpost); 
    curl_close ($leadpost);//close connection 

    $cleanout = ob_get_clean ();

    if ($curl_errno > 0) 
    {
      $logs = array
      (
        "logs"          => $curl_error,
        "type"          => 'ERROR'
      ); 
    } 
    else 
    {
      if (empty ($result)) 
      { 
        $logs = array
        (
          "logs"          => $this->extractPageText($cleanout),
          "type"          => 'CURL NOTICE'
        );   
      } 
      else 
      { 
        $logs = array
        (
          "logs"          => $this->extractPageText($cleanout),
          "type"          => 'NOTICE'
        );   
      }
    }

    $lead->extlog = json_encode ($logs);
    Log::info($posturl . ', ' . $id . ', ' . $lead->extlog);
    $lead->save ();

    $returnstr['status'] = 'External POST complete';
    return Response::json ($returnstr);
  }

  /* Use asynchronous CURL request */
	private function lead_start_external_post ($lead)
  {
  	$path_url = 'lead/external/' . $lead->id;
  	CurlAsync::make_request_internal ($path_url);
  }

	/*
	 * A.K.A. the double-click trap
	 * Sometimes the user or vendor double-clicks the submit button.
	 * The database unique validation cannot catch this on time.
	 * Therefore, if the queued lead is almost identical then mark it as duplicate.
	 */
	private function lead_cron_hasduplicate ($lead)
	{
		$returnval = false;
		$threshold = 9; // seconds to mark leads with identical core values as duplicate

		// get selected core fields
		$matches = DB::table ('lead')
			->whereNotIn ('id', array ($lead->id))
			->where ('group', $lead->group)
			->where ('lastname', $lead->lastname)
			->where ('firstname', $lead->firstname)
			->where ('email', $lead->email)
			->where ('contactno', $lead->contactno)
			->where ('ipaddress', $lead->ipaddress)
			->where ('urlsource', $lead->urlsource)
			->where ('status', 'pass')
			->where ('istestlead', $lead->istestlead)
			->get();

		if (count($matches) > 0)
		{
			$datetimeformat = 'Y-m-d H:i:s';

			foreach ($matches as $match)
			{
				$lead_created = new DateTime ($lead->created_at);
				$match_created = new DateTime ($match->created_at);
				$diff = date_diff ($lead_created, $match_created, true);
				$seconds = $diff->s + ($diff->i * 60) + ($diff->h * 3600) + ($diff->d * 24 * 86400);

				if ($seconds < $threshold)
				{
					$returnval = true;
					break;
				}
			}
		}

	  return $returnval;
	}

	private function get_returnstr_array ()
	{		
  	$returnstr = [];
		$returnstr['processed'] = 0;
		$returnstr['pass'] = 0;
		$returnstr['fail'] = 0;
		$returnstr['test'] = 0;
		$returnstr['external'] = 0;

		return $returnstr;
	}

  private function lead_cron_process ($lead)
  {
  	$returnstr = [];

  	if ($lead->isValidUnique())
		{
			$lead->status = 'pass';
			$returnstr[] = 'pass';
		}
		else
		{
			$lead->status = 'fail';
			$lead->remarks = json_encode ($lead->error_messages);
			$returnstr[] = 'fail';
		}

		if ($lead->istestlead == true)
		{
			$returnstr[] = 'test';
		}

		/* no external post for test leads */
		if ($lead->status == 'pass' && $lead->istestlead == false)
		{
			/* check cache for external post URL: */
			$group_external = $this->lead_get_external ($lead);

			if ($group_external != null)
			{
				$this->lead_start_external_post ($lead);
				$returnstr[] = 'external';
			}
		}

		$lead->save ();
		$returnstr[] = 'processed';

		return $returnstr;
  }

	/*
	  Ad-hoc lead cron a.k.a. cron-on-demand:
	*/
	public function lead_cron_selected ($id)
  {
  	$lead = $this->lead->find ($id);
  	
  	if ($lead->status == 'queued')
  	{
	  	$result = $this->lead_cron_process ($lead);
  	}

  	$returnstr = $this->get_returnstr_array ();

  	foreach ($result as $key => $value)
  	{
  		$returnstr[$value]++;
  	}

  	foreach ($returnstr as $key => $value)
  	{
  		if ($value == 0)
  		{
  			unset ($returnstr[$key]);
  		}
  	}

  	$returnstr['status'] = 'Lead cron ' . $lead->id . ' complete';

  	Log::info('Cron summary: ' . json_encode ($returnstr));
  	return Response::json ($returnstr);

  }

  /*
	  Scheduled task to run at specific interval.
	*/
	public function lead_cron ()
	{
		$time_start = time ();

		$returnstr = $returnstr = $this->get_returnstr_array ();

		$queued_leads = $this->lead->where('status', 'queued')->orderBy ('created_at', 'desc')->get();
		
		foreach ($queued_leads as $queued_lead)
		{
			$queued_result = $this->lead_cron_process ($queued_lead);

	  	foreach ($queued_result as $key => $value)
	  	{
	  		$returnstr[$value]++;
	  	}
		}

		foreach ($returnstr as $key => $value)
  	{
  		if ($value == 0)
  		{
  			unset ($returnstr[$key]);
  		}
  	}

		$time_taken = time () - $time_start;

		$returnstr['status'] = 'Lead cron run complete';
		$returnstr['time'] = date ('H:i:s', $time_taken);

		Log::info('Cron summary: ' . json_encode ($returnstr));
		return Response::json ($returnstr);
	}

}
