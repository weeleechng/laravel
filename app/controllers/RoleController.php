<?php

class RoleController extends \BaseController {

	protected $role;

	public function __construct (Role $role)
	{
		$this->role = $role;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$roles = $this->role->orderBy ('name', 'asc')->get (); // Role::all ();
		return View::make('roles.index')->with([ 'roles' => $roles, 'page_title' => 'Manage roles', 'menu' => array('settings', 'manage_roles')]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$permissions = Permission::all ();
		return View::make('roles.create')->with(['permissions' => $permissions, 'page_title' => 'Manage roles', 'menu' => array('settings', 'manage_roles')]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$role = new Role;
		$permissions = Permission::all();
		$input = Input::all();
		if (!$role->isValid ($input))
		{
			return Redirect::back()->withInput()->withErrors($role->error_messages);
		}
		
		$role->id = Guid::make();
		$role->name = $input['nm'];
		$role->description = $input['dc'];

		$permission_array = array ();

		foreach ($permissions as $permission)
		{
			$permission_array[$permission->id] = "false";
		}

		foreach ($input['pm'] as $pm)
		{
			$permission_array[$pm] = "true";
		}

		$role->permissionjson = json_encode ($permission_array);

		$role->save ();

		return Redirect::route("roles.index")->with ("flashmessage", array('message' => "Role <strong>" . $role->name . "</strong> created", 'class' => 'success' ));

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$role = $this->role->find ($id);
		$permissions = Permission::all ();
		return View::make('roles.show')->with([ 'role' => $role, 'permissions' => $permissions, 'page_title' => 'Manage roles', 'menu' => array('settings', 'manage_roles') ]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$role = $this->role->find ($id);
		$permissions = Permission::all();
		return View::make('roles.edit')->with([ 'role' => $role, 'permissions' => $permissions, 'page_title' => 'Manage roles', 'menu' => array('settings', 'manage_roles') ]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$role = $this->role->find ($id);

		$input = Input::all();

		if (!$role->isValid ($input))
		{
			return Redirect::back()->withInput()->withErrors($role->error_messages);
		}

		$pm = Input::get ('pm');
		$current_pm = json_decode ($role->permissionjson, true);

		/* set all to false */
		foreach ($current_pm as $key => $value)
		{
			$current_pm[$key] = "false";
		}

    /* set selected keys to true */
		foreach ($pm as $key => $value)
		{
			$current_pm[$value] = "true";
		}

    /* update and save Role object */
		$role->name = $input['nm'];
		$role->description = $input['dc'];
		$role->permissionjson = json_encode ($current_pm);
		$role->save ();

		return Redirect::route("roles.index")->with ("flashmessage", array('message' => "Role <strong>" . $role->name . "</strong> updated", 'class' => 'success') );
	}

	public function confirmdestroy($id)
	{
		$role = $this->role->find ($id);
		return View::make('roles.confirmdelete')->with(['role' => $role, 'page_title' => 'Manage roles', 'menu' => array('settings', 'manage_roles')]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$role = $this->role->find ($id);

		if ($role->canbedeleted == false)
		{
		  return Redirect::route ("roles.index")->with ("flashmessage", "Request denied: Role <strong>" . $the_name . "</strong> cannot be deleted");	
		}

		$the_name = $role->name;
		$role->delete ();
		return Redirect::route ("roles.index")->with ("flashmessage", array('message' => "Role <strong>" . $the_name . "</strong> deleted", 'class' => 'success' ));
	}


}
