<?php

class UsersController extends \BaseController 
{

	protected $user;
	protected $loginuser;

	public function __construct (User $user)
	{
		$this->user = $user;
	}

	private function get_loginuser()
	{
		$this->loginuser = Auth::user();
	}

	private function get_select_roles ()
	{
		$roles = Role::all();
		$role_array = array (''=> '- select -');
	  foreach ($roles as $role)
	  {
	    $role_array[$role->id] = $role->name;
	  }

	  return $role_array;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->get_loginuser();
		// $users = DB::table('users')->orderBy('id', 'asc')->get();
		$users = $this->user->orderBy('status', 'asc')->orderBy('id', 'asc')->get();
		return View::make('users.index')->with([
			'users' => $users, 
			'current_user' => $this->loginuser->id, 
			'menu' => array('settings', 'manage_users')
			]);
	}

  public function bystatus($status)
	{
		$this->get_loginuser();
		// $users = DB::table('users')->where('status', $status)->orderBy('id', 'asc')->get();
		$users = $this->user->where('status', $status)->orderBy('id', 'asc')->get();
		return View::make('users.index')->with(['users' => $users, 'status' => $status, 'current_user' => $this->loginuser->id ]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create')->with([ 'roles' => $this->get_select_roles(), 'menu' => array('settings', 'manage_users'), 'page_title' => 'Add User']);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$user = new User;
		$input = Input::all();

		if (!$this->user->isValid ($input))
		{
			return Redirect::back()->withInput()->withErrors($this->user->error_messages);
		}

		$user = new User;
		$user->id = $input['newid']; // Input::get('id');
		$user->name = $input['name']; // Input::get('nm');
		$user->password = Hash::make ($input['newpassword']); // (Input::get('pw'));
		$user->email = $input['email']; // Input::get('em');
		$user->role = $input['role']; // Input::get('rl');
		$user->status = $input['status']; // Input::get('st');
		$user->save();

		// $this->user->create($input); // cannot use because HTML form and dB fieldnames different

		return Redirect::route('users.index')->with("flashmessage", array('message' =>"User <strong>" . $user->name . "</strong> created", 'class' => 'success') );
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// $user = DB::table('users')->where('id', $id)->first();
		$permissions = Permission::all ();
		$user = $this->user->where('id', $id)->first();
		return View::make('users.show')->with([ 
			'page_title' => $user->name,
			'user' => $user, 
			'permissions' => $permissions,
			 ]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->get_loginuser ();

		$the_user = $this->user->find ($id);
		$permissions = json_decode ($this->loginuser->userrole->permissionjson);

		/* Prevent user 'admin' role and status from being changed */
		if (!$this->user->is_admin($id) && $permissions->manage_users == "true")
		{
			$roles_select = $this->get_select_roles();
		}
		else
		{
			$roles_select = false;
		}

		/* Prevent non-'admin' users from changing 'admin' password */
		if (!$this->user->is_admin($this->loginuser->id) && $this->user->is_admin($id))
		{
			$editpassword = false;
		}
		else
		{
			$editpassword = true;
		}

		return View::make('users.edit')->with([
			'page_title' => 'Edit user', 
			'user' => $the_user,
			'roles' => $roles_select,
			'edit_password' => $editpassword ]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$the_user = $this->user->find ($id);

		// $user = new User;
		$input = Input::all();

		if (!$the_user->isValid ($input))
		{
			return Redirect::back()->withInput()->withErrors($the_user->error_messages);
		}

		$the_user->name = $input['name']; // Input::get('nm');
		if (isset ($input['changepassword'])) $the_user->password = Hash::make ($input['changepassword']); // (Input::get('pw'));
		$the_user->email = $input['email']; // Input::get('em');
		$the_user->role = $input['role']; // Input::get('rl');
		$the_user->status = $input['status']; // Input::get('st');
		$the_user->save ();

		// Users with edit user role 
		if(json_decode( Auth::getUser()->userrole->permissionjson )->manage_users == "true") {
			return Redirect::route("users.index")->with("flashmessage", array("message" => "User <strong>" . $the_user->name . "</strong> updated", "class" => "success") );
		}
		// Others 
		else {
			return Redirect::route("home")->with('flashmessage', array('message' => "User updated successfully", 'class' => "success"));
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function confirmdestroy($id)
	{
		$the_user = $this->user->find ($id);

		if ($the_user->userrole->canbedeleted == false || $this->user->is_admin ($the_user->id))
		{
			$can_delete = false;
		}
		else
		{
			$can_delete = true;
		}

		// multi-layer protection: role and ID check for user 'admin'
		return View::make('users.confirmdelete')->with([
			'page_title' => 'Delete User: ' .$the_user->name,
			'user' => $the_user, 
			'can_delete' => $can_delete]);
	}

	public function destroy($id)
	{
		$the_user = $this->user->find ($id);
		$the_name = $the_user->name;
		
		/* protect against delete via POST request also */
		if ($this->user->is_admin($the_user->id) || $the_user->userrole->canbedeleted == false)
		{
		  return Redirect::route ("users.index")->with ("flashmessage", array( "message" => "Request denied : User <strong>" . $the_name . "</strong> cannot be deleted", "class" => "alert"));	
		}

		$the_user->delete ();
		return Redirect::route ("users.index")->with ("flashmessage", array( "message" => "User <strong>" . $the_name . "</strong> deleted", "class" => "success"));
	}


	public function login()
	{
		$input = Input::only ('id', 'password');
		
		// Form Validation 
		if(! $this->user->isValid($input))
		{
			return Redirect::back()->withInput()->withErrors($this->user->error_messages);
		}

		// if log in fails redirect back with a message
		if (! Auth::attempt($input))
		{
			return Redirect::back()->withInput()->withErrors(array ('auth_msg' => 'Login failed, check your username and password.'));
		}

		// Otherwise redirect to home
		return Redirect::route('home');

	}

}
