<?php

class CronController extends \BaseController {

	public function cron ()
	{
		$lead_request = Request::create ('lead/cron', 'GET');
		$lead_response = Route::dispatch ($lead_request);

		$report_request = Request::create ('report/cron', 'GET');
		$report_response = Route::dispatch ($report_request);

		$respondstr = [];
		$respondstr['lead'] = $lead_response->getContent ();
		$respondstr['report'] = $report_response->getContent ();
		$respondstr['status'] = 'Completed';

		Log::info (json_encode ($respondstr));
		return Response::json ($respondstr);
	}

	public function index()
	{

	}

	public function create()
	{

	}

	public function store()
	{

	}

	public function show($id)
	{

	}

	public function edit($id)
	{

	}

	public function update($id)
	{

	}

	public function destroy($id)
	{

	}

}
