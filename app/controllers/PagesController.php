<?php

class PagesController extends BaseController
{
	public function house ()
	{
		/*
		$thename = 'visitor';
 	  return View::make('welcome')->with('name', $thename);
 	  */
 	  if (Auth::check())
 	  {
 	  	return View::make('authuser')->with (['user' => Auth::user(), 'menu' => array('home')]);
 	  }
 	  else
 	  {
 	    return Redirect::to('users/login');
 	  }
	}

	public function about ()
	{
 	  return View::make('about');
	}

	public function temp ()
	{
		return View::make ('message')->with(['head_title' =>'Coming Soon', 'message' => 'Under construction',]);
	}


	/* NOT IN USE: */

	public function database ()
	{
		
		$dbdata = DB::table('users')->orderBy('id', 'desc')->get();
		// $dbdata = User::orderBy('id')->get();
		return View::make('database')->with('users', $dbdata);
	}

	public function users ()
	{
		
		$active = DB::table('users')->where('status', 'active')->orderBy('id', 'asc')->get();
		$inactive = DB::table('users')->where('status', 'blocked')->orderBy('id', 'asc')->get();
		// $dbdata = User::orderBy('id')->get();
		return View::make('database.users')->with(['users' => $active, 'blocked' => $inactive]);
	}

	public function user ($userid)
	{
		$user = DB::table('users')->where('id', $userid)->first();
		return View::make('database.user')->with(['user' => $user]);
	}

}