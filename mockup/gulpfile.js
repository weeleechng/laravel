var gulp = require('gulp'),
	sass = require('gulp-ruby-sass');

var paths =  {
	sass: ['css/sass/*.sass'] 
};	

gulp.task('sass-compile', function () {
	return gulp.src(paths.sass)
		.pipe(sass())
		.pipe(gulp.dest('css'));
});

gulp.task('watch', function() {
	gulp.watch(paths.sass, ['sass-compile']);
});

gulp.task('default', ['sass-compile', 'watch']);