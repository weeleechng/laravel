function display_hide_field_row (row_class)
{
  /*if (row_class != undefined)*/
  if ($('select#gr').length > 0)
  {
    $('tr.report-field').each (function (index, row)
    {
      if ($(this).hasClass (row_class) || $(this).hasClass ('core'))
      {
        $(this).css ({ 'display':'table-row', });
      }
      else
      {
        $(this).css ({ 'display':'none', }); 
      }

    });
  }
  else
  {
    $('tr.report-field').css ('display', 'table-row');
  }
}

function update_csv_filename (select_value)
{
  $('#report_csv_filename').val (select_value);
}

function update_condition_value_fields (select_item)
{
  var select_value = $(select_item).val ();
  var select_row = $(select_item).parents ('tr.report-field');

  /*
  var field_name_pattern = /^rf-cond\[(\S+)\]$/;
  var field_name = field_name_pattern.exec ($(select_item).attr ('name'))[1];
  */

  switch (select_value)
  {
    case 'none':
    case 'not_empty':
    case 'empty':
      $(select_row).find ('input.rf-val-1').hide ();
      $(select_row).find ('span.rf-val-2-prefix').hide ();
      $(select_row).find ('input.rf-val-2').hide ();
      break;

    case 'equals':
    case 'not':
    case 'contains':
    case 'less':
    case 'less_equal':
    case 'more_equal':
    case 'more':
      $(select_row).find ('input.rf-val-1').show ();
      $(select_row).find ('span.rf-val-2-prefix').hide ();
      $(select_row).find ('input.rf-val-2').hide ();
      break;

    case 'between':
      $(select_row).find ('input.rf-val-1').show ();
      $(select_row).find ('span.rf-val-2-prefix').show ();
      $(select_row).find ('input.rf-val-2').show ();
      break;

    default:
      break;
  }
}

function add_condition_change_listener ()
{
  $('.report-field .sel-cond').change (function ()
  {
    update_condition_value_fields ($(this));
  });
}

function add_group_change_listener ()
{
  if ($('select#gr').length > 0)
  {
    $('select#gr').change (function () 
    { 
      display_hide_field_row ($(this).val ()); 
      update_csv_filename ($(this).val ());
    });

    update_csv_filename ($('select#gr').val ());
  }
}

function validate_form_field ()
{
  var returnval = true;

  if ($('#report_create input#nm').val () == '')
  {
    alert ('Report name is required.');
    $('#report_create input#nm').focus ();
    returnval = false;
  }

  return returnval;
}

function have_selected_custom_field ()
{
  var row_check_count = 0;

  $('tr.report-field').each (function (index, row)
  {
    if ($(row).find ('input[name^="rf-sel"]').is (':checked'))
    {
      row_check_count++;
    }
  });

  if (row_check_count > 0)
  {
    return true;
  }
  else
  {
    alert ('At least one field must be included in the report.');
    return false;
  }

}

function post_only_visible () /* rows and input */
{
  $('tr.report-field').each (function (index, row) 
  {
    if ($(row).css ('display') == 'none')
    {
      $(row).remove ();
    }

    $(row).find ('input.rf-val-1, input.rf-val-2').each (function (index, element)
    {
      if ($(element).css ('display') == 'none')
      {
        $(element).remove ();
      }
    });

  });

}

function add_submit_listener ()
{
  $('#report_create').submit (function ()
  {
    if (!validate_form_field ())
    {
      return false;
    }

    if (!have_selected_custom_field ())
    {
      return false;
    }

    post_only_visible ();
    return true;

  });
}

function hide_field_condition ()
{
  $('tr.report-field').each (function (index, row)
  {
    update_condition_value_fields ($(row).find ('select.sel-cond'));
  });

}

$(document).ready (function ()
{
  add_group_change_listener ();
  add_condition_change_listener ();
  display_hide_field_row ($('select#gr').val ());
  hide_field_condition ();
  add_submit_listener ();
});
