$.group_field_del = Array ();
$.group_field_row = '';

function truncate_empty_group_fields ()
{

  $('tr.group-field').each (function (index, row)
  {
    if ($(row).find ('input[name^="fl"]').val() == '')
    {
      $(row).remove ();
    }

  });

  console.log ('truncate empty fields complete');

}

function truncate_deleted_rows ()
{

  $('tr.group-field.row-deleted').each (function (index, row)
  {
    $(row).remove ();
  });

  console.log ('truncate deleted fields complete');

}

/* Instead of naming only dynamically added fields, re-name every one of them */
function name_dyn_group_fields ()
{

  $('tr.group-field').each (function (index, row)
  {
    var fieldnameval = $(row).find ('[name^="fl"]').val ();

    $(row).find ('input, select').each (function (index, field)
    {
      
      $(field).attr ('name', $(field).attr ('name').replace ('[]', '[' + fieldnameval + ']'));

    });

  });

  console.log ('name dyn fields complete');

}

function id_dyn_group_fields ()
{

  $('tr.group-field.add').each (function (index, row)
  {

    $(row).find ('input[type=checkbox]').each (function (index_field, checkbox_field)
    { 
      $(checkbox_field).attr ('id', $(checkbox_field).attr ('id') + '-add-' + index);
      $(checkbox_field).siblings ('label').attr ('for', $(checkbox_field).attr ('id'));
    });

    // $(row).addClass ('id_dyn-ed');
  });

  console.log ('id dyn fields complete');

}

function no_duplicate_map ()
{
  var mapped = Array ();
  var returnval = true;

  $('select[name^="mp"]').each (function (index, item)
  {

    if ($(item).val() != '' && $.inArray ($(item).val(), mapped) != -1)
    {
      alert ("Each field map can only be used once. Please check.");
      returnval = false;
      return false;
    }

    mapped.push ($(item).val ());

  });

  return returnval;

}

function has_atleast_two_uniquewith_s ()
{
  /* there must be at least 2 unique_with fields, if it is used */
  var checked_unique_with_count = 0;

  $('.checkbox-fieldset input[name*=unique_with]').each (function (index, value) 
  { 
    if ($(value).is (':checked'))
    {
      checked_unique_with_count++;
    }
  });

  if (checked_unique_with_count == 1)
  {
    alert ('There must be at least 2 fields selected if you wish to use the "Unique with" rule.');
    return false;  
  }
  else
  {
    return true;
  }
}

function no_duplicate_fieldname ()
{
  // console.log ('checking for duplicates');

  var fieldnames = Array ();
  var returnval = true;

  // $('[name^=fl]').each (function (index, field)
  $('tr.group-field:not(tr.row-deleted)').each (function (index, row)
  {
    var field = $(row).find ('[name^=fl]');
    if ($.inArray ($(field).val(), fieldnames) != -1 && $(field).val() != '')
    {
      alert ("Each Field name must be unique. Please check.");
      returnval = false;
      return false; // break out of .each loop
    }

    fieldnames.push ($(field).val ());

  });

  // console.log ('check for duplicates complete');

  return returnval; // return to caller function

}

function validate_group_fields ()
{

  var returnval = true;

  $('tr.group-field:not(tr.mandatory)').each (function (index, row)
  {
    
    var row_fieldname = $(row).find ('[name^="fl"]');
    var row_dispname = $(row).find ('[name^="dn"]');
    // var row_map = $(row).find ('td [name^="mp"]');

    // console.log ('checking group fields');

    if ($(row_fieldname).val () == '')
    {
      if ($(row_dispname).val () != '')
      {
        alert ("Field name is required.");
        $(row_fieldname).focus ();
        returnval = false;
        return false; // break out of .each loop
      }
    }

  });

  // console.log ('validate: group fields complete');
  return returnval; // return to caller function

}

function validate_core_fields ()
{

  // console.log ('validate: core fields');

  if ($('#id').val () == '')
  {
    $('#id').focus ();
    alert ('Group Identifier is required.'); 
    return false;
  }

  if ($('#nm').val () == '')
  {
    $('#nm').focus ();
    alert ('Group Name is required.'); 
    return false;
  }

  // console.log ('validate: core fields complete');
  return true;

}

function validate_mandatory_fields ()
{
  var returnval = true;

  $('tr.group-field.mandatory').each (function (index, row) 
  {
    if ($(row).find ('input[name^="fl"]').val() == '')
    {
      $(row).find ('input[name^="fl"]').focus ();
      alert ('This field is mandatory for a valid lead.');
      returnval = false;
      return false;
    }
  });

  return returnval;
}

function hide_undo_button ()
{
  $('#undo-remove-field').css ({ 'display':'none', });
}

function show_undo_button ()
{
  $('#undo-remove-field').css ({ 'display':'', });
}

function remove_group_row (inrow)
{
  $(inrow).find ('input[name^="deleted"]').val ('true');
  $.group_field_del.push ($(inrow));
  $(inrow).addClass ('row-deleted');
  $(inrow).css ({ 'display':'none', });
}

function restore_group_row (inrow)
{
  $(inrow).find ('input[name^="deleted"]').val ('false');
  $(inrow).removeClass ('row-deleted');
  $(inrow).css ({ 'display':'', });
}

function remove_remove_field_listener ()
{
  $('button.remove-field').off ('click.group_field_row_delete');
}

function add_remove_field_listener ()
{
  $('button.remove-field').on ('click.group_field_row_delete', function (clicked) 
  {
    remove_group_row ($(clicked.target).parents ('tr.group-field')); 
    show_undo_button ();
  });
}

function add_undo_remove_listener ()
{
  $('#undo-remove-field').click (function (clicked)
  { 
    var hidden = $.group_field_del.pop ();

    restore_group_row (hidden);

    if ($.group_field_del.length == 0)
    {
      hide_undo_button ();
    }

    // console.log ($.group_field_del.length);

  });
}

function add_add_field_listener ()
{
  $('#add-field').click (function ()
  { 
    $.group_field_row.appendTo ($('table.group--fields'));
    store_field_row ();

    /* reset all event listeners */
    remove_remove_field_listener ();
    add_remove_field_listener ();
    remove_checkset_action_listener ();
    add_checkset_action_listener ();
    remove_checkset_check_listener ();
    add_checkset_check_listener ();
    
    id_dyn_group_fields ();
  });

}

function add_checkset_action_listener ()
{
  $('.checkbox-fieldset').on ('mouseenter.group_field', function (hovered)
  {
    $(this).find ('.textfield-floating').css ({ 'display':'block', });
  });

  $('.checkbox-fieldset').on ('mouseleave.group_field', function (hovered)
  {
    $(this).find ('.textfield-floating').css ({ 'display':'none', });
  });
}

function remove_checkset_action_listener ()
{
  $('.checkbox-fieldset').off ('mouseenter.group_field');

  $('.checkbox-fieldset').off ('mouseleave.group_field');
}

function add_checkset_check_listener ()
{
  /* unique and unique_with checkboxes cannot be used together */
  $('.checkbox-fieldset input[type=checkbox]').on ('change.unique_one', function () 
  {
    var target_name;
    if ($(this).is(':checked') && $(this).attr ('name').indexOf ('unique') > -1) 
    {
      target_name = $(this).attr ('name');

      $(this).parent().parent().find ('.checkbox-fieldset input[type=checkbox]').each (function (index, value)
      {
        if ($(value).attr ('name').indexOf ('unique') > -1 && $(value).attr ('name') != target_name && $(value).is(':checked'))
        {
          $(value).prop ('checked', false);
          console.log ('Checkbox: Unique and Unique with cannot be used together');
        }
      });
    }  
  });
}

function remove_checkset_check_listener ()
{
  $('.checkbox-fieldset input[type=checkbox]').off ('change.unique_one');
}

function store_field_row ()
{
  $.group_field_row = $('tr.group-field.add').last().clone ();
  $.group_field_row.attr ('rowcount', parseInt ($.group_field_row.attr('rowcount')) + 1);  
}

function add_submit_listener ()
{
  $('#group_create').submit (function (submit_event) 
  { 

    var validate_test = false;

    if (validate_core_fields () == true)
    if (validate_mandatory_fields () == true)
    if (validate_group_fields () == true)
    if (no_duplicate_fieldname () == true)
    if (has_atleast_two_uniquewith_s () == true)
    {
      validate_test = true;
      truncate_empty_group_fields ();
      truncate_deleted_rows ();
      name_dyn_group_fields ();
    }

    if (validate_test == false)
    {
      submit_event.preventDefault ();
    }

  });

}

$(document).ready (function ()
{
  hide_undo_button (); 
  add_remove_field_listener ();
  add_undo_remove_listener ();
  add_add_field_listener ();
  add_checkset_action_listener ();
  add_checkset_check_listener ();
  add_submit_listener ();

  store_field_row ();
});
