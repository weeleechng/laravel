/*** SAMPLE: jQuery AJAX submission code ***/

function process_response (data)
{
  if (data.status == 'success')
  {
    toggle_submit_button (true, 'Success');
    alert ('Submission received. Thank you.');
    location.reload();
  }
  else if (data.status == 'fail')
  {
    $.each (data.errors, function (index, value)
    {
      $('#label_' + index).html (value);
    });

    toggle_submit_button (true);
  }
}

function toggle_submit_button (button_switch, button_value)
{
  var button_value_text;

  switch (button_switch)
  {
    case false:
      $('#sample_form #submit-button').attr ({'disabled':'disabled'});
      button_value_text = 'Processing';
      break;

    case true:
      button_value_text = 'Submit';
      $('#sample_form #submit-button').removeAttr ('disabled');
      break;
  }

  if (button_value != undefined)
  {
    button_value_text = button_value;
  }

  $('#sample_form #submit-button').attr ({ 'value': button_value_text, });
}

$(document).ready (function ()
{
  $('#sample_form').on ('submit', function (submitevent)
  {
    submitevent.preventDefault ();
    $('.response_label').html ('');

    $.ajax
    ({
      url     : $(this).attr ('action'),
      type    : $(this).attr ('method'),
      dataType: 'json',
      data    : $(this).serialize (),
      success : function (data) { process_response (data); },
      error   : function (xhr, err) { alert ('Error'); }
    });

    toggle_submit_button (false);

  });

});
