/* Render form HTML in textarea */
$.fn.toHtmlString = function () 
{
  return $('<div></div>').html($(this).clone()).html();
};

function process_response (data)
{
  if (data.status == 'success')
  {
    toggle_submit_button (true, 'Success');
    alert ('Submission received. Thank you.');
    location.reload(); // or do something to inform user that the form has been submitted
  }
  else if (data.status == 'fail')
  {
    $.each (data.errors, function (index, value)
    {
      $('#label_' + index).html (value);
    });

    toggle_submit_button (true);
  }
}

function toggle_submit_button (button_switch, button_value)
{
  var button_value_text;

  switch (button_switch)
  {
    case false:
      $('#sample_form #submit-button').attr ({'disabled':'disabled'});
      button_value_text = 'Processing';
      break;

    case true:
      button_value_text = 'Submit';
      $('#sample_form #submit-button').removeAttr ('disabled');
      break;
  }

  button_value_text = (button_value != undefined) ? button_value : button_value_text;
  $('#sample_form #submit-button').attr ({ 'value': button_value_text, });
}

function fill_sample_form_code (ajax_data)
{
  var sample_html_string = $('#sample_form').toHtmlString ();
  sample_html_string += '\n\n<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>';
  sample_html_string += '\n<script type="text/javascript">\n' + ajax_data.toString() + '\n</script>';
  sample_html_string = sample_html_string.replace (/\</g, "&lt;").replace (/\>/g, "&gt;");
  $('#sample_form_code').html (sample_html_string);
  $('#sample_form_code').height (($('#sample_form_code')[0].scrollHeight));
  $('#sample_form_code').focus(function() 
  {
    var $this = $(this);
    $this.select();

    // Work around Chrome's little problem
    $this.mouseup(function() 
    {
      // Prevent further mouseup intervention
      $this.unbind("mouseup");
      return false;
    });

  });
    
}

$(document).ready (function ()
{
  if ($("#sample_form_code").length > 0 && $("#sample_form").length > 0)
  {
    $.get 
    (
      $('#sample-form-ajax-script').attr ('src'), 
      function (data) 
      { 
        fill_sample_form_code (data); 
      }, 
      'html' // treat server response as plain text, not script
    );
  }
  else
  {
    console.log ("There's a problem generating the sample form code.");
  }

});
